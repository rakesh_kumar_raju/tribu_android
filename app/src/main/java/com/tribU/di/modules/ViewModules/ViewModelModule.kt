package com.tr.di.modules.ViewModules

import androidx.lifecycle.ViewModel
import com.bishaan.onboarding.viewmodel.SignupProcessVM
import com.example.movieapp.utils.ViewModelKey
import com.tribU.home.viewmodel.*
import com.tribU.main_cantainer.viewmodel.MainBaseVM
import com.tribU.otp.viewmodel.OtpActivityVM
import com.tribU.profile.viewmodel.AccountsVM
import com.tribU.profile.viewmodel.ProfileFragmentVM
import com.tribU.profile.viewmodel.ProfileVM
import com.tribU.profile.viewmodel.WalletVM
import com.tribU.transaction.viewmodel.SendTribuFragVM
import com.tribU.transaction.viewmodel.SendTribuHomeFragVM
import com.tribU.transaction.viewmodel.TimerActivityVM
import com.tribU.transaction.viewmodel.TransactionzFragVM

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

//    @Provides
//    open fun provideViewModelProvider(viewModel: TrendingFragVM?): ViewModelProvider.Factory? {
//        return ViewModelProviderFactory(viewModel)
//    }

    @Binds
    @IntoMap
    @ViewModelKey(CreateAccountFragVM::class)
    abstract fun bindCreateAccountViewModel(myCreateAccountFragVM: CreateAccountFragVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OtpActivityVM::class)
    abstract fun bindSignUpViewModel(myOtpActivityVM: OtpActivityVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(ProfileFragmentVM::class)
    abstract fun bindProfileViewModel(profileFragmentVM: ProfileFragmentVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(AccountVerificationVM::class)
    abstract fun bindAccountVerifyViewModel(accountVerificationVM: AccountVerificationVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddAccountFragVM::class)
    abstract fun bindAddNewAccountViewModel(addAccountFragVM: AddAccountFragVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(TransactionzFragVM::class)
    abstract fun bindTransactionViewModel(transactionzFragVM: TransactionzFragVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SendTribuFragVM::class)
    abstract fun bindSendTribuViewModel(sendTribuFragVM: SendTribuFragVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainBaseVM::class)
    abstract fun bindMainBaseViewModel(mainBaseVM: MainBaseVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(TimerActivityVM::class)
    abstract fun bindTimerViewModel(timerActivityVM: TimerActivityVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SendTribuHomeFragVM::class)
    abstract fun bindSendTribuHomeViewModel(sendTribuHomeFragVM: SendTribuHomeFragVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SignupProcessVM::class)
    abstract fun bindSignupProcessVM(signupProcessVM: SignupProcessVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(ProfileVM::class)
    abstract fun bindProfileVM(profileVM: ProfileVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(HomeVM::class)
    abstract fun bindHomeVM(homeVM: HomeVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(WalletVM::class)
    abstract fun bindWalletVM(walletVM: WalletVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountsVM::class)
    abstract fun bindAccountsVM(accountsVM: AccountsVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransactionsVM::class)
    abstract fun bindTransactionsVM(transactionsVM: TransactionsVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SplashVM::class)
    abstract fun bindSplashVM(splashVM: SplashVM): ViewModel




}