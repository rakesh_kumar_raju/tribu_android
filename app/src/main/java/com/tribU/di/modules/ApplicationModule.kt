package com.tribU.di.modules



import com.bishaan.utils.Util
import com.tribU.errorProvider.ErrorProvider
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.tribU.app.MyApplication
import com.tribU.errorProvider.ErrorProviderImpl
import com.tribU.utils.PrefUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: MyApplication) {

    // region Global Use
    @Singleton
    @Provides
    fun providesGson(): Gson {
        return GsonBuilder()
            .create()
    }

    @Singleton
    @Provides
    fun provideErrorProvider(): ErrorProvider {
        return ErrorProviderImpl(application.applicationContext)
    }

    @Singleton
    @Provides
    fun provideUtil(): Util = Util(application.applicationContext)

    @Singleton
    @Provides
    fun PrefUtils(): PrefUtils = PrefUtils(application.applicationContext)




}