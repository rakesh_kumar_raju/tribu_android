package com.tribU.di.component

import com.tribU.SplashNewActivity
import com.tr.di.modules.ViewModules.ViewModelModule
import com.tribU.app.MyApplication
import com.tribU.di.modules.ApplicationModule
import com.tribU.di.modules.NetworkModule
import com.tribU.di.modules.ViewModules.ViewModelFactoryModule
import com.tribU.home.fragments.*
import com.tribU.home.view.HomeActivity
import com.tribU.home.view.TransactionHistoryActivity
import com.tribU.linkaccount.view.LinkAccountActivity
import com.tribU.login.view.LoginActivity
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.on_boarding.view.LetsGetStartedActivity
import com.tribU.otp.view.OTPActivity
import com.tribU.profile.fragments.ContactUsFragment
import com.tribU.profile.view.*
import com.tribU.share.fragment.ShareFragment
import com.tribU.signup.view.GettingStarted1Activity
import com.tribU.signup.view.GettingStarted2Activity
import com.tribU.transaction.TimerActivity
import com.tribU.transaction.fragments.*


import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        NetworkModule::class, ViewModelModule::class,ViewModelFactoryModule::class
    ]
)

interface AppComponent {

    fun inject(myApplication: MyApplication)
    fun inject(myCreateAccountFragment: CreateAccountFragment)
    fun inject(myLetsGetStartedActivity: LetsGetStartedActivity)
    fun inject(myTransactionFragment: TransactionFragment)
    fun inject(myAddNewAccountFragment: AddNewAccountFragment)
    fun inject(myAccountConfirmationFragment: ConfirmationFragment)
    fun inject(mySendTribuFragment: SendTribuFragment)
    fun inject(mySuccessfullyTribuSentFragment: SuccessfullyTribuSentFragment)
    fun inject(profileActivity: ProfileActivity)
    fun inject(myContactUsFragment: ContactUsFragment)
    fun inject(myTribuFromHomeFragment: SendTribuFromHomeFragment)
    fun inject(myShareFragment: ShareFragment)
    fun inject(myLoginActivity: LoginActivity)
    fun inject(myOTPActivity: OTPActivity)
    fun inject(accountVerificationFragment: AccountVerificationFragment)
    fun inject(underVerificationFrag: UnderVerificationFrag)
    fun inject(myBottomNavigationActivity: BottomNavigationActivity)
    fun inject(transactionFailedFragment: TransactionFailedFragment)
    fun inject(timerActivity: TimerActivity)
    fun inject(homeActivity: HomeActivity)
    fun inject(walletActivity: WalletActivity)
    fun inject(gettingStarted1Activity: GettingStarted1Activity)
    fun inject(gettingStarted2Activity: GettingStarted2Activity)
    fun inject(accountsActivity: AccountsActivity)
    fun inject(addMoneyActivity: AddMoneyActivity)
    fun inject(transactionHistoryActivity: TransactionHistoryActivity)
    fun inject(supportActivity: SupportActivity)
    fun inject(splashNewActivity: SplashNewActivity)
    fun inject(linkAccountActivity: LinkAccountActivity)

}