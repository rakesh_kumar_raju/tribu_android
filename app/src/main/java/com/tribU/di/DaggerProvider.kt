package com.tribU.di
import com.tribU.app.MyApplication
import com.tribU.di.component.AppComponent
import com.tribU.di.component.DaggerAppComponent

import com.tribU.di.modules.ApplicationModule



class DaggerProvider {

    companion object {
        private var appComponent: AppComponent? = null

        fun initComponent(application: MyApplication) {
            appComponent = DaggerAppComponent.builder()
                .applicationModule(ApplicationModule(application))
                .build()
        }

        fun getAppComponent(): AppComponent? {
            return appComponent
        }

    }
}