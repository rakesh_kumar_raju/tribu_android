package com.tribU.pushNotification

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.tribU.R
import com.tribU.home.view.HomeActivity
import java.util.*
import kotlin.Exception


class PushNotificationReciever : FirebaseMessagingService() {

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        Log.d("remoteMessage", "From: " + remoteMessage.from)

        try {
            Log.d("remoteMessage", "notification : " + remoteMessage.notification)

        }catch (e:Exception)
        {

        }


        if (remoteMessage.data.isNotEmpty()) {
            Log.d("remoteMessage", "Message data payload: " + remoteMessage.data)
            var requestId:String? =""
            var amount:String? =""
            try {
                 requestId=remoteMessage.data["requestId"].toString()
                amount=remoteMessage.data["amount"].toString()
            }catch (e:Exception)
            {

            }


            showNotification(
                remoteMessage.data["title"],
                remoteMessage.data["body"],
                remoteMessage.data["type"],
                requestId,
                amount
            )


        }


    }

    @SuppressLint("ObsoleteSdkInt")
    private fun showNotification(
        title: String?,
        message: String?,
        type: String?,
        requsetId: String?,
        amount: String?
    ) {
        // Pass the intent to switch to the NotificationsActivity
        var intent = Intent(this, HomeActivity::class.java)

        if (type.toString().contains("accept_reject")) {

            intent.putExtra("requestId", requsetId.toString())
            intent.putExtra("amount", amount.toString())


        }


        intent.action = Intent.ACTION_MAIN
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        // Assign channel ID
        val channel_id = "notification_channel"

        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val pattern = longArrayOf(500, 500, 500, 500, 500)


        val builder = NotificationCompat.Builder(
            applicationContext,
            channel_id
        )
            .setAutoCancel(true)
            .setVibrate(pattern)
            .setLights(Color.BLUE, 1, 1)
            .setOnlyAlertOnce(true)
        //  .setSound(defaultSoundUri)
        //  .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setContentTitle(title)
            .setContentText(message)
            .setLargeIcon(
                BitmapFactory.decodeResource(
                    this.resources,
                    R.drawable.ic_splash_t
                )
            )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.drawable.ic_splash_t)
            builder.color = resources.getColor(R.color.yellow)
            builder.setColorized(true)
            builder.setContentIntent(pendingIntent)
        } else {
            builder.setSmallIcon(R.drawable.ic_splash_t)
            builder.setColorized(true)
            builder.setContentIntent(pendingIntent)
        }

        val random = Random()
        val m = random.nextInt(9999 - 1000) + 1000


        val notificationManager = getSystemService(
            NOTIFICATION_SERVICE
        ) as NotificationManager
        // Check if the Android Version is greater than Oreo
        if (Build.VERSION.SDK_INT
            >= Build.VERSION_CODES.O
        ) {
            val notificationChannel = NotificationChannel(
                channel_id, "web_app",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(
                notificationChannel
            )
        }

     //   val mp: MediaPlayer = MediaPlayer.create(applicationContext, R.raw.notification)
   //      mp.start()

        notificationManager.notify(m, builder.build())


    }
}