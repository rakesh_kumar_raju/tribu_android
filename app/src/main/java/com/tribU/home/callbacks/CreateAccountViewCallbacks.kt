package com.tribU.home.callbacks

interface CreateAccountViewCallbacks {
    fun addClick()
    fun backClick()
}