package com.tribU.home.callbacks

interface HomeViewCallbacks {
    fun continueClick()
    fun balanceClick()
    fun profileClick()
    fun sendClick()
}