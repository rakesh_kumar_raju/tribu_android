package com.tribU.home.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.activity.addCallback
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.google.common.hash.Hashing.md5
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.AddAccountFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.callbacks.AddAccountFragCallbacks
import com.tribU.home.callbacks.AddAccountViewCallbacks
import com.tribU.home.model.ResponseUserAccountInfoDM
import com.tribU.home.viewmodel.AddAccountFragVM
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import javax.inject.Inject


class AddNewAccountFragment : BaseDataBindingFragment<AddAccountFragDataBinding>(R.layout.add_new_account_layout),
    AddAccountViewCallbacks {

    @set:Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var view_model: AddAccountFragVM
    var mContext: Context? = null
    var from_which_screen=""
    var need_account_verify=false



    companion object {
        fun newInstance() =
            AddNewAccountFragment()
    }

    private var callBackListener: AddAccountFragCallbacks? = null
    //private val viewModel by viewModels<MyLibraryFragVM>(factoryProducer = { viewModelFactory })



    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
        try {
            callBackListener = activity as AddAccountFragCallbacks
        } catch (e: ClassCastException) {
            throw ClassCastException(
                activity.toString()
                        + "  must implement MyLibraryCallbacks"
            )
        }
    }
//    fun initBottomMenu() {
//        (mContext as HomeBaseActivity).bottomMenuItemSelector(false, true, false, false,false,"library")
//
//    }


    override fun onDataBindingCreated() {
        binding.callback=this
        view_model = ViewModelProvider(this, viewModelFactory).get(AddAccountFragVM::class.java)
        CommonUtilities.getString(requireActivity(), Constants.TOKEN)?.let { Log.d("get_token", it) }
        setupObserver()



        from_which_screen = requireArguments().getString(Constants.FROM_WHICH_SCREEN).toString()
        if(from_which_screen==Constants.FROM_HOME) {
            binding.btnAddNow.setText(R.string.create_stripe_account)
            if(CommonUtilities.getBoolean(requireContext(), Constants.IS_ACCOUNT_CREATED))
            {
                if(CommonUtilities.getBoolean(requireContext(), Constants.IS_ACCOUNT_VERIFIED)){

                    //account verified



                    binding.btnAddNow.visibility=View.INVISIBLE
                    binding.tvAccountNo.visibility=View.VISIBLE
                    UserAccountDetails()
                    binding.tvDetails.visibility=View.VISIBLE
                    binding.tvDetails.setText(R.string.verify_done_description)




                }
                else{


                    //verify account
                    binding.tvText.visibility=View.GONE
                    binding.tvDetails.visibility=View.VISIBLE
                    binding.tvAccountNo.visibility=View.VISIBLE
                    binding.btnAddNow.setText(R.string.verify_account)
                    binding.tvDetails.setText(R.string.verify_your_bank_account_description)
                    need_account_verify=true
                    UserAccountDetails()
                }
            }
            else{


                //add account


                binding.tvText.visibility=View.VISIBLE
                binding.tvDetails.visibility=View.VISIBLE
                binding.tvText.setText(R.string.add_your_bank_account)
                binding.btnAddNow.setText(R.string.add_now)
                binding.tvDetails.setText(R.string.add_your_bank_account_description)
                binding.tvAccountNo.visibility=View.GONE

            }


        }
        else if(from_which_screen==Constants.UNDER_VERIFICATION){

            //verify account

         /*   binding.tvAccountNo.visibility=View.VISIBLE
            binding.btnAddNow.setText(R.string.verify_account)
            UserAccountDetails()*/

            binding.tvText.visibility=View.GONE
            binding.tvDetails.visibility=View.VISIBLE
            binding.tvAccountNo.visibility=View.VISIBLE
            binding.btnAddNow.setText(R.string.verify_account)
            binding.tvDetails.setText(R.string.verify_your_bank_account_description)
            need_account_verify=true
            UserAccountDetails()
            need_account_verify=true
        }
        else if(from_which_screen==Constants.VERIFICATION_DONE){
            //account verified

            binding.btnAddNow.visibility=View.INVISIBLE
            binding.tvAccountNo.visibility=View.VISIBLE
            UserAccountDetails()
            binding.tvDetails.visibility=View.VISIBLE
            binding.tvDetails.setText(R.string.verify_done_description)
        }
        else{

        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            val startMain = Intent(Intent.ACTION_MAIN)
            startMain.addCategory(Intent.CATEGORY_HOME)
            startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
        }

        callback.isEnabled

    }
    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }

    override fun addNowClick() {
        if(need_account_verify==true){
            (mContext as BottomNavigationActivity).replaceFragment(AccountVerificationFragment())
            need_account_verify=false
        }
        else {
            (mContext as BottomNavigationActivity).replaceFragment(CreateAccountFragment())

        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as BottomNavigationActivity).binding!!.bottomNavigationMain.menu.getItem(0).isChecked = true
    }

fun UserAccountDetails(){
    view_model.GetBankInfoApi(CommonUtilities.getString(requireActivity(), Constants.TOKEN)!!)
}

    private fun setupObserver() {
        view_model.getBankInfoResult.observe(viewLifecycleOwner, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    //  CommonUtilities.showLoader(mContext!!)
                }
                Status.ERROR -> {
                    // CommonUtilities.hideLoader()
                    CommonUtilities.showToastString(mContext!!, it.message.toString())
                }
                Status.SUCCESS -> {
                    // CommonUtilities.hideLoader()
                    val result = it.data
                    if (result?.statusCode == 200) {
                        savePref(result)

                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(context as BottomNavigationActivity)

                    } else {

                        CommonUtilities.showToastString(mContext!!, it.data?.message.toString())

                    }
                }
            }

        })
    }

    fun savePref(result: ResponseUserAccountInfoDM){
        binding.tvText.setText(result.data!!.bankName)
        binding.tvAccountNo.setText("************" + result.data!!.last4)
    }

}