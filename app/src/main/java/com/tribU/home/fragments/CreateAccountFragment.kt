package com.tribU.home.fragments

import android.app.AlarmManager
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import androidx.core.app.NotificationCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.tribU.MyNotificationPublisher
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.CreateAccountFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.callbacks.CreateAccountViewCallbacks
import com.tribU.home.viewmodel.CreateAccountFragVM
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.signup.model.ResponseSignUpSignInDM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import com.tribU.utils.BottomSheet
import com.tribU.utils.BottomUpDialogAdapter
import javax.inject.Inject


class CreateAccountFragment :
    BaseDataBindingFragment<CreateAccountFragDataBinding>(R.layout.activity_create_account_layout),
    CreateAccountViewCallbacks {


    val NOTIFICATION_CHANNEL_ID = "10001"
    private val default_notification_channel_id = "default"

    companion object {


        fun newInstance() =
            CreateAccountFragment()
    }

    @set:Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var view_model: CreateAccountFragVM
    var accnt_holder_type_textfield = ""
    var accnt_holder_type = ""
    private val mBottomUpSheet = BottomSheet.getSheet()


    override lateinit var binding: CreateAccountFragDataBinding
    var mContext: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onDataBindingCreated() {
        binding.callback = this
        view_model = ViewModelProvider(this, viewModelFactory).get(CreateAccountFragVM::class.java)
        Log.d(
            "device_token",
            CommonUtilities.getString(requireActivity(), Constants.DEVICE_TOKEN)!!
        )
/*        setupObserver()
        var name = CommonUtilities.getString(requireActivity(), Constants.FULL_NAME)!!
        val words = name.split(" ") as MutableList<String>
        var count = words.size
        if (count == 1) {
            binding.edFirstName.setText(name)
        } else {
            binding.edFirstName.setText(words.get(0))
            words.removeAt(0)
            val str: String = words.joinToString(separator = " ")
            binding.edLastName.setText(str)
        }



        binding.edEmail.setText(CommonUtilities.getString(requireActivity(), Constants.EMAIL)!!)
        binding.edPhoneNo.setText(CommonUtilities.getString(requireActivity(), Constants.PHONE_NO))
        binding.tvPhoneCode.setCountryForNameCode(
            CommonUtilities.getString(
                requireActivity(),
                Constants.PHONE_NO
            )
        )
        binding.edAccntHolderType.setOnClickListener {
            accnt_holder_type_textfield = binding!!.edAccntHolderType.text.toString()




         //   initReferredByBottomSheet()
        }
           */

    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }


    override fun onBackPress(): Boolean {
        return super.onBackPress()
    }

    override fun addClick() {
        setValidations()

    }

    override fun backClick() {
        (mContext as BottomNavigationActivity).onBackPressed()
    }

    fun setValidations() {
        if (binding.edFirstName.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_first_name)
        } else if (binding.edLastName.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_last_name)
        } else if (binding.edPhoneNo.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_your_mobile_no)
        } else if (binding.edEmail.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_your_email)
        } else if (binding.edEmail.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_your_email)
        } else if (!(Patterns.EMAIL_ADDRESS.matcher(binding?.edEmail!!.text.toString())
                .matches())
        ) {
            CommonUtilities.showToast(context, R.string.please_enter_valid_email)
        } else if (binding.edAccntNumber.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_accnt_no)
        } else if (binding.edAccntNameHolder.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_accnt_holder_name)
        } else if (binding.edAccntHolderType.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_select_accnt_holder_type)
        } else if (binding.edRountingNo.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_routing_no)
        } else if (binding.edRountingNo.text.length < 9) {
            CommonUtilities.showToast(context, R.string.please_enter_valid_routing_no)
        }
        /*
            else if (binding.edMcc.text.length < 1) {
                CommonUtilities.showToast(context, R.string.please_enter_mcc)
            }



            else if (binding.edMcc.text.length  <4) {
                CommonUtilities.showToast(context, R.string.please_enter_valid_mcc)

            }

                */
        else {
            binding.btnAdd.isEnabled = false
            binding.loader.startAnimation();
            binding.loader.setIsVisible(true);
            view_model!!.createAccountApi(
                CommonUtilities.getString(requireContext(), Constants.TOKEN)!!,
                binding.edFirstName.text.toString(),
                binding.edLastName.text.toString(),
                binding.tvPhoneCode.textView_selectedCountry.text.toString(),
                binding.edPhoneNo.text.toString(),
                binding.edEmail.text.toString(),
                binding.edAccntNumber.text.toString(),
                binding.edAccntNameHolder.text.toString(),
                accnt_holder_type,
                binding.edRountingNo.text.toString(),
                binding.edMcc.text.toString()
            )


        }
    }
/*

    fun initReferredByBottomSheet() {
        mBottomUpSheet.cancellationBottomSheet(
            context as BottomNavigationActivity,
            resources.getStringArray(R.array.holder_type),
            "Select Account Holder Type"
        )
        mBottomUpSheet.setCustomObjectListener(object :
            BottomSheet.OnOptionSelectedListener {
            override fun onOptionSelected(
                mAdapter: BottomUpDialogAdapter,
                position: Int,
                selectedOption: String
            ) {
                if (!mAdapter.selectedItem(position).equals("")) {
                    binding.edAccntHolderType.setText(selectedOption)
                    if (position == 0) {
                        accnt_holder_type = "individual"
                    } else {
                        accnt_holder_type = "company"
                    }

                }

            }
        })
        mBottomUpSheet.show()
    }

*/

    private fun setupObserver() {
        view_model.createAccountResult.observe(viewLifecycleOwner, Observer {
            when (it?.status) {
                Status.LOADING -> {

                }
                Status.ERROR -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    CommonUtilities.showToastString(mContext!!, it.message.toString())
                    binding.btnAdd.isEnabled = true
                }
                Status.SUCCESS -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    val result = it.data
                    if (result?.statusCode == 200) {
                        Log.d("get_create_result", result.data.toString())
                        savePref(result)

                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(context as BottomNavigationActivity)
                        binding.btnAdd.isEnabled = true


                    } else {
                        CommonUtilities.showToastString(mContext!!, it.data?.message.toString())
                        binding.btnAdd.isEnabled = true

                    }
                }
            }

        })
    }


    fun savePref(result: ResponseSignUpSignInDM) {
        context?.let {
            CommonUtilities.putBoolean(
                it,
                Constants.IS_ACCOUNT_CREATED,
                result.data!!.isStripeOnBoardingCompleted!!
            )
        }
        binding.btnAdd.isEnabled = true
        scheduleNotification(
            getNotification("Please don’t forget to verify your bank account.")!!,
            86400
        )
        (context as BottomNavigationActivity).replaceFragment(UnderVerificationFrag())
    }

    private fun scheduleNotification(notification: Notification, delay: Int) {
        val notificationIntent = Intent(requireContext(), MyNotificationPublisher::class.java)
        notificationIntent.putExtra(MyNotificationPublisher.NOTIFICATION_ID, 1)
        notificationIntent.putExtra(MyNotificationPublisher.NOTIFICATION, notification)
        val pendingIntent = PendingIntent.getBroadcast(
            requireContext(),
            0,
            notificationIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val futureInMillis: Long = SystemClock.elapsedRealtime() + delay
        val alarmManager =
            (requireContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager?)!!
        alarmManager[AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis] = pendingIntent
    }

    private fun getNotification(content: String): Notification? {
        val builder: NotificationCompat.Builder =
            NotificationCompat.Builder(requireContext(), default_notification_channel_id)
        builder.setContentTitle("TribU")
        builder.setContentText(content)
        builder.setSmallIcon(R.drawable.ic_launcher_foreground)
        builder.setAutoCancel(true)
        builder.setChannelId(NOTIFICATION_CHANNEL_ID)
        return builder.build()
    }

}







