package com.tribU.home.fragments

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.AccountVerificationFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.callbacks.AccountVerificationViewCallbacks
import com.tribU.home.viewmodel.AccountVerificationVM
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.signup.model.ResponseSignUpSignInDM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import javax.inject.Inject

class AccountVerificationFragment : BaseDataBindingFragment<AccountVerificationFragDataBinding>(R.layout.layout_account_verifcation),
    AccountVerificationViewCallbacks {



    fun newInstance() =
        AccountVerificationFragment()


    @set:Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var view_model: AccountVerificationVM
    override lateinit var binding: AccountVerificationFragDataBinding
    var mContext: Context? = null


    private var callBackListener: AccountVerificationViewCallbacks? = null
    //private val viewModel by viewModels<MyLibraryFragVM>(factoryProducer = { viewModelFactory })

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }


    override fun onDataBindingCreated() {
        binding.callback=this
        view_model = ViewModelProvider(this, viewModelFactory).get(AccountVerificationVM::class.java)
        setupObserver()

        //initBottomMenu()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }

    override fun verifyClick() {
        setValidations()

    }


    fun setValidations() {
        if (binding.edEnterAmount1.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_first_name)
        }else if (binding.edEnterAmount2.text.length < 1) {
            CommonUtilities.showToast(context, R.string.please_enter_last_name)
        } else {
            binding.loader.startAnimation();
            binding.loader.setIsVisible(true);
            view_model!!.verifyAccountApi(
                CommonUtilities.getString(requireContext(), Constants.TOKEN)!!,
               "32",
                "45"
            )
        }
    }

    private fun setupObserver() {
        view_model.verifyAccountResult.observe(viewLifecycleOwner, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    //  CommonUtilities.showLoader(mContext!!)
                }
                Status.ERROR -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    CommonUtilities.showToastString(mContext!!, it.message.toString())
                }
                Status.SUCCESS -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    val result = it.data
                    if (result?.statusCode == 200) {
                        savePref(result)

                    }

                    else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(context as BottomNavigationActivity)

                    }else {
                        CommonUtilities.showToastString(mContext!!, it.data?.message.toString())

                    }
                }
            }

        })
    }


    fun savePref(result: ResponseSignUpSignInDM) {
        context?.let { CommonUtilities.putBoolean(it, Constants.IS_ACCOUNT_VERIFIED,result.data!!.bankAccountStatus!!) }
        (context as BottomNavigationActivity).replaceFragment(ConfirmationFragment())
    }

}