package com.tribU.home.fragments

import android.content.Context
import android.os.Bundle
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.UnderVerificationFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.callbacks.UnderVerificationViewCallbacks
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.util.Constants

class UnderVerificationFrag : BaseDataBindingFragment<UnderVerificationFragDataBinding>(R.layout.layout_go_for_verification),
    UnderVerificationViewCallbacks {



    fun newInstance() =
        UnderVerificationFrag()


    //@set:Inject
    //lateinit var factory: ViewModelFactory
    //public var view_model: CreateAccountFragVM?=null
    override lateinit var binding: UnderVerificationFragDataBinding
    var mContext: Context? = null


    private var callBackListener: UnderVerificationViewCallbacks? = null
    //private val viewModel by viewModels<MyLibraryFragVM>(factoryProducer = { viewModelFactory })

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }


    override fun onDataBindingCreated() {
        binding.callback=this
        //initBottomMenu()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }


    override fun goToHomeClick() {
        val args = Bundle()
        args.putString(Constants.FROM_WHICH_SCREEN, Constants.UNDER_VERIFICATION)
        var fragment= AddNewAccountFragment()
        fragment.setArguments(args)
        (context as BottomNavigationActivity).replaceFragment(fragment)
    }

}