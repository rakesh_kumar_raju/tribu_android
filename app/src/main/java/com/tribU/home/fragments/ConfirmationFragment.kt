package com.tribU.home.fragments

import android.content.Context
import android.os.Bundle
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.AccountConfirmationFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.callbacks.AccountConfirmationViewCallbacks
import com.tribU.home.viewmodel.CreateAccountFragVM
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.transaction.fragments.TransactionFragment
import javax.inject.Inject

class ConfirmationFragment : BaseDataBindingFragment<AccountConfirmationFragDataBinding>(R.layout.account_confirmation_layout),
    AccountConfirmationViewCallbacks
{ companion object {

    fun newInstance() =
        ConfirmationFragment()
}

    @set:Inject
    lateinit var factory: ViewModelFactory
    public var view_model: CreateAccountFragVM?=null
    override lateinit var binding: AccountConfirmationFragDataBinding
    var mContext: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onDataBindingCreated() {
        binding.callback=this



    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }


    override fun onBackPress(): Boolean {
        return super.onBackPress()
    }

    override fun crossClick() {
       (context as BottomNavigationActivity).replaceFragment(TransactionFragment())
    }


//        override fun onOptionsItemSelected(@NonNull item: MenuItem): Boolean {
//            when (item.itemId) {
//                android.R.id.home -> {
//                    (mContext as HomeBaseActivity).onBackPressed()
//                    return true
//                }
//            }
//            return super.onOptionsItemSelected(item)
//        }
//


}



