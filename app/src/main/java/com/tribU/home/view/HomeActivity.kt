package com.tribU.home.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.airbnb.lottie.LottieAnimationView
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.bishaan.onboarding.view.OnboardingActivity
import com.bumptech.glide.Glide
import com.google.android.gms.ads.*
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener
import com.google.gson.JsonObject
import com.tribU.R
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.HomeActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.callbacks.HomeViewCallbacks
import com.tribU.home.viewmodel.HomeVM
import com.tribU.network.Status
import com.tribU.on_boarding.view.LetsGetStartedActivity
import com.tribU.profile.view.AccountsActivity
import com.tribU.profile.view.AddMoneyActivity
import com.tribU.profile.view.ProfileActivity
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import com.tribU.util.MyBottomSheetDialog
import java.text.SimpleDateFormat
import javax.inject.Inject

class HomeActivity :
    BaseDataBindingActivity<HomeActivityDataBinding>(R.layout.activity_home),
    HomeViewCallbacks {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var vm: HomeVM

    lateinit var device_token: String
    private val TAG = HomeActivity::class.java.simpleName
    var firebaseId: String? = null


    var walletBal = 0.0
    var walletId = ""
    var requestId = ""
    var amount = ""

    private var interstitialAd: InterstitialAd? = null


    override fun onResume() {
        super.onResume()

        binding.edPrice.setText("")

        // binding.edPrice.performClick()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)

        vm.getWalletBalApi(CommonUtilities.getString(this, Constants.TOKEN).toString())


        Glide.with(this)
            .load(CommonUtilities.getString(this, Constants.PROFILE_PIC))
            .into(binding.ivProfile)

    }

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this


    }

    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        vm = ViewModelProvider(this, viewModelFactory).get(HomeVM::class.java)




        if (CommonUtilities.isConnectingToInternet(this) == true) {

            CommonUtilities.showLoader(this)

            vm.getFundingSources(CommonUtilities.getString(this, Constants.TOKEN).toString())
            vm.getUserStatus(CommonUtilities.getString(this, Constants.TOKEN).toString())


        } else {
            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
        }

        setupObserver()




        if (intent.extras != null) {
            requestId = intent.getStringExtra("requestId").toString()
            amount = intent.getStringExtra("amount").toString()

            if (requestId.isNotEmpty()) {


                // test request id for time ran out
                //   requestId = "61de86f29eafa6e886e53f79"

                Log.e("TAG", "requestId: $requestId")
                vm!!.GetTribuStatusApi(
                    CommonUtilities.getString(
                        this,
                        Constants.TOKEN
                    )!!, requestId.toString().trim()
                )


            }
        }

        initAdMob()


    }

    override fun onBackPressed() {
        //super.onBackPressed()
        //  finishAffinity()
    }

    override fun continueClick() {




        var amount = binding.edPrice.text.toString()

        if (binding.edPrice.text.isEmpty()) {
            CommonUtilities.showToastString(this, "Please enter amount")

        } else if (walletBal < amount.toDouble()) {


            dialogNotEnoughMoneyInWallet(this)

        } else if (walletId.isNotEmpty()) {

            if (CommonUtilities.isConnectingToInternet(this) == true) {

                CommonUtilities.showLoader(this)


                var obj = JsonObject()
                obj.addProperty("ifFromWallet", true)
                obj.addProperty("from", walletId)
                obj.addProperty("amount", binding.edPrice.text.toString().toInt())


                vm.sendTribuRequestApi(
                    CommonUtilities.getString(this, Constants.TOKEN).toString(),
                    obj
                )


            } else {
                CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
            }

        }


        //  CommonUtilities.dialogNotEnoughMoneyInWallet(this)
        //   CommonUtilities.dialogSendingYourTribu(this)
        //   CommonUtilities.dialogAnonymousUserSentYou(this)
        //   CommonUtilities.dialogTimeRanOut(this)


        //  showBottomSheet(1, "Tribu sent!", "Thank you for doing your part to change the world!")
        //   showBottomSheet(2, "TribU failed to send!", "Try again")


    }

    override fun balanceClick() {


        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.edPrice.windowToken, 0)

        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, TransactionHistoryActivity::class.java),
            isFinish = false,
            isForward = true
        )
    }

    override fun profileClick() {


        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.edPrice.windowToken, 0)


        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, ProfileActivity::class.java),
            isFinish = false,
            isForward = true
        )
    }

    override fun sendClick() {


    }


    private fun showBottomSheet(
        type: Int,
        title: String,
        message: String,
        transactionId: String,
        date: String,
        amount: String
    ) {


        var bottomSheetDialog = MyBottomSheetDialog(this, false)


        @SuppressLint("InflateParams") val view: View =
            layoutInflater.inflate(R.layout.success_bottom_sheet, null)




        if (type == 2) {
            (view.findViewById(R.id.lottie_error) as LottieAnimationView).visibility =
                View.VISIBLE
        } else if (type == 1) {
            (view.findViewById(R.id.lottie_successful_payment) as LottieAnimationView).visibility =
                View.VISIBLE
        } else {
            (view.findViewById(R.id.lottie_money_added) as LottieAnimationView).visibility =
                View.VISIBLE
        }



        (view.findViewById(R.id.tv_title) as TextView).text = title
        (view.findViewById(R.id.tv_message) as TextView).text = message
        (view.findViewById(R.id.tv_balance) as TextView).text = amount + " USD"
        (view.findViewById(R.id.tv_transaction_id) as TextView).text = transactionId


        try {

            var date1 = date.toString()
            try {


                var date1 = date.toString()

                var format1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                val newDate1 = format1.parse(date1)
                format1 = SimpleDateFormat("dd MMM, hh:mm aaa")
                val date2 = format1.format(newDate1)
                (view.findViewById(R.id.tv_date) as TextView).text = date2

            } catch (e: Exception) {
                e.printStackTrace()
                (view.findViewById(R.id.tv_date) as TextView).text = date1
            }


        } catch (e: Exception) {
            e.printStackTrace()
            (view.findViewById(R.id.tv_date) as TextView).visibility = View.GONE

        }


        (view.findViewById(R.id.bt_continue) as View).setOnClickListener {

            bottomSheetDialog!!.dismiss()
            binding.edPrice.setText("")


        }



        bottomSheetDialog!!.setContentView(view)


/*

        val behaviour: BottomSheetBehavior<View> = BottomSheetBehavior.from<View>(view)
        behaviour.setBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {

                    finish()

                }
            }

            override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {}
        })
*/


        /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             Objects.requireNonNull(bottomSheetDialog!!.window)!!
                 .addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
         }*/

        bottomSheetDialog!!.show()
    }

    private fun setupObserver() {

        vm.responseWalletBal.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        var data = result?.data?.balanceDetails

                        walletBal = data!!.balance!!.value!!.toDouble()




                        binding.tvBalance.text =
                            data?.balance?.value + " " + data?.balance?.currency
                        var firstname = CommonUtilities.getString(this, Constants.FIRST_NAME)
                        var lastname = CommonUtilities.getString(this, Constants.LAST_NAME)


                        if (!result?.data?.profilePicture.isNullOrEmpty()) {
                            binding.ivProfile.visibility = View.VISIBLE
                            binding.tvProfile.visibility = View.INVISIBLE

                            CommonUtilities.putString(
                                this,
                                Constants.PROFILE_PIC,
                                result?.data?.profilePicture
                            )
                            Glide.with(this)
                                .load(CommonUtilities.getString(this, Constants.PROFILE_PIC))
                                .into(binding.ivProfile)

                        } else {
                            binding.tvProfile.visibility = View.VISIBLE
                            binding.ivProfile.visibility = View.INVISIBLE


                            binding.tvProfile.text =
                                firstname?.get(0).toString().toUpperCase() + " " + lastname?.get(0)
                                    .toString().toUpperCase()

                        }
                        if (!CommonUtilities.getString(this, Constants.PROFILE_PIC)
                                .isNullOrEmpty()
                        ) {

                            binding.ivProfile.visibility = View.VISIBLE
                            binding.tvProfile.visibility = View.INVISIBLE
                            Glide.with(this)
                                .load(CommonUtilities.getString(this, Constants.PROFILE_PIC))
                                .into(binding.ivProfile)

                        }


                        //  binding.tvWelcome.text = "Welcome\n" + firstname + " " + lastname

                        binding.tvWelcome.text =
                            "Welcome\n" + firstname + " " + lastname!!.split(' ')
                                .joinToString(" ") { it.capitalize() }


                    } else {

                     //   CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })


        vm.responseFundingSources.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        if (result?.data != null) {


                            //   result?.data?.let { it1 -> fundingSourcesList.addAll(it1) }


                            for (i in result?.data) {
                                if (i.type.equals("bank")) {
                                } else {
                                    walletId = i.id.toString()
                                }
                            }


                        }


                    } else {
                     //   CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })


        vm.responsSendTribuRequest.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        if (result?.data != null) {


                            var data = result?.data






                            showBottomSheet(
                                1,
                                data.popUpText!!.title.toString(),
                                data.popUpText!!.description.toString(),
                                data.id.toString(),
                                data.popUpText!!.createdAt.toString(),
                                data.amount.toString()
                            )


                        }


                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })


        vm?.tribuStatus!!.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    //   CommonUtilities.showLoader(this)

                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                    CommonUtilities.showToastString(this!!, it.message)
                }
                Status.SUCCESS -> {
                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {

                        if (result.data != null) {

                            Log.d("status", result.data!!.status.toString())
                            if (result.data!!.status == true) {


                                dialogAnonymousUserSentYou(this, amount)


                            } else {


                                try {
                                    var data = result.data!!.popUpText

                                    CommonUtilities.dialogTimeRanOut(
                                        this,
                                        data!!.title.toString(),
                                        data!!.description.toString()
                                    )

                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            }
                        }


                    } else if (result?.statusCode == 401) {
                     //   CommonUtilities.DialogToLogin(this)

                    } else {
                       // CommonUtilities.showToastString(this!!, it.data?.message.toString())
                        //  binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                    }
                }
            }

        })


        vm?.acceptTribuResult!!.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    // CommonUtilities.showLoader(this)
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                    CommonUtilities.showToastString(this!!, it.message)
                }
                Status.SUCCESS -> {
                    val result = it.data
                    if (result?.statusCode == 200) {

                        /*     if (result.data != null) {

                                 loadInterstitialAd()

                             }*/


                        var data = result.data!!.popUpText



                        showBottomSheet(
                            3,
                            data!!.title.toString(),
                            data.description.toString(),
                            data.id.toString(),
                            data.createdAt.toString(),
                            result.data!!.lastResponse!!.amount.toString()
                        )




                        if (CommonUtilities.isConnectingToInternet(this) == true) {

                            //      CommonUtilities.showLoader(this)

                            vm.getWalletBalApi(
                                CommonUtilities.getString(this, Constants.TOKEN).toString()
                            )
                            //  vm.getFundingSources(CommonUtilities.getString(this, Constants.TOKEN).toString())

                        } else {
                            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
                        }

                    } else if (result?.statusCode == 401) {
                        CommonUtilities.hideLoader()


                        CommonUtilities.DialogToLogin(this)

                    } else {
                        CommonUtilities.hideLoader()

                        CommonUtilities.showToastString(this!!, it.data?.message.toString())
                        //  binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                    }
                }
            }

        })


        vm?.rejectTribuResult!!.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    // CommonUtilities.showLoader(this)
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                    CommonUtilities.showToastString(this!!, it.message)
                }
                Status.SUCCESS -> {
                    CommonUtilities.hideLoader()
                    val result = it.data
                    if (result?.statusCode == 200) {

                        if (result.data != null) {


                        }


                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(this)

                    } else {
                        CommonUtilities.showToastString(this!!, it.data?.message.toString())
                        //  binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                    }
                }
            }

        })


        vm.responseUserStatus.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 400) {


                        CommonUtilities.clearPrefrences(this)

                        CommonUtilities.fireActivityIntent(
                            this,
                            Intent(this, OnboardingActivity::class.java),
                            isFinish = true,
                            isForward = true
                        )

                        //   CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })


    }


    fun dialogNotEnoughMoneyInWallet(activity: Activity) {


        var dialogGet = CommonUtilities.customDialog(
            context = activity!!,
            dialogView = R.layout.dialog_not_enough_money
        )

        val btn1 = dialogGet.findViewById<View>(R.id.btn1) as Button
        val btn2 = dialogGet.findViewById<View>(R.id.btn2) as Button

        btn1.setOnClickListener {

            CommonUtilities.fireActivityIntent(
                this,
                Intent(this, AddMoneyActivity::class.java)
                    .putExtra(Constants.CLICK_TYPE, Constants.ADD)
                    .putExtra("balance", walletBal),
                isFinish = false,
                isForward = true
            )

            dialogGet.cancel()
            dialogGet.dismiss()
        }

        btn2.setOnClickListener {


            CommonUtilities.fireActivityIntent(
                this,
                Intent(this, AccountsActivity::class.java)
                    .putExtra(Constants.FROM_WHICH_SCREEN, Constants.FROM_HOME)
                    .putExtra("amount", binding.edPrice.text.toString().toInt()),
                isFinish = false,
                isForward = true
            )



            dialogGet.cancel()
            dialogGet.dismiss()
        }


    }

    fun initAdMob() {


        MobileAds.initialize(this,
            OnInitializationCompleteListener { initializationStatus -> //Showing a simple Toast Message to the user when The Google AdMob Sdk Initialization is Completed
            })
        interstitialAd = InterstitialAd(this)
        interstitialAd!!.setAdUnitId("ca-app-pub-3940256099942544/1033173712")
        MobileAds.setRequestConfiguration(
            RequestConfiguration.Builder()
                .setTestDeviceIds(listOf("866410036089864"))
                .build()
        )
        interstitialAd!!.setAdListener(object : AdListener() {
            override fun onAdLoaded() {

                showInterstitialAd()
            }

            override fun onAdClosed() {

                //    /dwolla/transferMoneyFromSelfToReciever


                hitAcceptTribUApi()


                /* val args = Bundle()
                 args.putString(Constants.SHOW_MESSAGE, Constants.RECEIVED_SUCCESSFUL)
                 var fragment = SuccessfullyTribuSentFragment()
                 fragment.setArguments(args)
                 replaceFragment(fragment)*/


            }

        })

    }

    private fun hitAcceptTribUApi() {
        if (CommonUtilities.isConnectingToInternet(this) == true) {

            CommonUtilities.showLoader(this)


            var obj = JsonObject()
            obj.addProperty("requestId", requestId)


            vm.AcceptTribuApi(
                CommonUtilities.getString(this, Constants.TOKEN).toString(),
                obj
            )


        } else {
            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
        }
    }

    private fun loadInterstitialAd() {
        val adRequest: AdRequest = AdRequest.Builder().build()
        interstitialAd!!.loadAd(adRequest)
        // Toast.makeText(this, "Ad is loading ", Toast.LENGTH_LONG).show()

        CommonUtilities.showLoader(this)
    }


    private fun showInterstitialAd() {
        if (interstitialAd!!.isLoaded()) {
            //showing the ad Interstitial Ad if it is loaded
            interstitialAd!!.show()

            CommonUtilities.hideLoader()
        } else {
            CommonUtilities.hideLoader()

            loadInterstitialAd()
            Toast.makeText(this, "Ad is not Loaded ", Toast.LENGTH_LONG).show()
        }
    }


    fun dialogAnonymousUserSentYou(activity: Activity, amount: String) {


        var dialogGet = CommonUtilities.customDialog(
            context = activity!!,
            dialogView = R.layout.dialog_anonymous_user_sent_you
        )

        val btn1 = dialogGet.findViewById<View>(R.id.btn1) as Button
        val btn2 = dialogGet.findViewById<View>(R.id.btn2) as Button
        //   val tvTitle = dialogGet.findViewById<View>(R.id.tv_title) as TextView
        val tvMessage = dialogGet.findViewById<View>(R.id.tv_message) as TextView


        //   tvTitle.text = title.toString()
        tvMessage.text = amount.toString()

        btn1.setOnClickListener {
            dialogGet.cancel()
            dialogGet.dismiss()


            loadInterstitialAd()


        }

        btn2.setOnClickListener {
            dialogGet.cancel()
            dialogGet.dismiss()

            if (CommonUtilities.isConnectingToInternet(this) == true) {

                CommonUtilities.showLoader(this)


                var obj = JsonObject()
                obj.addProperty("tribuRequestId", requestId)


                vm.RejectTribuApi(
                    CommonUtilities.getString(this, Constants.TOKEN).toString(),
                    obj
                )


            } else {
                CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
            }


        }


    }


}
















