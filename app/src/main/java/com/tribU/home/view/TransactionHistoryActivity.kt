package com.tribU.home.view

import android.util.Log
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.tribU.R
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.TransactionHistoryActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.viewmodel.TransactionsVM
import com.tribU.network.Status
import com.tribU.profile.adapter.TransactionHistoryAdapter
import com.tribU.profile.callbacks.TransactionHistoryActivityCallbacks
import com.tribU.profile.model.TransactionsItem
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import javax.inject.Inject

class TransactionHistoryActivity :
    BaseDataBindingActivity<TransactionHistoryActivityDataBinding>(R.layout.activity_transaction_history),
    TransactionHistoryActivityCallbacks {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var vm: TransactionsVM

    var myAdapter: TransactionHistoryAdapter? = null

    var transactionsList: ArrayList<TransactionsItem> = ArrayList()

    var limit = 15
    var skip = 0
    var total = 0

    private val TAG = TransactionHistoryActivity::class.java.simpleName

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this


    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        //SignUpViewModel = ViewModelProvider(this, viewModelFactory).get(LanguageVM::class.java)
        vm = ViewModelProvider(this, viewModelFactory).get(TransactionsVM::class.java)



        if (CommonUtilities.isConnectingToInternet(this) == true) {
            CommonUtilities.showLoader(this)


            transactionsList.clear()

            vm.transactionListApi(
                CommonUtilities.getString(this, Constants.TOKEN).toString(),
                skip,
                limit
            )

            setupObserver()


        } else {
            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
        }


    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun backClicked() {
        onBackPressed()
    }


    private fun setupObserver() {

        vm.responseWalletBalance.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        if (result?.data?.transactions != null) {

                            total = result?.data?.totalNumberofTransactions!!

                            result?.data?.transactions?.let { it1 -> transactionsList.addAll(it1) }


                            if (transactionsList.isNotEmpty()) {

                                binding.cardView.visibility = View.VISIBLE
                                initRecycler(transactionsList)
                                initPagination()

                            } else
                            {
                                binding.rvTransactions.visibility = View.GONE
                                binding.noData.visibility = View.VISIBLE
                            }


                        } else {
                            binding.rvTransactions.visibility = View.GONE
                            binding.noData.visibility = View.VISIBLE


                        }


                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })
    }


    fun initRecycler(list: ArrayList<TransactionsItem>) {


        myAdapter =
            TransactionHistoryAdapter(this, list)
        binding?.rvTransactions?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = myAdapter
        }


    }


    private fun initPagination() {

        binding!!.nestedScroll.isNestedScrollingEnabled = false
        binding!!.nestedScroll.setSmoothScrollingEnabled(true)
        binding!!.nestedScroll.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (v.getChildAt(v.childCount - 1) != null) {
                if (scrollY >= v.getChildAt(v.childCount - 1)
                        .measuredHeight - v.measuredHeight &&
                    scrollY > oldScrollY
                ) {


                    if (transactionsList.size < total) {


                        if (CommonUtilities.isConnectingToInternet(this) == true) {
                            CommonUtilities.showLoader(this)


                            vm.transactionListApi(
                                CommonUtilities.getString(this, Constants.TOKEN).toString(),
                                skip,
                                limit
                            )

                        } else {
                            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
                        }


                    }

                }
            }

        })

    }


}
















