package com.tribU.home.model

import com.google.gson.annotations.SerializedName

data class ResponseUserStatus(

	@field:SerializedName("data")
	val data: StatusData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class User(

	@field:SerializedName("profilePicture")
	val profilePicture: String? = null,

	@field:SerializedName("dwollaCustomerId")
	val dwollaCustomerId: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("phoneNo")
	val phoneNo: String? = null
)

data class StatusData(

	@field:SerializedName("user")
	val user: User? = null
)
