package com.tribU.home.model

import com.google.gson.annotations.SerializedName

data class ResponseGetAllData(

	@field:SerializedName("data")
	val data: AllData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class WaitScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class HomeScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class AccountVerificationScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class OnboardingScreen3(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class VerifyProcessHome(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class PleaseWaitScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class Undefined(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("_id")
	val id: String? = null
)

data class SuccessfullySendScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null,

	@field:SerializedName("paragraph2")
	val paragraph2: String? = null
)

data class LinkAccountScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class AccountSuccessScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class TutorialScreen2(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class TutorialStartScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class LoginOrRegisterScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class CreateAccountScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class OnboardingScreen2(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class VerifyProcessSend(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null,

	@field:SerializedName("paragraph2")
	val paragraph2: String? = null
)

data class WalletScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class VerificationFailureScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class AllData(

	@field:SerializedName("sendingScreen")
	val sendingScreen: SendingScreen? = null,

	@field:SerializedName("linkAccountScreen")
	val linkAccountScreen: LinkAccountScreen? = null,

	@field:SerializedName("successfullySendScreen")
	val successfullySendScreen: SuccessfullySendScreen? = null,

	@field:SerializedName("pleaseWaitScreen")
	val pleaseWaitScreen: PleaseWaitScreen? = null,

	@field:SerializedName("accountVerificationScreen")
	val accountVerificationScreen: AccountVerificationScreen? = null,

	@field:SerializedName("homeScreen")
	val homeScreen: HomeScreen? = null,

	@field:SerializedName("createAccountScreen")
	val createAccountScreen: CreateAccountScreen? = null,

	@field:SerializedName("waitScreen")
	val waitScreen: WaitScreen? = null,

	@field:SerializedName("tutorialStartScreen")
	val tutorialStartScreen: TutorialStartScreen? = null,

	@field:SerializedName("loginOrRegisterScreen")
	val loginOrRegisterScreen: LoginOrRegisterScreen? = null,

	@field:SerializedName("sendScreen")
	val sendScreen: SendScreen? = null,

	@field:SerializedName("walletScreen")
	val walletScreen: WalletScreen? = null,

	@field:SerializedName("undefined")
	val undefined: Undefined? = null,

	@field:SerializedName("onboardingScreen4")
	val onboardingScreen4: OnboardingScreen4? = null,

	@field:SerializedName("verifyProcessSend")
	val verifyProcessSend: VerifyProcessSend? = null,

	@field:SerializedName("letStartScreen")
	val letStartScreen: LetStartScreen? = null,

	@field:SerializedName("onboardingScreen3")
	val onboardingScreen3: OnboardingScreen3? = null,

	@field:SerializedName("onboardingScreen2")
	val onboardingScreen2: OnboardingScreen2? = null,

	@field:SerializedName("onboardingScreen1")
	val onboardingScreen1: OnboardingScreen1? = null,

	@field:SerializedName("tutorialScreen2")
	val tutorialScreen2: TutorialScreen2? = null,

	@field:SerializedName("accountSuccessScreen")
	val accountSuccessScreen: AccountSuccessScreen? = null,

	@field:SerializedName("verificationFailureScreen")
	val verificationFailureScreen: VerificationFailureScreen? = null,

	@field:SerializedName("tutorialScreen1")
	val tutorialScreen1: TutorialScreen1? = null,

	@field:SerializedName("verifyProcessHome")
	val verifyProcessHome: VerifyProcessHome? = null
)

data class OnboardingScreen1(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class TutorialScreen1(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class SendScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class SendingScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class LetStartScreen(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)

data class OnboardingScreen4(

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("heading")
	val heading: String? = null,

	@field:SerializedName("paragraph1")
	val paragraph1: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("screenName")
	val screenName: String? = null
)
