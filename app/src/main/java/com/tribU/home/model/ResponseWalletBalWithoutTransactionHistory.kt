package com.tribU.home.model

import com.google.gson.annotations.SerializedName

data class ResponseWalletBalWithoutTransactionHistory(

	@field:SerializedName("data")
	val data: WalletBalWithoutTransactionHistory? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class WalletBalWithoutTransactionHistory(

	@field:SerializedName("profilePicture")
	val profilePicture: String? = null,

	@field:SerializedName("balanceDetails")
	val balanceDetails: BalanceDetails? = null
)

data class Balance(

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("value")
	val value: String? = null
)

data class BalanceDetails(

	@field:SerializedName("lastUpdated")
	val lastUpdated: String? = null,

	@field:SerializedName("total")
	val total: Total? = null,

	@field:SerializedName("balance")
	val balance: Balance? = null
)

data class Total(

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("value")
	val value: String? = null
)
