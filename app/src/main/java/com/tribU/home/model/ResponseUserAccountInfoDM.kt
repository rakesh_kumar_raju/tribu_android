package com.tribU.home.model

import com.google.gson.annotations.SerializedName

data class ResponseUserAccountInfoDM(

	@field:SerializedName("data")
	val data: DataItem? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class DataItem(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("last4")
	val last4: String? = null,

	@field:SerializedName("account_type")
	val accountType: Any? = null,

	@field:SerializedName("metadata")
	val metadata: Metadata? = null,

	@field:SerializedName("account_holder_name")
	val accountHolderName: String? = null,

	@field:SerializedName("routing_number")
	val routingNumber: String? = null,

	@field:SerializedName("account_holder_type")
	val accountHolderType: String? = null,

	@field:SerializedName("bank_name")
	val bankName: String? = null,

	@field:SerializedName("fingerprint")
	val fingerprint: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("object")
	val object_data: String? = null,

	@field:SerializedName("customer")
	val customer: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null
)

data class Metadata(
	val any: Any? = null
)
