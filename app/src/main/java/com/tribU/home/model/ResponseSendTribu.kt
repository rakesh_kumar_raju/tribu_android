package com.tribU.home.model

import com.google.gson.annotations.SerializedName

data class ResponseSendTribu(

	@field:SerializedName("data")
	val data: SendTribuData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class SendTribuData(

	@field:SerializedName("tribuSentAt")
	val tribuSentAt: String? = null,

	@field:SerializedName("amount")
	val amount: Double? = null,

	@field:SerializedName("senderId")
	val senderId: String? = null,

	@field:SerializedName("ifFromWallet")
	val ifFromWallet: Boolean? = null,

	@field:SerializedName("popUpText")
	val popUpText: PopUpText? = null,

	@field:SerializedName("requestId")
	val requestId: String? = null,

	@field:SerializedName("from")
	val from: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class PopUpText(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
