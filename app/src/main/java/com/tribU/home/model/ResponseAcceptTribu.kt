package com.tribU.home.model

import com.google.gson.annotations.SerializedName

data class ResponseAcceptTribu(

	@field:SerializedName("data")
	val data: AcceptData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class LastResponse(

	@field:SerializedName("transactionType")
	val transactionType: String? = null,

	@field:SerializedName("tribuSentAt")
	val tribuSentAt: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("senderId")
	val senderId: String? = null,

	@field:SerializedName("transactionStatus")
	val transactionStatus: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("transferId")
	val transferId: String? = null,

	@field:SerializedName("recieverId")
	val recieverId: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)



data class AcceptData(

	@field:SerializedName("popUpText")
	val popUpText: PopUpText? = null,

	@field:SerializedName("lastResponse")
	val lastResponse: LastResponse? = null

)
