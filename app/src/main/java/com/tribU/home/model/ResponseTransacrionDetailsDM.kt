package com.tribU.home.model

import com.google.gson.annotations.SerializedName

data class ResponseTransacrionDetailsDM(

	@field:SerializedName("data")
	val data: DataValues? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("error")
	val error: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class ListItem(

	@field:SerializedName("stripeTransferId")
	val stripeTransferId: String? = null,

	@field:SerializedName("tribuSentAt")
	val tribuSentAt: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("stripeChargeId")
	val stripeChargeId: String? = null,

	@field:SerializedName("amount")
	val amount: Float? = null,

	@field:SerializedName("senderId")
	val senderId: String? = null,

	@field:SerializedName("tribuStatus")
	val tribuStatus: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("recieverId")
	val recieverId: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class DataValues(

	@field:SerializedName("list")
	val list: List<ListItem?>? = null,

	@field:SerializedName("totalNumberofTransactions")
	val totalNumberofTransactions: Int? = null
)
