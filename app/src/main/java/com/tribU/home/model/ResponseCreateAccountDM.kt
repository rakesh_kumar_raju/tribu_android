package com.tribU.home.model

import com.google.gson.annotations.SerializedName

data class ResponseCreateAccountDM(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class Data(

	@field:SerializedName("stripeAccountId")
	val stripeAccountId: String? = null,

	@field:SerializedName("stripeBankAccountId")
	val stripeBankAccountId: String? = null,

	@field:SerializedName("phoneNo")
	val phoneNo: String? = null,

	@field:SerializedName("isStripeOnBoardingCompleted")
	val isStripeOnBoardingCompleted: Boolean? = null,

	@field:SerializedName("imgUrl")
	val imgUrl: String? = null,

	@field:SerializedName("firebaseId")
	val firebaseId: String? = null,

	@field:SerializedName("totalTribuRecieved")
	val totalTribuRecieved: Int? = null,

	@field:SerializedName("bankAccountStatus")
	val bankAccountStatus: Boolean? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("stripeCustomerId")
	val stripeCustomerId: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("totalTribuSent")
	val totalTribuSent: Int? = null
)
