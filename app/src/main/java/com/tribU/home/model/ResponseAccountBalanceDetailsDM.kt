package com.tribU.home.model

import com.google.gson.annotations.SerializedName

data class ResponseAccountBalanceDetailsDM(

	@field:SerializedName("data")
	val data: DataList? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class DataList(

	@field:SerializedName("totalPending")
	val totalPending: Float? = null,

	@field:SerializedName("totalTribuRecived")
	val totalTribuRecived: Float? = null,

	@field:SerializedName("totalTribuSent")
	val totalTribuSent: Float? = null
)
