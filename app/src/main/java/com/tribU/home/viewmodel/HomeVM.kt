package com.tribU.home.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.home.model.ResponseAcceptTribu
import com.tribU.home.model.ResponseSendTribu
import com.tribU.home.model.ResponseUserStatus
import com.tribU.home.model.ResponseWalletBalWithoutTransactionHistory
import com.tribU.network.HomeData
import com.tribU.network.ProfileData
import com.tribU.network.Result
import com.tribU.network.uploadImageData
import com.tribU.profile.model.ResponseFundingSources
import com.tribU.profile.model.ResponseLogoutDM
import com.tribU.profile.model.ResponseUploadImageDM
import com.tribU.signup.model.ResponseSendTribuDM
import com.tribU.signup.model.ResponseSignUpSignInDM
import com.tribU.signup.model.ResponseTrubuStatusDM
import com.tribU.transaction.model.ResponseRejectTribUDM
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject

class HomeVM@Inject constructor(private  val data: HomeData,
                                private val errorProvider: ErrorProvider
): ViewModel() {




    private var _responseWalletBal = MutableLiveData<Result<ResponseWalletBalWithoutTransactionHistory>>()
    val responseWalletBal: LiveData<Result<ResponseWalletBalWithoutTransactionHistory>>
        get() = _responseWalletBal


    private var _responseFundingSources = MutableLiveData<Result<ResponseFundingSources>>()
    val responseFundingSources: LiveData<Result<ResponseFundingSources>>
        get() = _responseFundingSources


    private var _responsSendTribuRequest= MutableLiveData<Result<ResponseSendTribu>>()
    val responsSendTribuRequest: LiveData<Result<ResponseSendTribu>>
        get() = _responsSendTribuRequest


    private var _acceptTribuResult = MutableLiveData<Result<ResponseAcceptTribu>>()
    val acceptTribuResult: LiveData<Result<ResponseAcceptTribu>>
        get() = _acceptTribuResult


    private var _rejectTribuResult = MutableLiveData<Result<ResponseRejectTribUDM>>()
    val rejectTribuResult: LiveData<Result<ResponseRejectTribUDM>>
        get() = _rejectTribuResult



    private var _tribuStatus = MutableLiveData<Result<ResponseTrubuStatusDM>>()
    val tribuStatus: LiveData<Result<ResponseTrubuStatusDM>>
        get() = _tribuStatus

    private var _responseUserStatus = MutableLiveData<Result<ResponseUserStatus>>()
    val responseUserStatus: LiveData<Result<ResponseUserStatus>>
        get() = _responseUserStatus







    fun getWalletBalApi(token:String) {
        viewModelScope.launch {
            try {
                _responseWalletBal.postValue(Result.loading())
                val response = data.WalletBalWithoutTransactionHistoryProcess(token)
                _responseWalletBal.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _responseWalletBal.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }





    fun getFundingSources(token:String) {
        viewModelScope.launch {
            try {
                _responseFundingSources.postValue(Result.loading())
                val response = data.GetFundingSourcesProcess(token)
                _responseFundingSources.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _responseFundingSources.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }




    fun sendTribuRequestApi(token:String,obj:JsonObject) {
        viewModelScope.launch {
            try {
                _responsSendTribuRequest.postValue(Result.loading())
                val response = data.SendTribuRequest(token,obj)
                _responsSendTribuRequest.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _responsSendTribuRequest.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }






    fun AcceptTribuApi(token: String,amount:JsonObject) {
        viewModelScope.launch {
            try {
                _acceptTribuResult.postValue(Result.loading())
                val response = data.AcceptTribuProcess(token,amount)
                _acceptTribuResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _acceptTribuResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }

    fun RejectTribuApi(token: String,tribuRequestId:JsonObject) {
        viewModelScope.launch {
            try {
                _rejectTribuResult.postValue(Result.loading())
                val response = data.RejectTribuProcess(token,tribuRequestId)
                _rejectTribuResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _rejectTribuResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }




    fun GetTribuStatusApi(token: String,requestId:String) {
        viewModelScope.launch {
            try {
                _tribuStatus.postValue(Result.loading())
                val response = data.TribuStatusProcess(token,requestId)
                _tribuStatus.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _tribuStatus.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }


    fun getUserStatus(token:String) {
        viewModelScope.launch {
            try {
                _responseUserStatus.postValue(Result.loading())
                val response = data.GetUserStatusProcess(token)
                _responseUserStatus.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _responseUserStatus.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }










}
