package com.tribU.home.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tribU.errorProvider.ErrorProvider
import com.tribU.network.CreateAccountData
import com.tribU.signup.model.ResponseSignUpSignInDM
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.tribU.network.Result

class CreateAccountFragVM @Inject constructor(private  val create_account_data: CreateAccountData,
                                              private val errorProvider: ErrorProvider
): ViewModel() {

    private var _createAccountResult = MutableLiveData<Result<ResponseSignUpSignInDM>>()
    val createAccountResult: LiveData<Result<ResponseSignUpSignInDM>>
        get() = _createAccountResult

    fun createAccountApi(token: String,first_name:String,
                         last_name:String,country_code:String,phone:String,email:String,account_no:String
                         ,account_holder_name:String,
                         account_holder_type:String,routing_no:String,mcc:String) {
        viewModelScope.launch {
            try {
                _createAccountResult.postValue(Result.loading())
                val response = create_account_data.AccountCreateProcess("application/x-www-form-urlencoded",token,
                    first_name,last_name,country_code,phone,
                email,account_no,account_holder_name,account_holder_type,routing_no,mcc)
                _createAccountResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _createAccountResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }



}
