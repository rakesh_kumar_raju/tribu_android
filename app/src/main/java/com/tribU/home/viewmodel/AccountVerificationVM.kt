package com.tribU.home.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tribU.errorProvider.ErrorProvider
import com.tribU.network.AccountVerificationData
import com.tribU.network.Result
import com.tribU.signup.model.ResponseSignUpSignInDM
import kotlinx.coroutines.launch
import javax.inject.Inject

class AccountVerificationVM @Inject constructor(private  val account_verification_data: AccountVerificationData,
                                                private val errorProvider: ErrorProvider
): ViewModel() {

    private var _verifyAccountResult = MutableLiveData<Result<ResponseSignUpSignInDM>>()
    val verifyAccountResult: LiveData<Result<ResponseSignUpSignInDM>>
        get() = _verifyAccountResult

    fun verifyAccountApi(token: String,amount1:String,
                         amount2:String) {
        viewModelScope.launch {
            try {
                _verifyAccountResult.postValue(Result.loading())
                val response = account_verification_data.AccountVerificationProcess("application/x-www-form-urlencoded",token,
                    amount1, amount2)
                _verifyAccountResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _verifyAccountResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }
}
