package com.tribU.home.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tribU.errorProvider.ErrorProvider
import com.tribU.home.model.ResponseUserAccountInfoDM
import com.tribU.network.GetUserAccountData
import com.tribU.network.Result
import kotlinx.coroutines.launch
import javax.inject.Inject

class AddAccountFragVM @Inject constructor(private  val get_user_bank_data: GetUserAccountData,
                                           private val errorProvider: ErrorProvider
): ViewModel() {

    private var _getBankInfoResult = MutableLiveData<Result<ResponseUserAccountInfoDM>>()
    val getBankInfoResult: LiveData<Result<ResponseUserAccountInfoDM>>
        get() = _getBankInfoResult

    fun GetBankInfoApi(token: String) {
        viewModelScope.launch {
            try {
                _getBankInfoResult.postValue(Result.loading())
                val response = get_user_bank_data.UserAccountInfoProcess(token)
                _getBankInfoResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _getBankInfoResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }



}
