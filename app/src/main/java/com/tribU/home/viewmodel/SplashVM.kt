package com.tribU.home.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.home.model.ResponseGetAllData
import com.tribU.network.HomeData
import com.tribU.network.ProfileData
import com.tribU.network.Result
import com.tribU.network.uploadImageData
import com.tribU.profile.model.ResponseGetWalletBalance
import com.tribU.profile.model.ResponseLogoutDM
import com.tribU.profile.model.ResponseUploadImageDM
import com.tribU.signup.model.ResponseSignUpSignInDM
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject

class SplashVM @Inject constructor(
    private val data: HomeData.SplashData,
    private val errorProvider: ErrorProvider
) : ViewModel() {


    private var _responseAllData = MutableLiveData<Result<ResponseGetAllData>>()
    val responseAllData: LiveData<Result<ResponseGetAllData>>
        get() = _responseAllData


    fun getdataApi() {
        viewModelScope.launch {
            try {
                _responseAllData.postValue(Result.loading())
                val response = data.getAlldataProcess()
                _responseAllData.postValue(Result.success(response))
            } catch (exception: Exception) {
                exception.message?.let { Log.d("get_error", it) }
                _responseAllData.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }


}
