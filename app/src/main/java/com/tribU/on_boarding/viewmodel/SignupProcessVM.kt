package com.bishaan.onboarding.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.network.CreateAccountData
import com.tribU.network.Result
import com.tribU.signup.model.ResponseCreateVerifiedUser
import com.tribU.signup.model.ResponseGetStates
import kotlinx.coroutines.launch
import javax.inject.Inject

class SignupProcessVM @Inject constructor(
    private val data: CreateAccountData,
    private val errorProvider: ErrorProvider
) : ViewModel() {


    /**
     * get region  api
     * */
    private var _responseRegion = MutableLiveData<com.tribU.network.Result<ResponseGetStates>>()
    val responseRegion: LiveData<com.tribU.network.Result<ResponseGetStates>>
        get() = _responseRegion


    /**
     * get region api
     * */
    fun getStates() {
        viewModelScope.launch {
            try {
                _responseRegion.postValue(com.tribU.network.Result.loading())
                val response = data.getStatesProcess()
                _responseRegion.postValue(com.tribU.network.Result.success(response))
            } catch (exception: Exception) {
                _responseRegion.postValue(
                    com.tribU.network.Result.error(
                        errorProvider.getErrorMessage(
                            exception
                        )
                    )
                )
            }
        }
    }



    private var _responseCreateUser = MutableLiveData<Result<ResponseCreateVerifiedUser>>()
    val responseCreateUser: LiveData<Result<ResponseCreateVerifiedUser>>
        get() = _responseCreateUser


    fun createVerifiedUserApi(token:String,obj: JsonObject) {
        viewModelScope.launch {
            try {
                _responseCreateUser.postValue(Result.loading())
                val response = data.CreateVerfifedUserProcess(token,obj)
                _responseCreateUser.postValue(Result.success(response))
            } catch (exception: Exception) {
                exception.message?.let { Log.d("get_error", it) }
                _responseCreateUser.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }


}