package com.tribU.on_boarding.callbacks

interface LetsGetStartedViewCallbacks {

    fun nextClick()
}