package com.bishaan.onboarding.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.tribU.on_boarding.model.ViewPagerDM
import com.tribU.R
import android.text.TextPaint
import com.bumptech.glide.Glide
import com.tribU.SplashNewActivity


class OnboardingPagerAdapter(context: Context, pagerList:ArrayList<ViewPagerDM>): PagerAdapter() {
    internal var context: Context
    internal var pager:ArrayList<ViewPagerDM>

    init{
        this.context = context
        this.pager = pagerList
    }
    override fun isViewFromObject(view: View, o:Any):Boolean {
        return view === o
    }
    override fun instantiateItem(container: ViewGroup, position:Int):Any {
        val view = LayoutInflater.from(context).inflate(R.layout.onboarding_pager_item, container, false)
        val imageView = view.findViewById(R.id.iv_onboarding) as ImageView
        val heading1 = view.findViewById(R.id.tv_heading1) as TextView
        val heading2 = view.findViewById(R.id.tv_heading2) as TextView
        val text1 = view.findViewById(R.id.tv_subTitle1) as TextView
        val text2 = view.findViewById(R.id.tv_subTitle2) as TextView
        imageView.setBackgroundResource(pager.get(position).image!!)




        if (position==0)
        {
            Glide.with(context)
                .load(SplashNewActivity.appData.onboardingScreen1!!.imgUrl)
                .into(imageView)

        }
        if (position==1)
        {
            Glide.with(context)
                .load(SplashNewActivity.appData.onboardingScreen2!!.imgUrl)
                .into(imageView)

        }
        if (position==2)
        {
            Glide.with(context)
                .load(SplashNewActivity.appData.onboardingScreen3!!.imgUrl)
                .into(imageView)

        }
        if (position==3)
        {
            Glide.with(context)
                .load(SplashNewActivity.appData.onboardingScreen4!!.imgUrl)
                .into(imageView)

        }


        if (position==0)
        {
            heading1.visibility=View.VISIBLE
            text1.visibility=View.VISIBLE

            heading2.visibility=View.INVISIBLE
            text2.visibility=View.INVISIBLE
        }else
        {
            heading1.visibility=View.INVISIBLE
            text1.visibility=View.INVISIBLE

            heading2.visibility=View.VISIBLE
            text2.visibility=View.VISIBLE
        }


        val paint: TextPaint = heading1.getPaint()
        val width = paint.measureText(pager.get(position).text1!!)


        val textShader: Shader = LinearGradient(0f, 0f, width, heading1.textSize, intArrayOf(
            Color.parseColor("#FFFFFFFF"),
            Color.parseColor("#FECC2F")
        ), null, Shader.TileMode.REPEAT)

        heading1.paint.setShader(textShader)
        heading1.setText(pager.get(position).text1!!)
        text1.setText(pager.get(position).text2!!)



        val paint2: TextPaint = heading2.getPaint()
        val width2 = paint2.measureText(pager.get(position).text1!!)


        val textShader2: Shader = LinearGradient(0f, 0f, width2, heading2.textSize, intArrayOf(
            Color.parseColor("#FFFFFFFF"),
            Color.parseColor("#FECC2F")
        ), null, Shader.TileMode.REPEAT)

        heading2.paint.setShader(textShader2)
        heading2.setText(pager.get(position).text1!!)
        text2.setText(pager.get(position).text2!!)


       /* if(position==pager.size-1)
        {

            heading1.visibility=View.INVISIBLE
            heading2.visibility=View.INVISIBLE
        }
        else{

            heading1.visibility=View.VISIBLE
            heading2.visibility=View.VISIBLE
        }*/


        container.addView(view)
        return view
    }



    override fun destroyItem(container: ViewGroup, position:Int, `object`:Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return pager.size
    }

    override fun getItemPosition(`object`:Any):Int {
        return super.getItemPosition(`object`)
    }
}