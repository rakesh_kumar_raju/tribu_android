package com.bishaan.onboarding.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.bishaan.onboarding.adapter.OnboardingPagerAdapter
import com.tribU.on_boarding.model.ViewPagerDM
import com.bishaan.onboarding.viewmodel.OnboardingVM
import com.tribU.R
import com.tribU.SplashNewActivity
import com.tribU.databinding.OnboardingScreenLayoutBinding
import com.tribU.login.view.LoginActivity
import com.tribU.on_boarding.view.LetsGetStartedActivity
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants


class OnboardingActivity : AppCompatActivity() {
    var binding: OnboardingScreenLayoutBinding? = null
    var vm: OnboardingVM? = null
    var whichScreen: String? = null
    var pos: Int? = 0
    var pager_itemList = ArrayList<ViewPagerDM>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w: Window = window
            w.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
        }
        binding = DataBindingUtil.setContentView(this, R.layout.onboarding_screen_layout)


        /**
         *
         *   going to onboarding screen from splash and tutorial.
         *   Dont show skip and dont go to LetsGetStarted screen when coming from tutorial screen.
         *
         * */

        if (intent.getStringExtra(Constants.FROM_WHICH_SCREEN) == "tutorial") {

            binding?.tvSkip?.visibility = View.GONE

        }


        if (SplashNewActivity.appData!=null)
        {

            pager_itemList.clear()

            if (intent.getStringExtra(Constants.FROM_WHICH_SCREEN) == "tutorial") {



                pager_itemList.add(
                    ViewPagerDM(
                        R.mipmap.onboarding1,
                        SplashNewActivity.appData.onboardingScreen1!!.heading,
                        SplashNewActivity.appData.onboardingScreen1!!.paragraph1

                    )
                )
                pager_itemList.add(
                    ViewPagerDM(
                        R.mipmap.onboarding2,
                        SplashNewActivity.appData.onboardingScreen2!!.heading,
                        SplashNewActivity.appData.onboardingScreen2!!.paragraph1
                    )
                )
                pager_itemList.add(
                    ViewPagerDM(
                        R.mipmap.onboarding3,
                        SplashNewActivity.appData.onboardingScreen3!!.heading,
                        SplashNewActivity.appData.onboardingScreen3!!.paragraph1
                    )
                )
                pager_itemList.add(
                    ViewPagerDM(
                        R.mipmap.onboarding4,
                        SplashNewActivity.appData.onboardingScreen4!!.heading,
                        SplashNewActivity.appData.onboardingScreen4!!.paragraph1
                    )
                )


            }else
            {

                pager_itemList.add(
                    ViewPagerDM(
                        R.mipmap.onboarding1,
                        SplashNewActivity.appData.onboardingScreen1!!.heading,
                        SplashNewActivity.appData.onboardingScreen1!!.paragraph1

                    )
                )
                pager_itemList.add(
                    ViewPagerDM(
                        R.mipmap.onboarding2,
                        SplashNewActivity.appData.onboardingScreen2!!.heading,
                        SplashNewActivity.appData.onboardingScreen2!!.paragraph1
                    )
                )
                pager_itemList.add(
                    ViewPagerDM(
                        R.mipmap.onboarding3,
                        SplashNewActivity.appData.onboardingScreen3!!.heading,
                        SplashNewActivity.appData.onboardingScreen3!!.paragraph1
                    )
                )
                pager_itemList.add(
                    ViewPagerDM(
                        R.mipmap.onboarding4,
                        SplashNewActivity.appData.onboardingScreen4!!.heading,
                        SplashNewActivity.appData.onboardingScreen4!!.paragraph1
                    )
                )

            }




        }else
        {
            pager_itemList.clear()


            pager_itemList.add(
                ViewPagerDM(
                    R.mipmap.onboarding1,
                    "Welcome to\nTribU",
                    "Let’s start changing the world...."
                )
            )
            pager_itemList.add(
                ViewPagerDM(
                    R.mipmap.onboarding2,
                    "World first\napp",
                    "The World's first mobile app, all\nabout giving anonymously!"
                )
            )
            pager_itemList.add(
                ViewPagerDM(
                    R.mipmap.onboarding3,
                    "Send Gifts of\nCash",
                    "Now, right from your phone, you can\nsend anonymous gifts of cash to\npeople!"
                )
            )
            pager_itemList.add(
                ViewPagerDM(
                    R.mipmap.onboarding4,
                    "Here's How it\nworks",
                    "Once downloaded and registered,\nit's as simple as choosing a dollar\namount and clicking send! Any \"1\"\nother TribU user will receive the cash!\nTotally random and completely\nanonymous."
                )
            )

        }



        binding?.tvNext!!.setOnClickListener {

            binding?.viewpager!!.currentItem = 1;


        }

        val adapter = OnboardingPagerAdapter(this, pager_itemList)
        binding?.viewpager!!.adapter = adapter
        //  binding?.dotsIndicator!!.setViewPager(binding?.viewpager!!)
        binding?.viewpager!!.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == pager_itemList.size - 1) {
                    //   whichScreen="last"
                    //  binding?.tvLogin!!.visibility=View.VISIBLE
                    //   binding?.tvSignUp!!.visibility=View.VISIBLE


                    binding?.tvNext!!.setOnClickListener {


                        goToLetsGetStartedScreen()


                    }
                } else {
                    whichScreen = "non-last"
                    pos = position
                    //  binding?.tvLogin!!.visibility=View.INVISIBLE
                    //  binding?.tvSignUp!!.visibility=View.INVISIBLE

                    binding?.tvNext!!.setOnClickListener {

                        binding?.viewpager!!.setCurrentItem(binding?.viewpager!!.getCurrentItem() + 1);

                    }
                }
            }

        })


        binding?.tvSkip!!.setOnClickListener {

            CommonUtilities.fireActivityIntent(
                this,
                Intent(this, LetsGetStartedActivity::class.java),
                isFinish = true,
                isForward = true
            )
        }


    }

    fun goToLetsGetStartedScreen() {


        if (intent.getStringExtra(Constants.FROM_WHICH_SCREEN) == "tutorial") {

            onBackPressed()

        } else {

            CommonUtilities.fireActivityIntent(
                this,
                Intent(this, LetsGetStartedActivity::class.java),
                isFinish = true,
                isForward = true
            )

        }
    }

}