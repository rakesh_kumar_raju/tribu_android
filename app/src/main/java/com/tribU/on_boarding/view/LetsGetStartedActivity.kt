package com.tribU.on_boarding.view

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Build
import android.text.TextPaint
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tribU.R
import com.tribU.SplashNewActivity
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.LetsGetStartedActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.login.view.LoginActivity
import com.tribU.on_boarding.callbacks.LetsGetStartedViewCallbacks
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import java.util.*


class LetsGetStartedActivity :
    BaseDataBindingActivity<LetsGetStartedActivityDataBinding>(R.layout.activity_lets_get_started),
    LetsGetStartedViewCallbacks {


    lateinit var device_token: String
    private val TAG = LetsGetStartedActivity::class.java.simpleName
    var firebaseId: String? = null

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this
        CommonUtilities.getFirebaseToken(this)

        CommonUtilities.putBoolean(this, Constants.IS_OLD_USER, true)


        val paint: TextPaint = binding.tvHeading.getPaint()
        val width = paint.measureText("Let's\nget started")


        val textShader: Shader = LinearGradient(
            0f, 0f, width, binding.tvHeading.textSize, intArrayOf(
                Color.parseColor("#FFFFFFFF"),
                Color.parseColor("#FECC2F")
            ), null, Shader.TileMode.REPEAT
        )

        binding.tvHeading.paint.setShader(textShader)
        //    binding.tvHeading.text = "Let's\nget started"


        if (!SplashNewActivity.appData.letStartScreen!!.imgUrl.isNullOrEmpty()) {
            binding.tvHeading.text = SplashNewActivity.appData.letStartScreen!!.heading

        } else {
            binding.tvHeading.text = "Let's\nget started"
        }



        Glide.with(this)
            .load(SplashNewActivity.appData.letStartScreen!!.imgUrl)
            .into(binding.ivBg)
    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        //SignUpViewModel = ViewModelProvider(this, viewModelFactory).get(LanguageVM::class.java)
    }


    private fun showBottomSheet() {


        /*   //   boolean[] choicesInitial = {false, false, false};
            AlertDialog.Builder alertDialogBuilder = new MaterialAlertDialogBuilder(getActivity())
                    .setTitle("Select Popular Trends")
                    .setPositiveButton("DONE", null)
                    .setSingleChoiceItems(list, selectedTrend, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            int id = which;
                            selectedTrend = id;
                            binding.tvTrandType.setText(list.get(id));

                        }
                    });
            alertDialogBuilder.show();
*/

    var bottomSheetDialog = BottomSheetDialog(this)


    @SuppressLint("InflateParams")
    val view: View =
        layoutInflater.inflate(R.layout.dialog_lets_get_started, null)


/*
       var imageview = (view.findViewById(R.id.img) as ImageView)

        // Sync
        val bitmap = Blurry.with(this)
            .radius(10)
            .sampling(8)
            .capture(findViewById(R.id.img)).get()
        imageview.setImageDrawable(BitmapDrawable(resources, bitmap))
*/


    /*   val requestOptions = RequestOptions()
       requestOptions.transform(BlurTransformation(50))

       Glide.with(this)
           .applyDefaultRequestOptions(requestOptions)
           .load(R.drawable.bg_lets_get_started)
           .into(view.findViewById(R.id.iv_background) as ImageView);*/




    if (SplashNewActivity.appData != null)
    {
        (view.findViewById(R.id.tv_title) as TextView).text =
            SplashNewActivity.appData.loginOrRegisterScreen!!.heading
        (view.findViewById(R.id.tv_message) as TextView).text =
            SplashNewActivity.appData.loginOrRegisterScreen!!.paragraph1

    }




    (view.findViewById(R.id.bt_login) as View).setOnClickListener{

        bottomSheetDialog!!.dismiss()

        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, LoginActivity::class.java)
                .putExtra(Constants.FROM_WHICH_SCREEN, Constants.LOGIN),

            isFinish = false,
            isForward = true
        )

    }
    (view.findViewById(R.id.bt_register) as View).setOnClickListener{

        bottomSheetDialog!!.dismiss()
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, LoginActivity::class.java)
                .putExtra(Constants.FROM_WHICH_SCREEN, Constants.SIGNUP),
            isFinish = false,
            isForward = true
        )

    }
    bottomSheetDialog!!.setContentView(view)

    (view?.parent as View).setBackgroundColor(Color.TRANSPARENT)


    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
    {
        Objects.requireNonNull(bottomSheetDialog!!.window)!!
            .addFlags(WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_ALWAYS)
    }
    /*  bottomSheetDialog!!.setOnDismissListener {
          bottomSheetDialog = null
      }*/
    bottomSheetDialog!!.show()
}

override fun nextClick() {
    showBottomSheet()
}

override fun onBackPressed() {
    //super.onBackPressed()
    finishAffinity()
}

}
















