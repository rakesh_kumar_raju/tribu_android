package com.tribU.share.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.ShareFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.share.callbacks.MyShareFragCallbacks
import com.tribU.share.callbacks.ShareFragViewsCallback
import com.tribU.share.viewmodel.ShareFragVM
import javax.inject.Inject

class ShareFragment : BaseDataBindingFragment<ShareFragDataBinding>(R.layout.share_screen_layout),
    ShareFragViewsCallback {
    @set:Inject
    lateinit var factory: ViewModelFactory
    public var view_model: ShareFragVM? = null
    var mContext: Context? = null


    companion object {
        fun newInstance() =
            ShareFragment()
    }

    private var callBackListener: MyShareFragCallbacks? = null
//private val viewModel by viewModels<MyLibraryFragVM>(factoryProducer = { viewModelFactory })


    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
        try {
            callBackListener = activity as MyShareFragCallbacks
        } catch (e: ClassCastException) {
            throw ClassCastException(
                activity.toString()
                        + "  must implement MyLibraryCallbacks"
            )
        }
    }


    override fun onDataBindingCreated() {
        binding.callback = this
        share()

    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }

fun share(){
    val intent= Intent()
    intent.action= Intent.ACTION_SEND
    intent.putExtra(Intent.EXTRA_TEXT,"Hey Check out this Great app:")
    intent.type="text/plain"
    context!!.startActivity(Intent.createChooser(intent,"Share To:"))

}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as BottomNavigationActivity).binding!!.bottomNavigationMain.menu.getItem(3).isChecked = true

    }

}

