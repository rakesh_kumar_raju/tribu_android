package com.tribU.network

import com.google.gson.JsonObject
import com.tribU.home.model.*
import com.tribU.profile.model.*
import com.tribU.signup.model.*
import com.tribU.transaction.model.ResponseRejectTribUDM
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import javax.inject.Inject

class signInSignUpData @Inject constructor(private val api: ApiCalls) {

    suspend fun SignUpProcess(   // SignUP Data
        obj: JsonObject
    ): ResponseSignUpSignInDM2? = withContext(Dispatchers.IO) {
        val data =
            api.UserSignUp(obj)
        data
    }

    // SignIn Data
    suspend fun SignInProcess(
        obj: JsonObject
    ): ResponseSignUpSignInDM2? = withContext(Dispatchers.IO) {
        val sign_in_data =
            api.UserSignIn(obj)
        sign_in_data
    }
}


class uploadImageData @Inject constructor(private val api: ApiCalls) {
    //Upload Profile pic
    suspend fun UploadProfilePicProcess(
        files: MultipartBody.Part
    ): ResponseUploadImageDM? = withContext(Dispatchers.IO) {
        val upload_image_data = api.UploadProfilePic(
            files
        )
        upload_image_data
    }


    // Update Profile Pic
    suspend fun UpdateProfilePicProcess(
        content_type: String,
        token: String,
        imgUrl: String

    ): ResponseSignUpSignInDM? = withContext(Dispatchers.IO) {
        val update_profile_pic_data = api.UpdateProfilePic(content_type, token, imgUrl)
        update_profile_pic_data
    }


    // Update Profile Pic
    suspend fun LogoutProcess(
        token: String, obj: JsonObject
    ): ResponseLogoutDM? =
        withContext(Dispatchers.IO) {
            val data = api.logout(token)
            data
        }


}


class TransactionData @Inject constructor(private val api: ApiCalls) {
    //Account Balance Detail
    suspend fun AccntBalanceDetialProcess(
        token: String
    ): ResponseAccountBalanceDetailsDM? = withContext(Dispatchers.IO) {
        val accnt_balance_data = api.AccountBalanceDetails(
            token
        )
        accnt_balance_data
    }

    // Transaction List
    suspend fun GetTransactionList(
        token: String,
        startDate: String,
        endDate: String,
        skip: Int,
        limit: Int
    ):
            ResponseTransacrionDetailsDM? = withContext(Dispatchers.IO) {
        val transaction_list_data = api.UserTransactionList(token, startDate, endDate, skip, limit)
        transaction_list_data
    }
}


class SendTribuData @Inject constructor(private val api: ApiCalls) {

//    suspend fun SendTribuProcess(
//        token: String,
//        amount: JsonObject
//    ): ResponseSendTribuDM? = withContext(Dispatchers.IO) {
//        val send_tribu_data = api.SendTribu(
//            token, amount
//        )
//        send_tribu_data
//    }

    /*  suspend fun SendTribuRequestProcess(
          token: String,
          amount: JsonObject
      ): ResponseSendTribuDM? = withContext(Dispatchers.IO) {
          val send_tribu_data = api.SendTribuRequest(
              token, amount
          )
          send_tribu_data
      }*/


    suspend fun TribuStatusProcess(
        token: String,
        requestId: String
    ): ResponseTrubuStatusDM? = withContext(Dispatchers.IO) {
        val send_tribu_data = api.CheckStatus(
            token, requestId
        )
        send_tribu_data
    }


}


class BottomNavigationData @Inject constructor(private val api: ApiCalls) {

    suspend fun AcceptTribuProcess(
        token: String,
        amount: JsonObject
    ): ResponseAcceptTribu? = withContext(Dispatchers.IO) {
        val accept_tribu_data = api.AcceptTribu(
            token, amount
        )
        accept_tribu_data
    }


    suspend fun RejectTribuProcess(
        token: String,
        tribuRequestId: JsonObject
    ): ResponseRejectTribUDM? = withContext(Dispatchers.IO) {
        val reject_tribu_data = api.RejectTribu(
            token, tribuRequestId
        )
        reject_tribu_data
    }


    suspend fun TribuStatusProcess(
        token: String,
        requestId: String
    ): ResponseTrubuStatusDM? = withContext(Dispatchers.IO) {
        val send_tribu_data = api.CheckStatus(
            token, requestId
        )
        send_tribu_data
    }


}


class CreateAccountData @Inject constructor(private val api: ApiCalls) {


    suspend fun getStatesProcess(
    ): ResponseGetStates? = withContext(Dispatchers.IO) {
        val data = api.getStates()
        data
    }

    suspend fun CreateVerfifedUserProcess(
        token:String,
        obj: JsonObject
    ): ResponseCreateVerifiedUser? = withContext(Dispatchers.IO) {
        val data = api.CreateVerifiedUser(
            token,
            obj
        )
        data
    }



    suspend fun AccountCreateProcess(
        content_type: String,
        token: String,
        firstName: String,
        lastName: String,
        country_code: String,
        phoneNo: String,
        email: String,
        accountNumber: String,
        accountHolderName: String,
        accountHolderType: String,
        routingNumber: String,
        mcc: String
    ): ResponseSignUpSignInDM? = withContext(Dispatchers.IO) {
        val accnt_create_data = api.CreateStripeAccount(
            content_type,
            token,
            firstName,
            lastName,
            country_code,
            phoneNo,
            email,
            //   mcc,
            accountHolderName,
            accountHolderType,
            accountNumber,
            routingNumber
        )
        accnt_create_data
    }

}

class GetUserAccountData @Inject constructor(private val api: ApiCalls) {

    suspend fun UserAccountInfoProcess(
        token: String

    ): ResponseUserAccountInfoDM? = withContext(Dispatchers.IO) {
        val user_account_info = api.UserBankInfo(
            token
        )
        user_account_info
    }
}


class AccountVerificationData @Inject constructor(private val api: ApiCalls) {

    suspend fun AccountVerificationProcess(
        content_type: String,
        token: String,
        amount1: String,
        amount2: String

    ): ResponseSignUpSignInDM? = withContext(Dispatchers.IO) {
        val account_verification_info = api.AccountVerification(
            content_type,
            token, amount1, amount2
        )
        account_verification_info
    }
}


class ProfileData @Inject constructor(private val api: ApiCalls) {


    /*   //Upload Profile pic
       suspend fun UploadProfilePicProcess(
           files: MultipartBody.Part
       ): ResponseUploadImageDM? = withContext(Dispatchers.IO) {
           val upload_image_data = api.UploadProfilePic(
               files
           )
           upload_image_data
       }


       // Update Profile Pic
       suspend fun UpdateProfilePicProcess(
           content_type: String,
           token: String,
           imgUrl: String

       ): ResponseSignUpSignInDM? = withContext(Dispatchers.IO) {
           val update_profile_pic_data = api.UpdateProfilePic(content_type, token, imgUrl)
           update_profile_pic_data
       }
   */


    suspend fun AddOrTransferMoneyProcess(
        token: String,
        obj: JsonObject
    ): ResponseAddOrTransferMoney? = withContext(Dispatchers.IO) {
        val data = api.addOrTransferMoney(
            token, obj
        )
        data
    }


    suspend fun GetFundingSourcesProcess(
        token: String
    ): ResponseFundingSources? =
        withContext(Dispatchers.IO) {
            val data = api.getFundingSources(token)
            data
        }


    suspend fun SendTribuRequest(
        token: String,
        obj: JsonObject
    ): ResponseSendTribu? = withContext(Dispatchers.IO) {
        val data = api.sentTribuRequest(
            token, obj
        )
        data
    }


    // Transaction List
    suspend fun GetTransactions(
        token: String,
        skip: Int,
        limit: Int
    ):
            ResponseGetWalletBalance? = withContext(Dispatchers.IO) {
        val transaction_list_data = api.transactionList(token, skip, limit)
        transaction_list_data
    }


    suspend fun LogoutProcess(
        token: String
    ): ResponseLogoutDM? =
        withContext(Dispatchers.IO) {
            val data = api.logout(token)
            data
        }


    //Upload Profile pic
    suspend fun UploadProfilePicProcess(
        files: MultipartBody.Part
    ): ResponseUploadImageDM? = withContext(Dispatchers.IO) {
        val upload_image_data = api.UploadProfilePic(
            files
        )
        upload_image_data
    }


    // Update Profile Pic
    suspend fun UpdateProfilePicProcess(
        content_type: String,
        token: String,
        imgUrl: String

    ): ResponseSignUpSignInDM? = withContext(Dispatchers.IO) {
        val update_profile_pic_data = api.UpdateProfilePic(content_type, token, imgUrl)
        update_profile_pic_data
    }


}

class HomeData @Inject constructor(private val api: ApiCalls) {


    suspend fun WalletBalWithoutTransactionHistoryProcess(
        token: String
    ): ResponseWalletBalWithoutTransactionHistory? =
        withContext(Dispatchers.IO) {
            val data = api.walletBalanceWithoutTransactionHistory(token)
            data
        }


    suspend fun GetFundingSourcesProcess(
        token: String
    ): ResponseFundingSources? =
        withContext(Dispatchers.IO) {
            val data = api.getFundingSources(token)
            data
        }

    suspend fun SendTribuRequest(
        token: String,
        obj: JsonObject
    ): ResponseSendTribu? = withContext(Dispatchers.IO) {
        val data = api.sentTribuRequest(
            token, obj
        )
        data
    }


    suspend fun AcceptTribuProcess(
        token: String,
        amount: JsonObject
    ): ResponseAcceptTribu? = withContext(Dispatchers.IO) {
        val accept_tribu_data = api.AcceptTribu(
            token, amount
        )
        accept_tribu_data
    }


    suspend fun RejectTribuProcess(
        token: String,
        tribuRequestId: JsonObject
    ): ResponseRejectTribUDM? = withContext(Dispatchers.IO) {
        val reject_tribu_data = api.RejectTribu(
            token, tribuRequestId
        )
        reject_tribu_data
    }


    suspend fun TribuStatusProcess(
        token: String,
        requestId: String
    ): ResponseTrubuStatusDM? = withContext(Dispatchers.IO) {
        val send_tribu_data = api.CheckStatus(
            token, requestId
        )
        send_tribu_data
    }


    suspend fun GetUserStatusProcess(
        token: String
    ): ResponseUserStatus? = withContext(Dispatchers.IO) {
        val data = api.getUserStatus(token)
        data
    }

    class SplashData @Inject constructor(private val api: ApiCalls) {


        suspend fun getAlldataProcess(
        ): ResponseGetAllData? = withContext(Dispatchers.IO) {
            val data = api.getAllData()
            data
        }

    }



}





