package com.tribU.network

import com.google.gson.JsonObject
import com.tribU.home.model.*
import com.tribU.profile.model.*
import com.tribU.signup.model.*
import com.tribU.transaction.model.ResponseRejectTribUDM
import okhttp3.MultipartBody
import retrofit2.http.*

interface ApiCalls {


/*
    //SignUp
    @FormUrlEncoded
    @POST("auth/signup")
    suspend fun UserSignUp(
        @Header("Content-Type") content_type: String,
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("countryCode") countryCode: String,
        @Field("phoneNo") phoneNo: String,
        @Field("firebaseId") firebaseId: String,
        @Field("deviceType") deviceType: String,
        @Field("deviceToken") deviceToken: String
    ): ResponseSignUpSignInDM
*/


/*
    //SignIn
    @FormUrlEncoded
    @POST("auth/signin")
    suspend fun UserSignIn(
      //  @Header("Content-Type") content_type: String,
        @Field("countryCode") countryCode: String,
        @Field("phoneNo") phoneNo: String,
        @Field("firebaseId") firebaseId: String,
        @Field("deviceType") deviceType: String,
        @Field("deviceToken") deviceToken: String
    ): ResponseSignUpSignInDM
*/


    //SignUp
    @POST("auth/signup")
    suspend fun UserSignUp(
        @Body obj: JsonObject
    ): ResponseSignUpSignInDM2

    //SignIn
    @POST("auth/signin")
    suspend fun UserSignIn(
        @Body obj: JsonObject
    ): ResponseSignUpSignInDM2


    //SignIn
    @POST("dwolla/createVerifiedUser")
    suspend fun CreateVerifiedUser(
        @Header("token") token: String,
        @Body obj: JsonObject
    ): ResponseCreateVerifiedUser




    //upload Profile Picture
    @Multipart
    @POST("auth/upload")
    suspend fun UploadProfilePic(
        @Part files: MultipartBody.Part
    ): ResponseUploadImageDM


    //update profile pic
    @FormUrlEncoded
    @POST("auth/updateProfilePicture")
    suspend fun UpdateProfilePic(
        @Header("Content-Type") content_type: String,
        @Header("token") token: String,
        @Field("imgUrl") imgUrl: String
    ): ResponseSignUpSignInDM


    //create Account
    @FormUrlEncoded
    @POST("auth/connectStripeAccount")
    suspend fun CreateStripeAccount(
        @Header("Content-Type") content_type: String,
        @Header("token") token: String,
        @Field("firstName") firstName: String,
        @Field("lastName") lastName: String,
        @Field("countryCode") countryCode: String,
        @Field("phoneNo") phoneNo: String,
        @Field("email") email: String,
        //  @Field("mcc") mcc: String,
        @Field("accountHolderName") accountHolderName: String,
        @Field("accountHolderType") accountHolderType: String,
        @Field("accountNumber") accountNumber: String,
        @Field("routingNumber") routingNumber: String
    ): ResponseSignUpSignInDM


    // User Bank Info
    @GET("auth/bankAccountStatus")
    suspend fun UserBankInfo(
        @Header("token") token: String
    ): ResponseUserAccountInfoDM


    // Account Bank Verification
    @FormUrlEncoded
    @POST("auth/verifyBankAccount")
    suspend fun AccountVerification(
        @Header("Content-Type") content_type: String,
        @Header("token") token: String,
        @Field("amount1") Float: String,
        @Field("amount2") amount2: String
    ): ResponseSignUpSignInDM


    // Send Tribu Request
  //  @Headers("Content-Type: application/json")
 /*   @POST("auth/sentTribuRequest")
    suspend fun SendTribuRequest(
        @Header("token") token: String,
        @Body obj: JsonObject
    ): ResponseSendTribuDM*/


//    // Accept Tribu
//  //  @Headers("Content-Type: application/json")
//    @POST("auth/sentTribu")
//    suspend fun SendTribu(
//        @Header("token") token: String,
//        @Body obj: JsonObject
//    ): ResponseSendTribuDM



    // Accept Tribu


  //  @Headers("Content-Type: application/json")
   // @POST("auth/sendTribu")
    @POST("/dwolla/transferMoneyFromSelfToReciever")
    suspend fun AcceptTribu(
        @Header("token") token: String,
        @Body obj: JsonObject
    ): ResponseAcceptTribu


    // Reject Tribu
  //  @Headers("Content-Type: application/json")
 //   @POST("auth/rejctTribuSentRequest")
    @POST("dwolla/rejctTribuSentRequest")
    suspend fun RejectTribu(
        @Header("token") token: String,
        @Body obj: JsonObject
    ): ResponseRejectTribUDM






    // Account Balance details
    @POST("auth/balanceDetails")
    suspend fun AccountBalanceDetails(
        @Header("token") token: String

    ): ResponseAccountBalanceDetailsDM


    // Transaction List
    @GET("auth/balanceDetailsByDate")
    suspend fun UserTransactionList(
        @Header("token") token: String,
        @Query("startDate") startDate: String,
        @Query("endDate") endDate: String,
        @Query("skip") skip: Int,
        @Query("limit") limit: Int
    ): ResponseTransacrionDetailsDM


    // Tribu Sent Request Status

  //  @GET("auth/getTribuSentRequestStatus")
    @GET("dwolla/transactionStatus/{id}")
    suspend fun CheckStatus(
        @Header("token") token: String,
        @Path("id") tribuRequestId: String
    ): ResponseTrubuStatusDM


    @POST("auth/logout")
    suspend fun logout(
        @Header("token") token: String
     //   @Body obj: JsonObject
    ): ResponseLogoutDM


    @GET("dwolla/walletBalanceWithoutTransactionHistory")
    suspend fun walletBalanceWithoutTransactionHistory(
        @Header("token") token: String
    ): ResponseWalletBalWithoutTransactionHistory


    @GET("dwolla/customerTransferSource")
    suspend fun getFundingSources(
        @Header("token") token: String
    ): ResponseFundingSources


    // Transaction List
    @GET("dwolla/walletBalance")
    suspend fun transactionList(
        @Header("token") token: String,
        @Query("skip") skip: Int,
        @Query("limit") limit: Int
    ): ResponseGetWalletBalance


    @POST("dwolla/transferMoneyFromSelfToSelf")
    suspend fun addOrTransferMoney(
        @Header("token") token: String,
        @Body obj: JsonObject
    ): ResponseAddOrTransferMoney


    @POST("dwolla/sentTribuRequest")
    suspend fun sentTribuRequest(
        @Header("token") token: String,
        @Body obj: JsonObject
    ): ResponseSendTribu


//    static let getTribuSentRequestStatus = “dwolla/transactionStatus/”

    // User Bank Info
    @GET("getStateData")
    suspend fun getStates(): ResponseGetStates


    // get all image url data
    @GET("getAllData")
    suspend fun getAllData(): ResponseGetAllData

    // get user status
    @GET("auth/checkUser")
    suspend fun getUserStatus(
        @Header("token") token: String
        ): ResponseUserStatus


}