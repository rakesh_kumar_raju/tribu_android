package com.tribU.network

interface NetworkConfiguration {
    fun getBaseUrl(): String
}