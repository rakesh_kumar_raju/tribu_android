package com.tribU.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}