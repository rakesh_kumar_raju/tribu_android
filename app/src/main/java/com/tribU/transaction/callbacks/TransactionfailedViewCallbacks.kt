package com.tribU.transaction.callbacks

interface TransactionfailedViewCallbacks {
    fun crossClick()
    fun goToHomeClick()
}