package com.tribU.transaction.callbacks

interface TransactionFragViewCallbacks {
    fun sendClick()
    fun timeFilterClick()
    fun showAddClick()
}