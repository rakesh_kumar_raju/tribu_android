package com.tribU.transaction.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tribU.errorProvider.ErrorProvider
import com.tribU.home.model.ResponseAccountBalanceDetailsDM
import com.tribU.home.model.ResponseTransacrionDetailsDM
import com.tribU.network.Result
import com.tribU.network.TransactionData
import kotlinx.coroutines.launch
import javax.inject.Inject

class TransactionzFragVM @Inject constructor(private  val transactionData: TransactionData,
                                             private val errorProvider: ErrorProvider
): ViewModel() {

    private var _balanceDetailsResult = MutableLiveData<Result<ResponseAccountBalanceDetailsDM>>()

    val balanceDetailsResult: LiveData<Result<ResponseAccountBalanceDetailsDM>>
        get() = _balanceDetailsResult

    private var _transactionListResult = MutableLiveData<Result<ResponseTransacrionDetailsDM>>()

    val transactionListResult: LiveData<Result<ResponseTransacrionDetailsDM>>
        get() = _transactionListResult

    fun balanceResultApi(token:String){
        viewModelScope.launch {
            try {
                _balanceDetailsResult.postValue(Result.loading())
                val response = transactionData.AccntBalanceDetialProcess(token)
                _balanceDetailsResult.postValue(Result.success(response))
            Log.d("get_true","true")
            } catch (exception: Exception) {
                exception.message?.let { Log.d("get_error", it) }
                _balanceDetailsResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }

    fun transactionListApi(token:String,startDate:String,endDate:String,skip:Int,limit:Int){
        viewModelScope.launch {
            try {
                _transactionListResult.postValue(Result.loading())
                val response = transactionData.GetTransactionList(token,startDate,endDate,skip,limit)
                _transactionListResult.postValue(Result.success(response))
            } catch (exception: Exception) {
                exception.message?.let { Log.d("get_error", it) }
                _transactionListResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }


}


