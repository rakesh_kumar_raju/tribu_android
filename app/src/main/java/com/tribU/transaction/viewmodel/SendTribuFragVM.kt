package com.tribU.transaction.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.network.Result
import com.tribU.network.SendTribuData
import com.tribU.signup.model.ResponseSendTribuDM
import com.tribU.signup.model.ResponseTrubuStatusDM
import kotlinx.coroutines.launch
import javax.inject.Inject

class SendTribuFragVM @Inject constructor(private  val send_tribu_data: SendTribuData,
                                          private val errorProvider: ErrorProvider
):
    ViewModel() {
    private var _sendTribuRequest = MutableLiveData<Result<ResponseSendTribuDM>>()
    val sendTribuRequest: LiveData<Result<ResponseSendTribuDM>>
        get() = _sendTribuRequest

    private var _tribuStatus = MutableLiveData<Result<ResponseTrubuStatusDM>>()
    val tribuStatus: LiveData<Result<ResponseTrubuStatusDM>>
        get() = _tribuStatus



//
//    fun SendTribuApi(token: String,amount:JsonObject) {
//        viewModelScope.launch {
//            try {
//                _sendTribuResult.postValue(Result.loading())
//                val response = send_tribu_data.SendTribuProcess(token,amount)
//                _sendTribuResult.postValue(Result.success(response))
//                Log.d("check_working", "true")
//            } catch (exception: Exception) {
//                Log.d("check_working", exception.message.toString())
//                _sendTribuResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
//            }
//        }
//    }

    fun SendTribuRequestApi(token: String,amount:JsonObject) {
        viewModelScope.launch {
            try {
               /* _sendTribuRequest.postValue(Result.loading())
                val response = send_tribu_data.SendTribuRequestProcess(token,amount)
                _sendTribuRequest.postValue(Result.success(response))
                Log.d("check_working", "true")*/
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _sendTribuRequest.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }


    fun GetTribustatusApi(token: String,requestId:String) {
        viewModelScope.launch {
            try {
                _tribuStatus.postValue(Result.loading())
                val response = send_tribu_data.TribuStatusProcess(token,requestId)
                _tribuStatus.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _tribuStatus.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }



}
