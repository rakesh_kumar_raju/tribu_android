package com.tribU.transaction.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tribU.errorProvider.ErrorProvider
import com.tribU.network.BottomNavigationData
import com.tribU.network.Result
import com.tribU.network.signInSignUpData
import com.tribU.signup.model.ResponseSignUpSignInDM
import com.tribU.signup.model.ResponseTrubuStatusDM
import kotlinx.coroutines.launch
import javax.inject.Inject

class TimerActivityVM  @Inject constructor(private  val statusData: BottomNavigationData,
                                           private val errorProvider: ErrorProvider
): ViewModel() {

    private var _checkStatusResult = MutableLiveData<Result<ResponseTrubuStatusDM>>()

    val checkStatusResult: LiveData<Result<ResponseTrubuStatusDM>>
        get() = _checkStatusResult


    fun checkStatusApi(token: String, requestId: String) {
        viewModelScope.launch {
            try {
                _checkStatusResult.postValue(Result.loading())
                val response = statusData.TribuStatusProcess(token, requestId)
                _checkStatusResult.postValue(Result.success(response))
            } catch (exception: Exception) {
                _checkStatusResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }
}