package com.tribU.transaction

import android.content.Intent
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import com.tribU.R
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.TimerActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.transaction.callbacks.TimerViewCallbacks
import com.tribU.transaction.viewmodel.TimerActivityVM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import javax.inject.Inject

class TimerActivity : BaseDataBindingActivity<TimerActivityDataBinding>(R.layout.activity_timer),
    TimerViewCallbacks {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var ViewModel: TimerActivityVM
    lateinit var device_token: String
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    lateinit var auth: FirebaseAuth
    var request_id=""

    private val TAG = TimerActivity::class.java.simpleName
    var firebaseId: String? = null

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this
        request_id= intent.getStringExtra(Constants.REQUEST_ID).toString()
        getTribuStatusObserver()
        CheckTribuStatus()
        startTimer()

    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        ViewModel = ViewModelProvider(this, viewModelFactory).get(TimerActivityVM::class.java)
        //SignUpViewModel = ViewModelProvider(this, viewModelFactory).get(LanguageVM::class.java)
    }


//    override fun signupClicked() {
//        CommonUtilities.fireActivityIntent(
//            this,
//            Intent(this, SignUpActivity::class.java),
//            isFinish = false,
//            isForward = true
//        )
//    }


    fun getTribuStatusObserver(){
        ViewModel.checkStatusResult.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {

                }
                Status.ERROR -> {
                    CommonUtilities.showToastString(this!!, it.message)
                }
                Status.SUCCESS -> {

                    val result = it.data
                    if (result?.statusCode == 200) {

                    /*    if (result.data != null) {


                            if (result.data!!.status.equals("pending")) {
                                Log.d("get_trans_status","pending")
                                Handler().postDelayed({

                                    CheckTribuStatus()
                                }, 5000)


                            } else if (result.data!!.status.equals("done")) {
                                Log.d("get_trans_status","done")
                                Constants.TRANSACTION_DONE=true
                                CommonUtilities.fireActivityIntent(
                                    this@TimerActivity,
                                    Intent(this@TimerActivity, BottomNavigationActivity::class.java),
                                    isFinish = false,
                                    isForward = true
                                )



                            } else if (result.data!!.status.equals("close")) {
                                Log.d("get_trans_status","close")
                                Constants.TRANSACTION_DONE=true
                                CommonUtilities.fireActivityIntent(
                                    this@TimerActivity,
                                    Intent(this@TimerActivity, BottomNavigationActivity::class.java),
                                    isFinish = false,
                                    isForward = true
                                )
//                                val args = Bundle()
//                                args.putString(Constants.SHOW_MESSAGE, Constants.SENT_SUCCESSFUL)
//                                var fragment = SuccessfullyTribuSentFragment()
//                                fragment.setArguments(args)
//                                (this as BottomNavigationActivity).replaceFragment(fragment)
                            }
                        }
*/

                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(this)

                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                       // binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                    }
                }
            }

        })
    }


    fun CheckTribuStatus() {


        if (!request_id.isNullOrEmpty()) {


            ViewModel.checkStatusApi(
                CommonUtilities.getString(
                    this,
                    Constants.TOKEN
                )!!, request_id.toString().trim()
            )

        }


    }
    fun startTimer() {

        var i = 300

        binding.progressTime.setProgress(100)
        var mCountDownTimer = object : CountDownTimer(60000 * 5, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
                i--
                binding.progressTime.setProgress(i as Int * 100 / (60000 * 5 / 1000))


                var millis = millisUntilFinished
                var minutes = ((millis / 1000) / 60).toString()
                var seconds = ((millis / 1000) % 60).toString()

                if(seconds.toString().length==1)
                {
                    seconds="0"+seconds.toString()
                }

                if(minutes.toString().length==1)
                {
                    minutes="0"+minutes.toString()
                }


                binding.tvTime.setText(minutes.toString() + ":" + seconds)

            }

            override fun onFinish() {
                //Do what you want
                i--
                binding.progressTime.setProgress(0)


//                isTimerVisible = false
//
//                binding.pendingView.visibility = View.GONE
//                binding.mainView.visibility = View.VISIBLE



                        CommonUtilities.fireActivityIntent(
            this@TimerActivity,
            Intent(this@TimerActivity, BottomNavigationActivity::class.java),
            isFinish = false,
            isForward = true
        )
            }
        }
        mCountDownTimer.start()


    }


    override fun onBackPressed() {

    }
}
















