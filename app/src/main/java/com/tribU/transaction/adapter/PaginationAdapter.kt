package com.tribU.transaction.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.tribU.R
import com.tribU.home.model.ListItem
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class PaginationAdapter(context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    private var movies: MutableList<ListItem>?
    private val context: Context
    private var isLoadingAdded = false
    fun getMovies(): List<ListItem>? {
        return movies
    }

    fun setMovies(movies: MutableList<ListItem>?) {
        this.movies = movies
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            ITEM -> viewHolder = getViewHolder(parent, inflater)
            LOADING -> {
                val v2: View = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingVH(v2)
            }
        }
        return viewHolder!!
    }

    @NonNull
    private fun getViewHolder(
        parent: ViewGroup,
        inflater: LayoutInflater
    ): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val v1: View = inflater.inflate(R.layout.item_transaction_detail_layout, parent, false)
        viewHolder = MovieVH(v1)
        return viewHolder
    }




    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movie = movies!![position]
        when (getItemViewType(position)) {
            ITEM -> {
               var sender_id= CommonUtilities.getString(context, Constants.SENDER_ID)
             Log.d("sender_id",sender_id!!)

                val movieVH = holder as MovieVH?
if(sender_id==movie.senderId.toString()){
    movieVH!!.tv_amount.setText("- $"+movie.amount.toString())
}
                else{
    movieVH!!.tv_amount.setText("+ $"+movie.amount.toString())
                }

                movieVH!!.tb_date.setText(movie.tribuSentAt!!.toDate().formatTo("dd, MMMM "))

            }
            LOADING -> {
            }
        }
    }

    override fun getItemCount(): Int {
        return if (movies == null) 0 else movies!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == movies!!.size - 1 && isLoadingAdded) LOADING else ITEM
    }

    /*
   Helpers
   _________________________________________________________________________________________________
    */
    fun add(mc: ListItem) {
        movies!!.add(mc)
        notifyItemInserted(movies!!.size - 1)
    }

    fun addAll(mcList: List<ListItem>) {
        for (mc in mcList) {
            add(mc)
        }
    }

    fun remove(city: ListItem?) {
        val position = movies!!.indexOf(city)
        if (position > -1) {
            movies!!.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun clear() {
        isLoadingAdded = false
        while (itemCount > 0) {
            remove(getItem(0))
        }
    }

    val isEmpty: Boolean
        get() = itemCount == 0

    fun addLoadingFooter() {
        isLoadingAdded = true
        add(ListItem())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = movies!!.size - 1
        val item = getItem(position)
        if (item != null) {
            movies!!.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): ListItem {
        return movies!![position]
    }
    /*
   View Holders
   _________________________________________________________________________________________________
    */
    /**
     * Main list's content ViewHolder
     */
    protected inner class MovieVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tb_date: TextView
        val tv_amount: TextView

        init {
            tb_date = itemView.findViewById(R.id.tv_transaction_date)
            tv_amount = itemView.findViewById(R.id.tv_transaction_amount)
        }
    }

    protected inner class LoadingVH(itemView: View?) : RecyclerView.ViewHolder(itemView!!)
    companion object {
        private const val ITEM = 0
        private const val LOADING = 1
    }


    init {
        this.context = context
        movies = ArrayList()
    }





    fun String.toDate(dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timeZone: TimeZone = TimeZone.getTimeZone("UTC")): Date {
        val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
        parser.timeZone = timeZone
        return parser.parse(this)
    }

    fun Date.formatTo(dateFormat: String, timeZone: TimeZone = TimeZone.getDefault()): String {
        val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
        formatter.timeZone = timeZone
        return formatter.format(this)
    }

}
