package com.tribU.transaction.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.tribU.R
import com.tribU.databinding.ItemTransactionDetailLayoutBinding
import com.tribU.home.model.ListItem
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class TransactionDetailAdapter(context: Context, itemList: ArrayList<ListItem>) : RecyclerView.Adapter<TransactionDetailAdapter.ViewHolder>()
{
    internal var context: Context
    internal var itemList:ArrayList<ListItem>
    init{
        this.context = context
        this.itemList = itemList
    }
    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ViewHolder {
        val binding: ItemTransactionDetailLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(viewGroup.getContext()),
            R.layout.item_transaction_detail_layout, viewGroup, false
        )
        return ViewHolder(binding)
    }
    override fun getItemCount(): Int {
        return itemList.size
    }
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val input = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val output = SimpleDateFormat("dd-MM-yyyy")

        var d: Date? = null
        try {
            d = input.parse(itemList.get(position).tribuSentAt)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val formatted = output.format(d)
        Log.i("DATE", "" + formatted)





        val f = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        var convertedDate = f.parse(itemList.get(position).tribuSentAt);
        f.timeZone = TimeZone.getTimeZone("Asia/Calcutta")

viewHolder.binding.tvTransactionAmount.setText("$" + itemList.get(position).amount!!)
viewHolder.binding.tvTransactionDate.setText(formatted)

    }
    class ViewHolder(binding: ItemTransactionDetailLayoutBinding) :
        RecyclerView.ViewHolder(binding.getRoot()) {
        var binding: ItemTransactionDetailLayoutBinding
        init {
            this.binding = binding
        }
    }


}