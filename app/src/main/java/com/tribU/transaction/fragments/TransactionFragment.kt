package com.tribU.transaction.fragments

import android.app.DatePickerDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.DatePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.google.android.gms.ads.*
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.TransactionFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.model.ListItem
import com.tribU.home.model.ResponseAccountBalanceDetailsDM
import com.tribU.home.model.ResponseTransacrionDetailsDM
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.transaction.adapter.PaginationAdapter
import com.tribU.transaction.callbacks.MyTransactionFragCallbacks
import com.tribU.transaction.callbacks.TransactionFragViewCallbacks
import com.tribU.transaction.viewmodel.TransactionzFragVM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import com.tribU.util.PaginationScrollListener
import java.text.SimpleDateFormat
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class TransactionFragment: BaseDataBindingFragment<TransactionFragDataBinding>(R.layout.transaction_old_user_layout),
    TransactionFragViewCallbacks , DatePickerDialog.OnDateSetListener {

    @set:Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var view_model: TransactionzFragVM
    var mContext: Context? = null
    var transactionList_itemList = ArrayList<ListItem>()
    var transactionDetailAdapter: PaginationAdapter? = null
    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    private var TOTAL_PAGES = false
    private var skip = 0
    private var limit = 0
    var counting=0
    private var filter_date = ""
    private var first_data = false
    private var mIsRestoredFromBackstack = false
    private var datePickerDialog: DatePickerDialog? = null

    companion object {
        fun newInstance() =
            TransactionFragment()
    }
    val AD_UNIT_ID = "ca-app-pub-3940256099942544/1033173712"
    private var callBackListener: MyTransactionFragCallbacks? = null

    //private val viewModel by viewModels<MyLibraryFragVM>(factoryProducer = { viewModelFactory })


    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
        try {
            callBackListener = activity as MyTransactionFragCallbacks
        } catch (e: ClassCastException) {
            throw ClassCastException(
                activity.toString()
                        + "  must implement MyLibraryCallbacks"
            )
        }
    }


    override fun onDataBindingCreated() {

        binding.callback = this

        if(mIsRestoredFromBackstack==false) {
            view_model = ViewModelProvider(this, viewModelFactory).get(TransactionzFragVM::class.java)
            setupObserver()
            view_model.balanceResultApi(
                CommonUtilities.getString(
                    requireActivity(),
                    Constants.TOKEN
                )!!
            )
            initRecylerTransactionDetails()

        }

    }

    override fun onResume() {
        super.onResume()
        if(mIsRestoredFromBackstack==true)
            {
                isLoading = false
                isLastPage = false
                skip = 0
                first_data = false
                transactionDetailAdapter!!.clear()
                binding.rvTransactionDetails.adapter = null
                view_model =
                    ViewModelProvider(this, viewModelFactory).get(TransactionzFragVM::class.java)
                setupObserver()
                view_model.balanceResultApi(
                    CommonUtilities.getString(
                        requireActivity(),
                        Constants.TOKEN
                    )!!
                )
                initRecylerTransactionDetails()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setupObserver()
        mIsRestoredFromBackstack = false;

    }

    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }

    override fun sendClick() {
        (mContext as BottomNavigationActivity).replaceFragment(SendTribuFragment())
    }

    override fun timeFilterClick() {
       // loadInterstitialAd() ;
        datePickerDialogShow()
    }

    override fun showAddClick() {
        //transactionDetailAdapter!!.clear()
       // loadInterstitialAd()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as BottomNavigationActivity).binding!!.bottomNavigationMain.menu.getItem(1).isChecked =
            true
    }


    private fun setupObserver() {
        view_model.balanceDetailsResult.observe(viewLifecycleOwner, Observer {
            when (it?.status) {
                Status.LOADING -> {

                }
                Status.ERROR -> {

                    CommonUtilities.showToastString(mContext!!, it.message)
                }
                Status.SUCCESS -> {

                    val result = it.data
                    if (result?.statusCode == 200) {
                        Log.d("getting_value", result.data.toString())
                        saveBalancedetails(result)


                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(context as BottomNavigationActivity)

                    } else {
                        CommonUtilities.showToastString(mContext!!, it.data?.message.toString())

                    }
                }
            }

        })

        view_model.transactionListResult.observe(viewLifecycleOwner, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    //  CommonUtilities.showLoader(mContext!!)
                }
                Status.ERROR -> {
                    // CommonUtilities.hideLoader()
                    CommonUtilities.showToastString(mContext!!, it.message)
                }
                Status.SUCCESS -> {
                    // CommonUtilities.hideLoader()
                    val result = it.data
                    if (result?.statusCode == 200) {

                        saveTransactionListResult(result)


                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(context as BottomNavigationActivity)

                    } else {
                        CommonUtilities.showToastString(mContext!!, it.data?.message.toString())

                    }
                }
            }

        })
    }


    fun saveBalancedetails(result: ResponseAccountBalanceDetailsDM) {
        binding.tvSendTribu.setText("$" + result.data!!.totalTribuSent!!)
        binding.tvReceviedTribu.setText("$" + result.data!!.totalTribuRecived!!)
        binding.tvPendingTribu.setText("$" + result.data!!.totalPending!!)

    }


    fun initRecylerTransactionDetails() {
            Log.d("call_adapter", "loadFirstPage: ")
        transactionDetailAdapter = PaginationAdapter(mContext!!)

//        if(counting>0) {
//            transactionDetailAdapter!!.clear()
//
//        }
        var linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding?.rvTransactionDetails.setLayoutManager(linearLayoutManager)
        binding?.rvTransactionDetails.setAdapter(transactionDetailAdapter)



        binding?.rvTransactionDetails.addOnScrollListener(object : PaginationScrollListener(
            linearLayoutManager
        ) {
            override fun loadMoreItems() {
                isLoading = true
                // mocking network delay for API call
                Handler().postDelayed(Runnable { loadNextPage() }, 1000)
            }


            override fun isLastPage(): Boolean {
                return isLastPage
            }


            override fun isLoading(): Boolean {
                return isLoading
            }
        })


        Handler().postDelayed({ loadFirstPage() }, 1000)
    }


    private fun loadFirstPage() {
        first_data=true
        Log.d("TAG", "loadFirstPage: ")
        // val movies: List<Movie> =
        view_model.transactionListApi(
            CommonUtilities.getString(
                requireActivity(),
                Constants.TOKEN
            )!!, "", "", 0, 10
        )
    }


    private fun loadNextPage() {
       skip = skip + 10
        //transactionDetailAdapter!!.addLoadingFooter()
        view_model.transactionListApi(
            CommonUtilities.getString(
                requireActivity(),
                Constants.TOKEN
            )!!, "", "", skip, 10
        )

    }

    fun saveTransactionListResult(result: ResponseTransacrionDetailsDM) {
        Log.d("get_data", result.toString())
        if (first_data == true)
        {
            first_data = false
            if (result.data!!.list.isNullOrEmpty()) {
                Log.d("empty_list", "true ")
                isLastPage = true
                binding.tvNoData.visibility = View.VISIBLE
            }
         else {
             binding.tvNoData.visibility = View.GONE
            if (result.data.list!!.size < 10) {
                Log.d("there", "true")
                transactionDetailAdapter!!.addAll(result.data!!.list as List<ListItem>)
                counting = counting + 1
                isLastPage = true
            } else {
                Log.d("here", "true")
                transactionDetailAdapter!!.addAll(result.data!!.list as List<ListItem>)
            }
        }
    }
            else
    {
        if (result.data!!.list.isNullOrEmpty()) {
            Log.d("empty_list", "true ")
            isLastPage = true

        } else {
            if (result.data.list!!.size < 10) {
                Log.d("there1", "true")
                //transactionDetailAdapter!!.removeLoadingFooter();
                isLastPage = true;
                transactionDetailAdapter!!.addAll(result.data!!.list as List<ListItem>)
                counting = counting + 1
            } else {
                Log.d("here1", "true")
                // transactionDetailAdapter!!.removeLoadingFooter();
                isLoading = false;
                transactionDetailAdapter!!.addAll(result.data!!.list as List<ListItem>)
                counting = counting + 1
            }
        }

        }

    }


    fun datePickerDialogShow() {
        val calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        datePickerDialog =
            DatePickerDialog(requireContext(), R.style.datepicker, this, mYear, mMonth, mDay)
        datePickerDialog!!.getDatePicker().maxDate = calendar.timeInMillis
        datePickerDialog?.show()
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar2: Calendar = Calendar.getInstance()
        calendar2.set(year, month, dayOfMonth, 0, 0, 0)
        val selectedDate = calendar2.time
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        filter_date=dateFormat.format(selectedDate)
        transactionDetailAdapter!!.clear()
        get_dates(filter_date)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun get_dates(date: String){
        val zoneId = ZoneId.of("UTC")
        val localDate = LocalDate.parse(date)
        val newPattern =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(zoneId)
        val startOfDay: Instant = localDate.atStartOfDay(zoneId).toInstant()
        val endOfDay: Instant =
            LocalDateTime.of(localDate, LocalTime.MAX).atZone(zoneId).toInstant()
                .truncatedTo(ChronoUnit.MILLIS)
        first_data=true
        view_model.transactionListApi(
            CommonUtilities.getString(
                requireActivity(),
                Constants.TOKEN
            )!!, startOfDay.toString(), endOfDay.toString(), 0, 0
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.loader.startAnimation();
        binding.loader.setIsVisible(true);
        val handler = Handler()
        handler.postDelayed({
            binding.loader.stopAnimation();
            binding.loader.setIsVisible(false);
        }, 2000) //5 seconds

    }

    override fun onDestroyView() {
        super.onDestroyView()
        mIsRestoredFromBackstack = true;
        binding.rvTransactionDetails.adapter=null
        transactionDetailAdapter!!.clear()
        isLastPage=false
        isLoading=false
        skip=0

    }

}
