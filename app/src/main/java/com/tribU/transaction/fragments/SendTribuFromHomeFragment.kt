package com.tribU.transaction.fragments

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.biometric.BiometricConstants
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.google.gson.JsonObject
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.SendTribuFromHomeDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.profile.callbacks.SendTribuFragCallbacks
import com.tribU.signup.model.ResponseTrubuStatusDM
import com.tribU.transaction.TimerActivity
import com.tribU.transaction.callbacks.SendTribuFromHomeViewCallbacks
import com.tribU.transaction.viewmodel.SendTribuFragVM
import com.tribU.transaction.viewmodel.SendTribuHomeFragVM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import java.util.concurrent.Executor
import javax.inject.Inject


class SendTribuFromHomeFragment :
    BaseDataBindingFragment<SendTribuFromHomeDataBinding>(R.layout.send_tribu_from_home),
    SendTribuFromHomeViewCallbacks {

    @set:Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var view_model: SendTribuHomeFragVM
    var mContext: Context? = null
    var requestId: String? = null
    var biometricPrompt:BiometricPrompt?=null
    var promptInfo: BiometricPrompt.PromptInfo?=null
    var isTimerVisible = false
    var have_sensor=false


    companion object {
        fun newInstance() =
            SendTribuFromHomeFragment()
    }

    private var callBackListener: SendTribuFragCallbacks? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
        try {
            callBackListener = activity as SendTribuFragCallbacks
        } catch (e: ClassCastException) {
            throw ClassCastException(
                activity.toString()
                        + "  must implement MyLibraryCallbacks"
            )
        }
    }



    override fun onDataBindingCreated() {
        binding.callback = this
        view_model = ViewModelProvider(this, viewModelFactory).get(SendTribuHomeFragVM::class.java)
        // setupObserver()
        if(Constants.TRANSACTION_DONE==true){
            Constants.TRANSACTION_DONE=false
            val args = Bundle()
                                args.putString(Constants.SHOW_MESSAGE, Constants.SENT_SUCCESSFUL)
                                var fragment = SuccessfullyTribuSentFragment()
                                fragment.setArguments(args)
                                (context as BottomNavigationActivity).replaceFragment(fragment)
        }
        binding.edEnterAmount.addTextChangedListener(textWatcher)
        binding.edEnterAmount.setSelection(binding.edEnterAmount.length())
        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

             //  binding.edEnterAmount.setText(progress.toString());
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        }
        )
        binding.customSwitch.setOnCheckedChangeListener { _, isChecked ->
            val message = if (isChecked) {
                if (binding.edEnterAmount.text.length < 1) {
                    CommonUtilities.showToast(context, R.string.please_enter_amount_first)
                    binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                } else {

                    if(have_sensor==false){
                        setupObserver()
                        var obj = JsonObject()
                    obj.addProperty("amount", binding.edEnterAmount.text.toString())

                    view_model.SendTribuRequestApi(
                        CommonUtilities.getString(
                            requireActivity(),
                            Constants.TOKEN
                        )!!,
                        obj
                    )
                    }
                    else {
                        biometricPrompt!!.authenticate(
                            promptInfo!!
                        )
                    }



                }
            } else {

            }

        }

        val biometricManager = BiometricManager.from(requireContext())
        when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
                have_sensor=true
             //   CommonUtilities.showToastString(requireContext(),"can")

            }
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                have_sensor=false
              //  msgtex.setText("This device doesnot have a fingerprint sensor")
               // CommonUtilities.showToastString(requireContext(),"does not have")
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                have_sensor=false
               // msgtex.setText("The biometric sensor is currently unavailable")
               // CommonUtilities.showToastString(requireContext(),"unavailable")
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                have_sensor=false
              //  msgtex.setText("Your device doesn't have fingerprint saved,please check your security settings")
               // CommonUtilities.showToastString(requireContext(),"not saved")
            }
        }


        val executor: Executor = ContextCompat.getMainExecutor(requireContext())
         promptInfo = BiometricPrompt.PromptInfo.Builder().setTitle("TribU")
            .setDescription("Use your fingerprint to send tribu ").setNegativeButtonText("Cancel")
            .build()

        biometricPrompt =
            BiometricPrompt(
                requireActivity(),
                executor,
                object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                        if (errString != null) {
                            super.onAuthenticationError(errorCode, errString)
                        }
                        if (errorCode == BiometricConstants.ERROR_USER_CANCELED) {
                            binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                            biometricPrompt!!.authenticate(
                                promptInfo!!
                            )
                        }
                        else if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                            binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                            biometricPrompt!!.authenticate(
                                promptInfo!!
                            )
                            }
                    }

                    // THIS METHOD IS CALLED WHEN AUTHENTICATION IS SUCCESS
                    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                        super.onAuthenticationSucceeded(result)
                        setupObserver()
                        var obj = JsonObject()
                    obj.addProperty("amount", binding.edEnterAmount.text.toString())

                    view_model.SendTribuRequestApi(
                        CommonUtilities.getString(
                            requireActivity(),
                            Constants.TOKEN
                        )!!,
                        obj
                    )

                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
                    }
                })


    }





    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as BottomNavigationActivity).binding!!.bottomNavigationMain.menu.getItem(2).isChecked =
            true
    }


    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (s.toString() == "") {
                binding.seekBar.progress = 0
            } else {
                binding.seekBar.progress = Math.round(s.toString().toFloat())
            }
            binding.edEnterAmount.setSelection(binding.edEnterAmount.length())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }


    private fun setupObserver() {
        view_model.sendTribuRequest.observe(viewLifecycleOwner, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    binding.loader.startAnimation();
                    binding.loader.setIsVisible(true);
                }
                Status.ERROR -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    CommonUtilities.showToastString(mContext!!, it.message)
                }
                Status.SUCCESS -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    val result = it.data
                    if (result?.statusCode == 200) {


                        requestId = result.data!!.requestId.toString()

                        CommonUtilities.fireActivityIntent(
            requireActivity(),
            Intent(requireActivity(), TimerActivity::class.java).putExtra(Constants.REQUEST_ID,requestId),
            isFinish = false,
            isForward = true
        )
//
//                        getTribuStatusObserver()
//                        CheckTribuStatus()
//
//                        isTimerVisible = true
//
//                        binding.pendingView.visibility = View.VISIBLE
//                        binding.mainView.visibility = View.GONE
//
//                        startTimer()


                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(context as BottomNavigationActivity)

                    } else {
                        CommonUtilities.showToastString(mContext!!, it.data?.message.toString())
                        binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                    }
                }
            }

        })
    }

//    fun getTribuStatusObserver(){
//        view_model.tribuStatus.observe(viewLifecycleOwner, Observer {
//            when (it?.status) {
//                Status.LOADING -> {
//                    binding.loader.startAnimation();
//                    binding.loader.setIsVisible(true);
//                }
//                Status.ERROR -> {
//                    binding.loader.stopAnimation();
//                    binding.loader.setIsVisible(false);
//                    CommonUtilities.showToastString(mContext!!, it.message)
//                }
//                Status.SUCCESS -> {
//                    binding.loader.stopAnimation();
//                    binding.loader.setIsVisible(false);
//                    val result = it.data
//                    if (result?.statusCode == 200) {
//
//                        if (result.data != null) {
//
//
//                            if (result.data!!.status.equals("pending")) {
//
//                                Handler().postDelayed({
//
//                                    CheckTribuStatus()
//                                }, 5000)
//
//
//                            } else if (result.data!!.status.equals("done")) {
//
//
//                                savePref(result)
//                                val args = Bundle()
//                                args.putString(Constants.SHOW_MESSAGE, Constants.SENT_SUCCESSFUL)
//                                var fragment = SuccessfullyTribuSentFragment()
//                                fragment.setArguments(args)
//                                (context as BottomNavigationActivity).replaceFragment(fragment)
//
//
//                            } else if (result.data!!.status.equals("close")) {
//
//                                savePref(result)
//                                val args = Bundle()
//                                args.putString(Constants.SHOW_MESSAGE, Constants.SENT_SUCCESSFUL)
//                                var fragment = SuccessfullyTribuSentFragment()
//                                fragment.setArguments(args)
//                                (context as BottomNavigationActivity).replaceFragment(fragment)
//                            }
//                        }
//
//
//                    } else if (result?.statusCode == 401) {
//                        CommonUtilities.DialogToLogin(context as BottomNavigationActivity)
//
//                    } else {
//                        CommonUtilities.showToastString(mContext!!, it.data?.message.toString())
//                        binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
//                    }
//                }
//            }
//
//        })
//    }


    fun savePref(result: ResponseTrubuStatusDM?) {
        CommonUtilities.showToastString(requireContext(), result!!.message)
        Log.d("get_result", result.toString())
        Log.d("get_result", CommonUtilities.getString(requireActivity(), Constants.TOKEN)!!)
        //      (context as BottomNavigationActivity).replaceFragment(SuccessfullyTribuSentFragment())
    }


//    fun CheckTribuStatus() {
//
//
//        if (!requestId.isNullOrEmpty()) {
//
//
//            view_model.GetTribustatusApi(
//                CommonUtilities.getString(
//                    requireActivity(),
//                    Constants.TOKEN
//                )!!, requestId.toString().trim()
//            )
//
//        }
//
//
//    }

//    fun startTimer() {
//
//        var i = 300
//
//        binding.progressTime.setProgress(100)
//        var mCountDownTimer = object : CountDownTimer(60000 * 5, 1000) {
//            override fun onTick(millisUntilFinished: Long) {
//                Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
//                i--
//                binding.progressTime.setProgress(i as Int * 100 / (60000 * 5 / 1000))
//
//
//                var millis = millisUntilFinished
//                var minutes = ((millis / 1000) / 60).toString()
//                var seconds = ((millis / 1000) % 60).toString()
//
//                if(seconds.toString().length==1)
//                {
//                    seconds="0"+seconds.toString()
//                }
//
//                if(minutes.toString().length==1)
//                {
//                    minutes="0"+minutes.toString()
//                }
//
//
//                binding.tvTime.setText(minutes.toString() + ":" + seconds)
//
//            }
//
//            override fun onFinish() {
//                //Do what you want
//                i--
//                binding.progressTime.setProgress(0)
//
//
//                isTimerVisible = false
//
//                binding.pendingView.visibility = View.GONE
//                binding.mainView.visibility = View.VISIBLE
//
//
//            }
//        }
//        mCountDownTimer.start()
//
//
//    }

    override fun onResume() {
        super.onResume()
    }

}

