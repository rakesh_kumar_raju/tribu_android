package com.tribU.transaction.fragments

import android.content.Context
import android.os.Bundle
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.SuccessfullySentFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.transaction.callbacks.SuccessfullySentViewCallbacks
import com.tribU.transaction.viewmodel.SuccessfullySentTribuVM
import com.tribU.util.Constants
import javax.inject.Inject

class SuccessfullyTribuSentFragment  : BaseDataBindingFragment<SuccessfullySentFragDataBinding>(R.layout.successfully_tribu_sent_layout),
    SuccessfullySentViewCallbacks
{ companion object {

    fun newInstance() =
        SuccessfullyTribuSentFragment()
}

    @set:Inject
    lateinit var factory: ViewModelFactory
    public var view_model: SuccessfullySentTribuVM?=null
    override lateinit var binding: SuccessfullySentFragDataBinding
    var mContext: Context? = null
    var from_which_screen=""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onDataBindingCreated() {
        binding.callback=this

        from_which_screen = requireArguments().getString(Constants.SHOW_MESSAGE).toString()
if(from_which_screen==Constants.RECEIVED_SUCCESSFUL){
    binding.tvSentText.setText(R.string.tribu_received_succesfully)
}
        else if(from_which_screen==Constants.TIME_OUT){
        binding.tvSentText.setText(R.string.request_has_been_closed)
    }
else if(from_which_screen==Constants.ALREADY_SENT){
        binding.tvSentText.setText(R.string.request_has_been_closed)
    }
else if(from_which_screen==Constants.SENT_SUCCESSFUL){
        binding.tvSentText.setText(R.string.tribu_sent_succesfully)
    }

    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }

    override fun crossClick() {
        (context as BottomNavigationActivity).replaceFragment(TransactionFragment())
    }


//        override fun onOptionsItemSelected(@NonNull item: MenuItem): Boolean {
//            when (item.itemId) {
//                android.R.id.home -> {
//                    (mContext as HomeBaseActivity).onBackPressed()
//                    return true
//                }
//            }
//            return super.onOptionsItemSelected(item)
//        }
//


}



