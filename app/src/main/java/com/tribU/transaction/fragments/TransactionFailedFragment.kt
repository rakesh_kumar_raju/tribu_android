package com.tribU.transaction.fragments

import android.content.Context
import android.os.Bundle
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.SuccessfullySentFragDataBinding
import com.tribU.databinding.TransactionFailedFragDataBinding
import com.tribU.databinding.TransactionFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.transaction.callbacks.SuccessfullySentViewCallbacks
import com.tribU.transaction.callbacks.TransactionfailedViewCallbacks
import com.tribU.transaction.viewmodel.SuccessfullySentTribuVM
import com.tribU.util.Constants
import javax.inject.Inject

class TransactionFailedFragment  : BaseDataBindingFragment<TransactionFailedFragDataBinding>(R.layout.layout_transaction_failed),
    TransactionfailedViewCallbacks
{ companion object {

    fun newInstance() =
        TransactionFailedFragment()
}

    @set:Inject
    lateinit var factory: ViewModelFactory
    public var view_model: SuccessfullySentTribuVM?=null
    override lateinit var binding: TransactionFailedFragDataBinding
    var mContext: Context? = null
    var from_which_screen=""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onDataBindingCreated() {
        binding.callback=this

    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }

    override fun crossClick() {
        (context as BottomNavigationActivity).replaceFragment(TransactionFragment())
    }

    override fun goToHomeClick() {

    }


//        override fun onOptionsItemSelected(@NonNull item: MenuItem): Boolean {
//            when (item.itemId) {
//                android.R.id.home -> {
//                    (mContext as HomeBaseActivity).onBackPressed()
//                    return true
//                }
//            }
//            return super.onOptionsItemSelected(item)
//        }
//


}



