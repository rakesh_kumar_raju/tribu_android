package com.tribU.transaction.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.SeekBar
import android.widget.Toast
import androidx.biometric.BiometricConstants
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.google.gson.JsonObject
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.SendTribuFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.signup.model.ResponseSendTribuDM
import com.tribU.transaction.TimerActivity
import com.tribU.transaction.callbacks.SendTribuFragViewCallbacks
import com.tribU.transaction.viewmodel.SendTribuFragVM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import java.util.concurrent.Executor
import javax.inject.Inject

class SendTribuFragment : BaseDataBindingFragment<SendTribuFragDataBinding>(R.layout.tribu_sent_layout),
    SendTribuFragViewCallbacks
{ companion object {

    fun newInstance() =
        SendTribuFragment()
}


    @set:Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var view_model: SendTribuFragVM
    var have_sensor=false
    var requestId: String? = null
    var biometricPrompt:BiometricPrompt?=null
    var promptInfo: BiometricPrompt.PromptInfo?=null
    override lateinit var binding: SendTribuFragDataBinding
    var mContext: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onDataBindingCreated() {
        binding.callback=this
        setupObserver()
        binding.edEnterAmount.addTextChangedListener(textWatcher)
        binding.edEnterAmount.setSelection(binding.edEnterAmount.length())
        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

               // binding.edEnterAmount.setText(progress.toString());
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        }
        )
        binding.customSwitch.setOnCheckedChangeListener { _, isChecked ->
            val message = if (isChecked) {
                if (binding.edEnterAmount.text.length < 1) {
                    CommonUtilities.showToast(context, R.string.please_enter_amount_first)
                    binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                } else {

                    if(have_sensor==false){
                        Log.d("no_sensor","true")

                        var obj = JsonObject()
                        obj.addProperty("amount", binding.edEnterAmount.text.toString())

                        view_model.SendTribuRequestApi(
                            CommonUtilities.getString(
                                requireActivity(),
                                Constants.TOKEN
                            )!!,
                            obj
                        )
                    }
                    else {
                        biometricPrompt!!.authenticate(
                            promptInfo!!
                        )
                    }



                }
            } else {

            }

        }
        val biometricManager = BiometricManager.from(requireContext())
        when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
                have_sensor=true
                //   CommonUtilities.showToastString(requireContext(),"can")

            }
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                have_sensor=false
                //  msgtex.setText("This device doesnot have a fingerprint sensor")
                // CommonUtilities.showToastString(requireContext(),"does not have")
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                have_sensor=false
                // msgtex.setText("The biometric sensor is currently unavailable")
                // CommonUtilities.showToastString(requireContext(),"unavailable")
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                have_sensor=false
                //  msgtex.setText("Your device doesn't have fingerprint saved,please check your security settings")
                // CommonUtilities.showToastString(requireContext(),"not saved")
            }
        }


        val executor: Executor = ContextCompat.getMainExecutor(requireContext())
        promptInfo = BiometricPrompt.PromptInfo.Builder().setTitle("TribU")
            .setDescription("Use your fingerprint to send tribu ").setNegativeButtonText("Cancel")
            .build()

        biometricPrompt =
            BiometricPrompt(
                requireActivity(),
                executor,
                object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                        if (errString != null) {
                            super.onAuthenticationError(errorCode, errString)
                        }
                        if (errorCode == BiometricConstants.ERROR_USER_CANCELED) {
                            binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                            biometricPrompt!!.authenticate(
                                promptInfo!!
                            )
                        }
                        else if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                            binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                            biometricPrompt!!.authenticate(
                                promptInfo!!
                            )
                        }
                    }

                    // THIS METHOD IS CALLED WHEN AUTHENTICATION IS SUCCESS
                    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                        super.onAuthenticationSucceeded(result)


                        var obj = JsonObject()
                        obj.addProperty("amount", binding.edEnterAmount.text.toString())

                        view_model.SendTribuRequestApi(
                            CommonUtilities.getString(
                                requireActivity(),
                                Constants.TOKEN
                            )!!,
                            obj
                        )

                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
                    }
                })



    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        view_model = ViewModelProvider(this, viewModelFactory).get(SendTribuFragVM::class.java)
    }




//        override fun onOptionsItemSelected(@NonNull item: MenuItem): Boolean {
//            when (item.itemId) {
//                android.R.id.home -> {
//                    (mContext as HomeBaseActivity).onBackPressed()
//                    return true
//                }
//            }
//            return super.onOptionsItemSelected(item)
//        }
//


    override fun backClick() {
        (mContext as BottomNavigationActivity).replaceFragment(TransactionFragment())
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (s.toString() == "") {
            binding.seekBar.progress = 0
        } else {
            binding.seekBar.progress = Math.round(s.toString().toFloat())
        }
        binding.edEnterAmount.setSelection(binding.edEnterAmount.length())
    }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }



    private fun setupObserver() {
        Log.d("no_sensor_testing","true")
        view_model.sendTribuRequest.observe(viewLifecycleOwner, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    Log.d("get_result","working")
                    binding.loader.startAnimation();
                    binding.loader.setIsVisible(true);
                }
                Status.ERROR -> {
                    Log.d("get_result","working")
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    CommonUtilities.showToastString(mContext!!, it.message)
                }
                Status.SUCCESS -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    Log.d("get_result","working")
                    val result = it.data
                    if (result?.statusCode == 200) {
                        requestId = result.data!!.requestId.toString()

                        CommonUtilities.fireActivityIntent(
                            requireActivity(),
                            Intent(requireActivity(), TimerActivity::class.java).putExtra(Constants.REQUEST_ID,requestId),
                            isFinish = false,
                            isForward = true
                        )

                    }
                    else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(context as BottomNavigationActivity)

                    }else {
                        CommonUtilities.showToastString(mContext!!, it.data?.message.toString())
                        binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                    }
                }
            }

        })
    }


    fun savePref(result: ResponseSendTribuDM){
        CommonUtilities.showToastString(requireContext(),result.message)
        Log.d("get_result",result.toString())
        Log.d("get_result", CommonUtilities.getString(requireActivity(), Constants.TOKEN)!!)
        (context as BottomNavigationActivity).replaceFragment(SuccessfullyTribuSentFragment())
    }
}



