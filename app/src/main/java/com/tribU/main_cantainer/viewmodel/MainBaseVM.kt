package com.tribU.main_cantainer.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.network.BottomNavigationData
import com.tribU.network.Result
import com.tribU.network.SendTribuData
import com.tribU.signup.model.ResponseSendTribuDM
import com.tribU.signup.model.ResponseTrubuStatusDM
import com.tribU.transaction.model.ResponseRejectTribUDM
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainBaseVM @Inject constructor(private  val bottomNavigationData: BottomNavigationData,
                                          private val errorProvider: ErrorProvider
):
    ViewModel() {

    private var _acceptTribuResult = MutableLiveData<Result<ResponseSendTribuDM>>()
    val acceptTribuResult: LiveData<Result<ResponseSendTribuDM>>
        get() = _acceptTribuResult


    private var _rejectTribuResult = MutableLiveData<Result<ResponseRejectTribUDM>>()
    val rejectTribuResult: LiveData<Result<ResponseRejectTribUDM>>
        get() = _rejectTribuResult



    private var _tribuStatus = MutableLiveData<Result<ResponseTrubuStatusDM>>()
    val tribuStatus: LiveData<Result<ResponseTrubuStatusDM>>
        get() = _tribuStatus




    fun AcceptTribuApi(token: String,amount:JsonObject) {
        viewModelScope.launch {
            try {
              /*  _acceptTribuResult.postValue(Result.loading())
                val response = bottomNavigationData.AcceptTribuProcess(token,amount)
                _acceptTribuResult.postValue(Result.success(response))*/
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _acceptTribuResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }

    fun RejectTribuApi(token: String,tribuRequestId:JsonObject) {
        viewModelScope.launch {
            try {
                _rejectTribuResult.postValue(Result.loading())
                val response = bottomNavigationData.RejectTribuProcess(token,tribuRequestId)
                _rejectTribuResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _rejectTribuResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }




    fun GetTribuStatusApi(token: String,requestId:String) {
        viewModelScope.launch {
            try {
                _tribuStatus.postValue(Result.loading())
                val response = bottomNavigationData.TribuStatusProcess(token,requestId)
                _tribuStatus.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _tribuStatus.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }



}
