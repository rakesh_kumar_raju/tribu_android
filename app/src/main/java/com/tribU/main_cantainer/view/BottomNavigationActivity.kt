package com.tribU.main_cantainer.view


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.ads.*
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener
import com.google.gson.JsonObject
import com.tribU.R
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.BottomNavigationActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.callbacks.AddAccountFragCallbacks
import com.tribU.home.fragments.AddNewAccountFragment
import com.tribU.main_cantainer.callbacks.BottomNavigationCallbacks
import com.tribU.main_cantainer.viewmodel.MainBaseVM
import com.tribU.network.Status
import com.tribU.profile.callbacks.ProfileActivityCallbacks
import com.tribU.profile.callbacks.SendTribuFragCallbacks
import com.tribU.share.callbacks.MyShareFragCallbacks
import com.tribU.share.fragment.ShareFragment
import com.tribU.transaction.callbacks.MyTransactionFragCallbacks
import com.tribU.transaction.fragments.SendTribuFromHomeFragment
import com.tribU.transaction.fragments.SuccessfullyTribuSentFragment
import com.tribU.transaction.fragments.TransactionFragment
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import id.zelory.compressor.Compressor
import javax.inject.Inject


class BottomNavigationActivity :
    BaseDataBindingActivity<BottomNavigationActivityDataBinding>(R.layout.activity_bottom_navigation),
    BottomNavigationCallbacks,
    AddAccountFragCallbacks,
    MyTransactionFragCallbacks,
    SendTribuFragCallbacks,
    MyShareFragCallbacks {
    // var binding: ActivityBottomNavigationBinding? = null

    private var mSelectedItem = 0
    private val SELECTED_ITEM = "arg_selected_item"
    val newBackStackLength = supportFragmentManager.backStackEntryCount + 1
    private var interstitialAd: InterstitialAd? = null

    @set:Inject
    lateinit var viewModelFactory: ViewModelFactory
    var vm: MainBaseVM? = null

    var requestId = ""


    override fun onDataBindingCreated() {
        binding.callback = this

        if (Constants.TRANSACTION_DONE == true) {
            replaceFragment(SendTribuFromHomeFragment())
        }
        setupObserver()


        if (intent.extras != null) {
            requestId = intent.getStringExtra("requestId").toString()

            if (requestId.isNotEmpty()) {

                vm!!.GetTribuStatusApi(
                    CommonUtilities.getString(
                        this,
                        Constants.TOKEN
                    )!!, requestId.toString().trim()
                )
            }
        }





        if (CommonUtilities.getBoolean(this, Constants.FROM_SIGNUP)) {

            CommonUtilities.putBoolean(this, Constants.FROM_SIGNUP, false)

            CommonUtilities.DialogThanksForSignup(this)
        }


        binding!!.bottomNavigationMain.setOnNavigationItemSelectedListener({ item ->
            when (item.getItemId()) {
                R.id.nav_home -> {
                    val args = Bundle()
                    args.putString(Constants.FROM_WHICH_SCREEN, Constants.FROM_HOME)
                    var fragment = AddNewAccountFragment()
                    fragment.setArguments(args)
                    replaceFragment(fragment)
                }
                R.id.nav_transaction -> replaceFragment(TransactionFragment())
                R.id.nav_hidden_option -> replaceFragment(SendTribuFromHomeFragment())
                R.id.nav_share -> replaceFragment(ShareFragment())
                // R.id.nav_profile -> replaceFragment(ProfileFragment())
                else -> {
                    val startMain = Intent(Intent.ACTION_MAIN)
                    startMain.addCategory(Intent.CATEGORY_HOME)
                    startMain.flags =
                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(startMain)
                }
            }
            true
        })
        binding!!.bottomNavigationMain.setSelectedItemId(R.id.nav_home);
        if (binding!!.bottomNavigationMain.selectedItemId == null) {
            val startMain = Intent(Intent.ACTION_MAIN)
            startMain.addCategory(Intent.CATEGORY_HOME)
            startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
        }

        MobileAds.initialize(this,
            OnInitializationCompleteListener { initializationStatus -> //Showing a simple Toast Message to the user when The Google AdMob Sdk Initialization is Completed
            })
        interstitialAd = InterstitialAd(this)
        interstitialAd!!.setAdUnitId("ca-app-pub-3940256099942544/1033173712")
        MobileAds.setRequestConfiguration(
            RequestConfiguration.Builder()
                .setTestDeviceIds(listOf("866410036089864"))
                .build()
        )
        interstitialAd!!.setAdListener(object : AdListener() {
            override fun onAdLoaded() {

                showInterstitialAd()
            }

            override fun onAdClosed() {
                val args = Bundle()
                args.putString(Constants.SHOW_MESSAGE, Constants.RECEIVED_SUCCESSFUL)
                var fragment = SuccessfullyTribuSentFragment()
                fragment.setArguments(args)
                replaceFragment(fragment)
            }

        })

    }

    private fun loadInterstitialAd() {
        val adRequest: AdRequest = AdRequest.Builder().build()
        interstitialAd!!.loadAd(adRequest)
        Toast.makeText(this, "Ad is loading ", Toast.LENGTH_LONG).show()
    }


    private fun showInterstitialAd() {
        if (interstitialAd!!.isLoaded()) {
            //showing the ad Interstitial Ad if it is loaded
            interstitialAd!!.show()

        } else {
            loadInterstitialAd()
            Toast.makeText(this, "Ad is not Loaded ", Toast.LENGTH_LONG)
                .show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //   getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //  binding = DataBindingUtil.setContentView(this, R.layout.activity_bottom_navigation)
        //   vm = ViewModelProviders.of(this)[MainBaseVM::class.java]
        //  binding?.mview = vm


    }

    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        vm = ViewModelProvider(this, viewModelFactory).get(MainBaseVM::class.java)


    }


    private fun setupObserver() {


        vm?.tribuStatus!!.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    binding.loader.startAnimation();
                    binding.loader.setIsVisible(true);
                }
                Status.ERROR -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    CommonUtilities.showToastString(this!!, it.message)
                }
                Status.SUCCESS -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    val result = it.data
                    if (result?.statusCode == 200) {

                 /*       if (result.data != null) {

                            Log.d("status", result.data!!.status.toString())
                            if (result.data!!.status.equals("pending")) {


                                DialogTribuRecieved(this, requestId)


                            } else if (result.data!!.status.equals("done")) {


                                val args = Bundle()
                                args.putString(Constants.SHOW_MESSAGE, Constants.ALREADY_SENT)
                                var fragment = SuccessfullyTribuSentFragment()
                                fragment.setArguments(args)
                                replaceFragment(fragment)

                            } else if (result.data!!.status.equals("close")) {

                                val args = Bundle()
                                args.putString(Constants.SHOW_MESSAGE, Constants.TIME_OUT)
                                var fragment = SuccessfullyTribuSentFragment()
                                fragment.setArguments(args)
                                replaceFragment(fragment)
                            }
                        }
*/

                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(this)

                    } else {
                        CommonUtilities.showToastString(this!!, it.data?.message.toString())
                        //  binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                    }
                }
            }

        })

    }

    fun getTribuAcceptObserver() {

        vm?.acceptTribuResult!!.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    binding.loader.startAnimation();
                    binding.loader.setIsVisible(true);
                }
                Status.ERROR -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    CommonUtilities.showToastString(this!!, it.message)
                }
                Status.SUCCESS -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    val result = it.data
                    if (result?.statusCode == 200) {

                        if (result.data != null) {
                            loadInterstitialAd()

                        }


                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(this)

                    } else {
                        CommonUtilities.showToastString(this!!, it.data?.message.toString())
                        //  binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                    }
                }
            }

        })


    }

    fun getTribuRejectObserver() {

        vm?.rejectTribuResult!!.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    binding.loader.startAnimation();
                    binding.loader.setIsVisible(true);
                }
                Status.ERROR -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    CommonUtilities.showToastString(this!!, it.message)
                }
                Status.SUCCESS -> {
                    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);
                    val result = it.data
                    if (result?.statusCode == 200) {

                        if (result.data != null) {


                        }


                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(this)

                    } else {
                        CommonUtilities.showToastString(this!!, it.data?.message.toString())
                        //  binding.customSwitch.setChecked(!binding.customSwitch.isChecked());
                    }
                }
            }

        })


    }


    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.mFrame, fragment)
            .addToBackStack(null).commit()

    }

    fun changeFragment(fragmentToChange: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.mFrame, fragmentToChange)
            addToBackStack(null)
            commit()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val homeIns: Fragment? =
            supportFragmentManager.findFragmentById(R.id.mFrame) as Fragment?
/*        if (homeIns is ProfileFragment) {
            if (resultCode == Activity.RESULT_OK) {
                val uri: Uri = data?.data!!
                var file = ImagePicker.getFile(data)


                val compressedImageFile = Compressor.getDefault(this).compressToFile(file);
                homeIns.setImgae(uri, file!!)
            }
        }*/

    }


    fun DialogTribuRecieved(activity: Activity, requestId: String) {


        var dialogGet = CommonUtilities.customDialog(
            context = activity!!,
            dialogView = R.layout.tribu_received_layout
        )

        val tv_accept = dialogGet.findViewById<View>(R.id.tv_accept) as Button
        val tv_reject = dialogGet.findViewById<View>(R.id.tv_reject) as Button

        tv_accept.setOnClickListener {
            getTribuAcceptObserver()
            dialogGet.cancel()
            dialogGet.dismiss()


            if (!requestId.isNullOrEmpty()) {


                var obj = JsonObject()
                obj.addProperty("requestId", requestId)

                vm!!.AcceptTribuApi(
                    CommonUtilities.getString(
                        this,
                        Constants.TOKEN
                    )!!,
                    obj
                )


            }


        }

        tv_reject.setOnClickListener {
            getTribuRejectObserver()
            dialogGet.cancel()
            dialogGet.dismiss()

            if (!requestId.isNullOrEmpty()) {


                var obj = JsonObject()
                obj.addProperty("tribuRequestId", requestId)

                vm!!.RejectTribuApi(
                    CommonUtilities.getString(
                        this,
                        Constants.TOKEN
                    )!!,
                    obj
                )


            }

        }


    }

}








