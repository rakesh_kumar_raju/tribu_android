package com.tribU.otp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.network.signInSignUpData
import com.tribU.network.Result
import com.tribU.signup.model.ResponseSignUpSignInDM
import com.tribU.signup.model.ResponseSignUpSignInDM2
import kotlinx.coroutines.launch
import javax.inject.Inject

class OtpActivityVM  @Inject constructor(private  val signInSignUpData: signInSignUpData,
                                         private val errorProvider: ErrorProvider
): ViewModel() {

    private var _signUpResult = MutableLiveData<Result<ResponseSignUpSignInDM2>>()

    val signUpResult: LiveData<Result<ResponseSignUpSignInDM2>>
        get() = _signUpResult

    private var _signInResult = MutableLiveData<Result<ResponseSignUpSignInDM2>>()

    val signInResult: LiveData<Result<ResponseSignUpSignInDM2>>
        get() = _signInResult

  /*  fun signUpUser(name:String,email:String,country_code:String,phone:String,firebaseId:String,device_token:String){
        viewModelScope.launch {
            try {
                _signUpResult.postValue(Result.loading())
                val response = signInSignUpData.SignUpProcess("application/x-www-form-urlencoded",name,email,country_code,phone,firebaseId,
                    "2",device_token)
                _signUpResult.postValue(Result.success(response))
            } catch (exception: Exception) {
                _signUpResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }*/

    fun signUpUser(obj: JsonObject) {
        viewModelScope.launch {
            try {
                _signUpResult.postValue(Result.loading())
                val response = signInSignUpData.SignUpProcess(obj)
                _signUpResult.postValue(Result.success(response))
            } catch (exception: Exception) {
                exception.message?.let { Log.d("get_error", it) }
                _signUpResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }

    fun signInUser(obj: JsonObject) {
        viewModelScope.launch {
            try {
                _signInResult.postValue(Result.loading())
                val response = signInSignUpData.SignInProcess(obj)
                _signInResult.postValue(Result.success(response))
            } catch (exception: Exception) {
                exception.message?.let { Log.d("get_error", it) }
                _signInResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }


}


