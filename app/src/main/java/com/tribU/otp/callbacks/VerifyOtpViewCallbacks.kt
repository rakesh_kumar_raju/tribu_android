package com.tribU.otp.callbacks

interface VerifyOtpViewCallbacks {
    fun verifyProceedClicked()
    fun resendOtpClicked()
    fun backClicked()
    fun nextClick()

}