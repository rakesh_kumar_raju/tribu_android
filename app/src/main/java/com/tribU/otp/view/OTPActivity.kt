package com.tribU.otp.view

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.text.TextPaint
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.JsonObject
import com.tribU.R
import com.tribU.SplashNewActivity
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.VerifyOtpActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.view.HomeActivity
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.otp.callbacks.VerifyOtpViewCallbacks
import com.tribU.otp.viewmodel.OtpActivityVM
import com.tribU.signup.model.ResponseSignUpSignInDM
import com.tribU.signup.model.ResponseSignUpSignInDM2
import com.tribU.signup.view.GettingStarted1Activity
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import android.os.CountDownTimer





class OTPActivity :
    BaseDataBindingActivity<VerifyOtpActivityDataBinding>(R.layout.activity_otpactivity),
    VerifyOtpViewCallbacks {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var SignUpViewModel: OtpActivityVM
    lateinit var device_token: String
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    lateinit var auth: FirebaseAuth
    private var storedVerificationId: String? = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private var mVerificationId: String? = ""
    private val TAG = OTPActivity::class.java.simpleName
    lateinit var mobile_with_code: String
    lateinit var mobile_no: String
    lateinit var mobileCode: String
    lateinit var full_name: String
    lateinit var email: String
    var firebaseId: String? = null

    private var db: FirebaseFirestore? = null

    override fun onDataBindingCreated() {
        auth = FirebaseAuth.getInstance()
        binding.callback = this
        binding.lifecycleOwner = this
        db = FirebaseFirestore.getInstance();
        //  binding.loader.startAnimation();
        //  binding.loader.setIsVisible(true);
        //  binding.btsend.isEnabled=false
        device_token = CommonUtilities.getString(this, Constants.DEVICE_TOKEN).toString()
        val intent = intent
        mobile_no = intent.getStringExtra(Constants.PHONE_NO).toString()
        mobileCode = intent.getStringExtra(Constants.PHONE_CODE).toString()
        mobile_with_code =
            intent.getStringExtra(Constants.PHONE_CODE).toString() + intent.getStringExtra(
                Constants.PHONE_NO
            ).toString()


        binding.tvSentTo.text = mobile_with_code


        if (intent.getStringExtra(Constants.FROM_WHICH_SCREEN).toString() == Constants.SIGNUP) {
            //   full_name = intent.getStringExtra(Constants.FULL_NAME).toString()
            //  email = intent.getStringExtra(Constants.EMAIL).toString()

            binding.btsend.text = "Sign up"

        } else {
            binding.btsend.text = "Sign in"
        }


        val paint: TextPaint = binding.tvHeading.getPaint()
        val width = paint.measureText("You're all set")


        val textShader: Shader = LinearGradient(
            0f, 0f, width, binding.tvHeading.textSize, intArrayOf(
                Color.parseColor("#FFFFFFFF"),
                Color.parseColor("#FECC2F")
            ), null, Shader.TileMode.REPEAT
        )

        binding.tvHeading.paint.setShader(textShader)
        binding.tvHeading.text = "You're all set"




        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {


            override fun onVerificationCompleted(credential: PhoneAuthCredential) {

            }


            override fun onVerificationFailed(e: FirebaseException) {
                Log.d("GFG", "onVerificationFailed  $e")
                /*    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);*/
                CommonUtilities.hideLoader()
                CommonUtilities.showToastString(this@OTPActivity, e.message)
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                Log.d("GFG", "onCodeSent: $verificationId")
                mVerificationId = verificationId
                resendToken = token
                /*  binding.loader.stopAnimation();
                  binding.loader.setIsVisible(false);*/
                binding.btsend.isEnabled = true

                CommonUtilities.hideLoader()

                startTimer()


            }
        }
        setupObserver()


        /**
         *  if coming from splash ( if user is created but KYC not done )
         * */
        var from_which_screen = intent.getStringExtra(Constants.FROM_WHICH_SCREEN).toString()
        if (from_which_screen == Constants.SPLASH) {


            binding.clYouAreSet.visibility = View.VISIBLE
            binding.btsend.isEnabled = true
            binding.clOne.visibility = View.GONE
            binding.clTwo.visibility = View.GONE

        } else {
            sendVerificationCode(mobile_with_code)

        }
        /**
         * */


        if (SplashNewActivity.appData != null) {
            binding.tvHeading.text =
                SplashNewActivity.appData.createAccountScreen!!.heading
            binding.tvTitle.text =
                SplashNewActivity.appData.createAccountScreen!!.paragraph1

            Glide.with(this)
                .load(SplashNewActivity.appData.createAccountScreen!!.imgUrl)
                .into(binding.ivBg)


        }


    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        SignUpViewModel = ViewModelProvider(this, viewModelFactory).get(OtpActivityVM::class.java)
    }

    override fun verifyProceedClicked() {

        if (binding.pvPin.text!!.length < 1) {
            CommonUtilities.showToast(this, R.string.please_enter_otp)
        } else if (binding.pvPin.text!!.length < 6) {
            CommonUtilities.showToast(this, R.string.please_enter_6digit_otp)
        } else {
            binding.pvPin.hideKeyboard()
            // testing()
            binding.loader.startAnimation();
            binding.loader.setIsVisible(true);
            binding.btsend.isEnabled = false
            verifyVerificationCode(binding.pvPin.text!!.toString())

            /*  if (intent.getStringExtra(Constants.FROM_WHICH_SCREEN).toString() == Constants.SIGNUP) {


                  binding.clYouAreSet.visibility = View.VISIBLE
                  binding.btsend.isEnabled = true
                  binding.clOne.visibility = View.GONE
                  binding.clTwo.visibility = View.GONE


              } else {
                  CommonUtilities.fireActivityIntent(
                      this,
                      Intent(this, HomeActivity::class.java),
                      isFinish = true,
                      isForward = true
                  )
              }*/


        }

    }

    override fun resendOtpClicked() {
        binding.tvResendOtp.visibility = View.INVISIBLE
        binding.tvTimer.visibility = View.VISIBLE

        resendVerificationCode(mobile_no, resendToken)
        /* binding.loader.startAnimation();
         binding.loader.setIsVisible(true);*/
    }

    override fun backClicked() {
        onBackPressed()
    }

    override fun nextClick() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, GettingStarted1Activity::class.java),
            isFinish = true,
            isForward = true
        )
    }

    private fun verifyVerificationCode(code: String) {
        //creating the credential
        val credential = PhoneAuthProvider.getCredential(mVerificationId!!, code)


        //signing the user

        CommonUtilities.showLoader(this)

        signInWithPhoneAuthCredential(credential)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this,
                OnCompleteListener<AuthResult?> { task ->
                    if (task.isSuccessful) {
                        if (intent.getStringExtra(Constants.FROM_WHICH_SCREEN)
                                .toString() == Constants.LOGIN
                        ) {


                            var obj = JsonObject()
                            obj.addProperty("countryCode", mobileCode)
                            obj.addProperty("phoneNo", mobile_no)
                            obj.addProperty("firebaseId", FirebaseAuth.getInstance().getCurrentUser()!!.uid.toString())
                            obj.addProperty("deviceType", "2")
                            obj.addProperty("deviceToken", device_token)

                            SignUpViewModel.signInUser(obj)


                        } else {

                            addUserToFireStore(
                                mobile_with_code,
                                FirebaseAuth.getInstance().currentUser!!.uid.toString()
                            )


                        }
                    } else {

                        binding.btsend.isEnabled = true

                        if (task.exception is FirebaseAuthInvalidCredentialsException) {
                            //    CommonUtilities.showToastString(this, task.exception!!.message)
                            CommonUtilities.showToastString(this, "Invalid OTP!")
                        }

                    }
                })
    }

    private fun sendVerificationCode(number: String) {
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(number) // Phone number to verify
            .setTimeout(30L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this) // Activity (for callback binding)
            .setCallbacks(callbacks) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
        Log.d("GFG", "Auth started")

        CommonUtilities.showLoader(this)
    }






    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    fun addUserToFireStore(phone_no: String, firebaseId: String) {
        val user_data = hashMapOf(
            "phone" to phone_no,
            "firebaseId" to firebaseId
        )
        db!!.collection("users")
            .add(user_data)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot written with ID: ${documentReference.id}")
                /*  binding.loader.stopAnimation();
                  binding.loader.setIsVisible(false);*/

                var obj = JsonObject()
                obj.addProperty("countryCode", mobileCode)
                obj.addProperty("phoneNo", mobile_no)
                obj.addProperty(
                    "firebaseId",
                    FirebaseAuth.getInstance().getCurrentUser()!!.uid.toString()
                )
                obj.addProperty("deviceType", "2")
                obj.addProperty("deviceToken", device_token)

                SignUpViewModel.signUpUser(obj)

            }
            .addOnFailureListener { e ->
                /*binding.loader.stopAnimation();
                binding.loader.setIsVisible(false);*/
                CommonUtilities.hideLoader()
                Log.w(TAG, "Error adding document", e)
            }

    }


    private fun resendVerificationCode(
        phoneNumber: String,
        token: PhoneAuthProvider.ForceResendingToken?
    ) {
        val optionsBuilder = PhoneAuthOptions.newBuilder(auth)
            //  .setPhoneNumber("+91" + phoneNumber)       // Phone number to verify
            .setPhoneNumber(mobile_with_code)       // Phone number to verify
            .setTimeout(30L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
        if (token != null) {
            optionsBuilder.setForceResendingToken(token) // callback's ForceResendingToken
        }
        PhoneAuthProvider.verifyPhoneNumber(optionsBuilder.build())
    }

    private fun startTimer() {







        lifecycleScope.launch(Dispatchers.Main) {


      /*      object : CountDownTimer(30000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    val time = String.format(
                        "%02d:%02d",
                        TimeUnit.SECONDS.toMinutes(millisUntilFinished),
                        millisUntilFinished - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(millisUntilFinished))
                    )
                    binding.tvTimer?.text = time
                }

                override fun onFinish() {
                    binding.tvResendOtp.visibility = View.VISIBLE
                    binding.tvTimer.visibility = View.GONE
                }
            }.start()
            */





            val totalSeconds = TimeUnit.MILLISECONDS.toSeconds(30000)
            val tickSeconds = 1
            for (second in totalSeconds downTo tickSeconds) {
                val time = String.format(
                    "%02d:%02d",
                    TimeUnit.SECONDS.toMinutes(second),
                    second - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(second))
                )
                binding.tvTimer?.text = time




                delay(1000)
            }
            binding.tvResendOtp.visibility = View.VISIBLE
            binding.tvTimer.visibility = View.INVISIBLE


        }
    }

    private fun setupObserver() {
        SignUpViewModel.signUpResult.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    //  CommonUtilities.showLoader(this)

                }
                Status.ERROR -> {

                    CommonUtilities.hideLoader()


                    CommonUtilities.showToastString(this, it.message.toString())
                    binding.btsend.isEnabled = true
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {
//                        if (result?.message == "Success") {
                        Log.d(TAG, it.data!!.data.toString())
                        SavePrefResult(result, "signup")







                        binding.clYouAreSet.visibility = View.VISIBLE
                        binding.btsend.isEnabled = true
                        binding.clOne.visibility = View.GONE
                        binding.clTwo.visibility = View.GONE


                        /*     CommonUtilities.fireActivityIntent(
                                 this,
                                 Intent(this, GettingStarted1Activity::class.java),
                                 isFinish = false,
                                 isForward = true
                             )*/


//                            CommonUtilities.fireActivityIntent(
//                                this,
//                                Intent(this, BottomNavigationActivity::class.java),
//                                isFinish = true,
//                                isForward = true
//                            )
                        //    }

                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())
                        binding.btsend.isEnabled = true
                    }
                }
            }
        })

        SignUpViewModel.signInResult.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    /* binding.loader.startAnimation();
                     binding.loader.setIsVisible(true);*/
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()

                    /*binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);*/
                    CommonUtilities.showToastString(this, it.message.toString())
                    binding.btsend.isEnabled = true
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()


                    /* binding.loader.stopAnimation();
                     binding.loader.setIsVisible(false);*/
                    val result = it.data
                    if (result?.statusCode == 200) {
                        Log.d(TAG, it.data!!.data.toString())
                        SavePrefResult(result, "signin")

                        CommonUtilities.fireActivityIntent(
                            this,
                            Intent(this, HomeActivity::class.java),
                            isFinish = true,
                            isForward = true
                        )

                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())
                        binding.btsend.isEnabled = true
                    }
                }
            }
        })
    }


    fun SavePrefResult(response: ResponseSignUpSignInDM2, from_where: String) {
        binding.btsend.isEnabled = true



        if (from_where == "signup") {
            CommonUtilities.putBoolean(this, Constants.FROM_SIGNUP, true)
        } else {
            CommonUtilities.putBoolean(this, Constants.IS_LOGIN, true)
        }

        CommonUtilities.putString(this, Constants.FIRST_NAME, response.data!!.userData!!.firstName)
        CommonUtilities.putString(this, Constants.LAST_NAME, response.data!!.userData!!.lastName)
        CommonUtilities.putString(this, Constants.SENDER_ID, response.data!!.userData!!.id)
        response.data!!.userData!!.isKycDone?.let {
            CommonUtilities.putBoolean(
                this, Constants.IS_KYC_DONE,
                it
            )
        }
        CommonUtilities.putString(this, Constants.TOKEN, response.data!!.userData!!.token)
        CommonUtilities.putString(
            this,
            Constants.DWOLLA_CUSTOMER_ID,
            response.data!!.userData!!.dwollaCustomerId
        )
        CommonUtilities.putString(
            this,
            Constants.PHONE_WITH_CODE,
            response.data!!.userData!!.phoneNo
        )
        CommonUtilities.putString(
            this,
            Constants.PHONE_NO,
            response.data!!.userData!!.phoneNoWithoutCountryCode
        )
        CommonUtilities.putString(
            this,
            Constants.PHONE_CODE,
            response.data!!.userData!!.countryCode
        )
        CommonUtilities.putBoolean(this, Constants.HAVE_TOKEN, true)
        response.data!!.userData!!.isDwollaOnboardingCompleted?.let {
            CommonUtilities.putBoolean(
                this,
                Constants.IS_ACCOUNT_CREATED,
                it
            )
        }
        response.data!!.userData!!.isAccountVerified?.let {
            CommonUtilities.putBoolean(
                this,
                Constants.IS_ACCOUNT_VERIFIED,
                it
            )
        }
        /*  if (response.data.imgUrl != null && response.data.imgUrl != "") {
              CommonUtilities.putString(
                  this,
                  Constants.PROFILE_PIC,
                  Constants.URL_FOR_IMAGE + response.data.imgUrl
              )
          }*/


    }


//    fun testing(){
//        if(intent.getStringExtra(Constants.FROM_WHICH_SCREEN).toString()==Constants.SIGNUP){
//            SignUpViewModel.signUpUser("deepika","testingser15@gmail.com",
//                "+919914105398",
//                "LuBpQvOZ8cmCgdPlL2c0",
//                device_token
//            )
//        }
//        else if(intent.getStringExtra(Constants.FROM_WHICH_SCREEN).toString()==Constants.LOGIN){
//            SignUpViewModel.signInUser(
//                "+919914105398",
//                "LuBpQvOZ8cmCgdPlL2c0",
//                device_token
//            )
//        }
//    }

}











