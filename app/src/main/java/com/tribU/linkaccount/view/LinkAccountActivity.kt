package com.tribU.linkaccount.view

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Build
import android.text.TextPaint
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tribU.R
import com.tribU.SplashNewActivity
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.LetsGetStartedActivityDataBinding
import com.tribU.databinding.LinkAccountActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.view.HomeActivity
import com.tribU.linkaccount.callbacks.LinkAccountViewCallbacks
import com.tribU.login.view.LoginActivity
import com.tribU.on_boarding.callbacks.LetsGetStartedViewCallbacks
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import java.util.*

class LinkAccountActivity :
    BaseDataBindingActivity<LinkAccountActivityDataBinding>(R.layout.activity_link_account),
    LinkAccountViewCallbacks {


    lateinit var device_token: String
    private val TAG = LinkAccountActivity::class.java.simpleName
    var firebaseId: String? = null

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this


        val paint: TextPaint = binding.tvHeading.getPaint()
        val width = paint.measureText("Linked Successfully")


        val textShader: Shader = LinearGradient(
            0f, 0f, width, binding.tvHeading.textSize, intArrayOf(
                Color.parseColor("#FFFFFFFF"),
                Color.parseColor("#FECC2F")
            ), null, Shader.TileMode.REPEAT
        )

        binding.tvHeading.paint.setShader(textShader)
        binding.tvHeading.text = "Linked\nSuccessfully"


        //    loadWebView(Constants.BASE_URL+ "dwolla/addFundingSource/"+CommonUtilities.getString(this, Constants.TOKEN))


        if (Constants.IS_PRODUCTION_ENVIORNMENT) {

            loadWebView(Constants.BASE_URL + "dwolla/addFundingSource/" + CommonUtilities.getString(this, Constants.TOKEN))
        } else {
            loadWebView(Constants.BASE_URL_STAGING+ "/dwolla/addFundingSource/" + CommonUtilities.getString(this, Constants.TOKEN))

        }

    }

    private fun loadWebView(url: String) {
        binding.webview.settings.javaScriptEnabled = true
      //  binding.webview.settings.useWideViewPort = true
        binding.webview.loadUrl(url)


        CommonUtilities.showLoader(this)


        binding.webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                val url = request?.url.toString()
                view?.loadUrl(url)


                Log.e("TAG", "shouldOverrideUrlLoading: " + url)

                if (url!!.contains("success")) {

                    binding.webview.visibility = View.GONE
                    binding.clPleaseWait.visibility = View.VISIBLE


                    savePref()

                }

                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)

                //  CommonUtilities.showLoader(this@LinkAccountActivity)


                Log.e("TAG", "onPageStarted: " + url)

                if (url!!.contains("success")) {

                    binding.webview.visibility = View.GONE
                    binding.clPleaseWait.visibility = View.VISIBLE

                    savePref()

                }

            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)

                CommonUtilities.hideLoader()
            }

            @RequiresApi(Build.VERSION_CODES.M)
            override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
            ) {
                super.onReceivedError(view, request, error)
                CommonUtilities.hideLoader()
                CommonUtilities.showToastString(
                    this@LinkAccountActivity,
                    error.description.toString()
                )

            }
        }
    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        //SignUpViewModel = ViewModelProvider(this, viewModelFactory).get(LanguageVM::class.java)
    }


    override fun backClick() {
        onBackPressed()
    }

    override fun nextClick() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, HomeActivity::class.java),
            isFinish = true,
            isForward = true
        )
    }

    override fun onBackPressed() {
        //super.onBackPressed()
       // finishAffinity()
    }

    fun savePref()
    {
        CommonUtilities.putBoolean(this,Constants.IS_ACCOUNT_VERIFIED,true)
        CommonUtilities.putBoolean(this,Constants.IS_LOGIN,true)

    }

}
















