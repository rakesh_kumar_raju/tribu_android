package com.tribU.linkaccount.callbacks

interface LinkAccountViewCallbacks {

    fun backClick()
    fun nextClick()
}