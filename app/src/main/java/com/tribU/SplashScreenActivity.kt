package com.tribU

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.bishaan.onboarding.view.OnboardingActivity
import com.bumptech.glide.Glide
import com.tribU.home.view.HomeActivity
import com.tribU.home.viewmodel.HomeVM
import com.tribU.home.viewmodel.SplashVM
import com.tribU.login.view.LoginActivity
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.on_boarding.view.LetsGetStartedActivity
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import javax.inject.Inject

class SplashScreenActivity : AppCompatActivity() {


    private lateinit var app_name_text: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        splashHandler()
    }


    fun splashHandler() {

        Handler().postDelayed({
            var is_old_user = CommonUtilities.getBoolean(this, Constants.IS_OLD_USER)
            if (!is_old_user) {

                CommonUtilities.fireActivityIntent(
                    this,
                    Intent(this, OnboardingActivity::class.java)
                        .putExtra(Constants.FROM_WHICH_SCREEN, "splash"),
                    isFinish = true,
                    isForward = true
                )


            } else {

                if (!CommonUtilities.getBoolean(this, Constants.IS_LOGIN)) {
                    CommonUtilities.fireActivityIntent(
                        this,
                        Intent(this, LetsGetStartedActivity::class.java),
                        //   Intent(this, LoginActivity::class.java),
                        isFinish = true,
                        isForward = true
                    )


                } else {

                    CommonUtilities.fireActivityIntent(
                        this,
                        Intent(this, HomeActivity::class.java),
                        isFinish = true,
                        isForward = true
                    )
                }
            }
        }, 10)

    }


}
