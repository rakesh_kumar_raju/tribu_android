package com.tribU.signup.model

import com.google.gson.annotations.SerializedName
import com.tribU.home.model.PopUpText

data class ResponseTrubuStatusDM(

    @field:SerializedName("data")
    val data: StatusData? = null,

    @field:SerializedName("message")
    val message: String? = null,

/*	@field:SerializedName("error")
	val error: String? = null,*/

    @field:SerializedName("statusCode")
    val statusCode: Int? = null
)

data class StatusData(

    @field:SerializedName("status")
    val status: Boolean? = null,

    @field:SerializedName("popUpText")
    val popUpText: PopUpText? = null
)

