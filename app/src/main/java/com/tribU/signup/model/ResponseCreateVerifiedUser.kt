package com.tribU.signup.model

import com.google.gson.annotations.SerializedName

data class ResponseCreateVerifiedUser(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)



data class Data(

	@field:SerializedName("phoneNoWithoutCountryCode")
	val phoneNoWithoutCountryCode: String? = null,

	@field:SerializedName("lastName")
	val lastName: String? = null,

/*	@field:SerializedName("deviceDetails")
	val deviceDetails: List<DeviceDetailsItem>? = null,*/

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("address1")
	val address1: String? = null,

	@field:SerializedName("postalCode")
	val postalCode: String? = null,

	@field:SerializedName("dwollaCustomerId")
	val dwollaCustomerId: String? = null,

	@field:SerializedName("dateOfBirth")
	val dateOfBirth: String? = null,
/*

	@field:SerializedName("processingTribuRequests")
	val processingTribuRequests: List<Any?>? = null,
*/

	@field:SerializedName("phoneNo")
	val phoneNo: String? = null,

	@field:SerializedName("ssn")
	val ssn: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("isDwollaOnboardingCompleted")
	val isDwollaOnboardingCompleted: Boolean? = null,

	@field:SerializedName("totalTribuRecieved")
	val totalTribuRecieved: Int? = null,

	@field:SerializedName("firebaseId")
	val firebaseId: String? = null,

	@field:SerializedName("countryCode")
	val countryCode: String? = null,

	@field:SerializedName("isKycDone")
	val isKycDone: Boolean? = null,

	@field:SerializedName("isAccountVerified")
	val isAccountVerified: Boolean? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

/*	@field:SerializedName("fundingSource")
	val fundingSource: List<Any?>? = null,*/

	@field:SerializedName("totalTribuSent")
	val totalTribuSent: Int? = null
)
