package com.tribU.signup.model

import com.google.gson.annotations.SerializedName

data class ResponseSendTribuDM(

    @field:SerializedName("data")
    val data: DataValue? = null,

    @field:SerializedName("error")
    val error: String? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("statusCode")
    val statusCode: Int? = null
)

data class DataValue(


    @field:SerializedName("amount")
    val amount: Double? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("requestId")
    val requestId: String? = null,


    @field:SerializedName("senderId")
    val senderId: String? = null


)
