package com.tribU.signup.model

import com.google.gson.annotations.SerializedName

data class ResponseSignUpSignInDM2(

	@field:SerializedName("data")
	val data: SignupSigninData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class SignupSigninData(

	@field:SerializedName("userData")
	val userData: UserData? = null
)

data class DeviceDetailsItem(

	@field:SerializedName("deviceType")
	val deviceType: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("deviceToken")
	val deviceToken: String? = null
)

data class UserData(

	@field:SerializedName("phoneNoWithoutCountryCode")
	val phoneNoWithoutCountryCode: String? = null,

	@field:SerializedName("lastName")
	val lastName: String? = null,

/*
	@field:SerializedName("deviceDetails")
	val deviceDetails: List<DeviceDetailsItem?>? = null,
*/

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("address1")
	val address1: String? = null,

	@field:SerializedName("postalCode")
	val postalCode: String? = null,

	@field:SerializedName("dwollaCustomerId")
	val dwollaCustomerId: String? = null,

	@field:SerializedName("dateOfBirth")
	val dateOfBirth: String? = null,

	@field:SerializedName("processingTribuRequests")
	val processingTribuRequests: ArrayList<String>? = null,

	@field:SerializedName("phoneNo")
	val phoneNo: String? = null,

	@field:SerializedName("ssn")
	val ssn: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("isDwollaOnboardingCompleted")
	val isDwollaOnboardingCompleted: Boolean? = null,

	@field:SerializedName("totalTribuRecieved")
	val totalTribuRecieved: Double? = null,

	@field:SerializedName("firebaseId")
	val firebaseId: String? = null,

	@field:SerializedName("countryCode")
	val countryCode: String? = null,

	@field:SerializedName("isKycDone")
	val isKycDone: Boolean? = null,

	@field:SerializedName("isAccountVerified")
	val isAccountVerified: Boolean? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("fundingSource")
	val fundingSource: ArrayList<FundingSourceItem>? = null,

	@field:SerializedName("totalTribuSent")
	val totalTribuSent: Double? = null
)

data class FundingSourceItem(

	@field:SerializedName("sourceId")
	val sourceId: String? = null,

	@field:SerializedName("isPrimary")
	val isPrimary: Boolean? = null,

	@field:SerializedName("_id")
	val id: String? = null
)