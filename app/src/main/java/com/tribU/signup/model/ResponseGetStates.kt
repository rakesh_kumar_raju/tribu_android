package com.tribU.signup.model

import com.google.gson.annotations.SerializedName

data class ResponseGetStates(

	@field:SerializedName("data")
	val data: ArrayList<StatesData>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("error")
	val error: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class StatesData(

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("name")
	val name: String? = null
)
