package com.tribU.signup.view

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.util.Patterns
import android.widget.DatePicker
import androidx.annotation.RequiresApi
import com.tribU.R
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.GettingStarted1ActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.login.view.LoginActivity
import com.tribU.signup.callbacks.GettingStarted1ViewCallbacks
import com.tribU.transaction.fragments.TransactionFragment
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import java.text.SimpleDateFormat
import java.util.*

class GettingStarted1Activity :
    BaseDataBindingActivity<GettingStarted1ActivityDataBinding>(R.layout.activity_getting_started1),
    GettingStarted1ViewCallbacks, DatePickerDialog.OnDateSetListener {


    lateinit var device_token: String
    private val TAG = GettingStarted1Activity::class.java.simpleName
    var firebaseId: String? = null

    companion object {

        @SuppressLint("StaticFieldLeak")
        var mInstance: GettingStarted1Activity? = null



        var firstName =""
        var lastName =""
        var email =""
        var dob =""

    }


    fun getInstance(): GettingStarted1Activity? {
        return mInstance
    }

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this
        mInstance = this


    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        //SignUpViewModel = ViewModelProvider(this, viewModelFactory).get(LanguageVM::class.java)
    }


    override fun signinClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, LoginActivity::class.java)
                .putExtra(Constants.FROM_WHICH_SCREEN, Constants.LOGIN),
            isFinish = true,
            isForward = true
        )
    }

    override fun backClicked() {
        onBackPressed()
    }

    override fun nextClicked() {


        if (binding.edFirstName.text.length < 1) {
            CommonUtilities.showToast(this, R.string.please_enter_first_name)
        } else if (binding.edLastName.text.length < 1) {
            CommonUtilities.showToast(this, R.string.please_enter_last_name)
        } else if (binding.edEmail.text.length < 1) {
            CommonUtilities.showToast(this, R.string.please_enter_your_email)
        } else if (binding.edEmail.text.length < 1) {
            CommonUtilities.showToast(this, R.string.please_enter_your_email)
        } else if (!(Patterns.EMAIL_ADDRESS.matcher(binding?.edEmail!!.text.toString())
                .matches())
        ) {
            CommonUtilities.showToast(this, R.string.please_enter_valid_email)
        } else if (binding.edDob.text.length < 1) {
            CommonUtilities.showToast(this, R.string.select_dob)
        }else {


            firstName = binding.edFirstName.text.toString()
            lastName = binding.edLastName.text.toString()
            email = binding.edEmail.text.toString()
            dob = binding.edDob.text.toString()
            dob = binding.edDob.text.toString()

            CommonUtilities.fireActivityIntent(
                this,
                Intent(this, GettingStarted2Activity::class.java),
                isFinish = false,
                isForward = true
            )

        }
    }

    override fun calendarClicked() {
        datePickerDialogShow()
    }


    fun datePickerDialogShow() {
        val calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        var datePickerDialog =
            DatePickerDialog(this, R.style.datepicker, this, mYear, mMonth, mDay)
        datePickerDialog!!.getDatePicker().maxDate = calendar.timeInMillis
        datePickerDialog?.show()
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar2: Calendar = Calendar.getInstance()
        calendar2.set(year, month, dayOfMonth, 0, 0, 0)
        val selectedDate = calendar2.time
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        var date= dateFormat.format(selectedDate)

        binding.edDob.setText(date.toString())

    }

}
















