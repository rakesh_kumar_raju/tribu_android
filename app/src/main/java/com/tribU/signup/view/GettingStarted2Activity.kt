package com.tribU.signup.view

import android.content.Intent
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.net.Uri
import android.text.TextPaint
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.bishaan.onboarding.viewmodel.SignupProcessVM
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import com.tribU.R
import com.tribU.SplashNewActivity
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.GettingStarted2ActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.linkaccount.view.LinkAccountActivity
import com.tribU.login.view.LoginActivity
import com.tribU.network.Status
import com.tribU.signup.callbacks.GettingStarted2ViewCallbacks
import com.tribU.signup.model.Data
import com.tribU.signup.model.StatesData
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import com.tribU.utils.BottomSheet
import com.tribU.utils.BottomUpDialogAdapter
import java.util.*
import javax.inject.Inject

class GettingStarted2Activity :
    BaseDataBindingActivity<GettingStarted2ActivityDataBinding>(R.layout.activity_getting_started2),
    GettingStarted2ViewCallbacks {

    var regionList: MutableList<StatesData> = ArrayList()
    var list: ArrayList<String> = ArrayList()
    private val mBottomUpSheet = BottomSheet.getSheet()


    @set:Inject
    lateinit var viewModFactory: ViewModelFactory
    lateinit var vm: SignupProcessVM


    lateinit var device_token: String
    private val TAG = GettingStarted2Activity::class.java.simpleName
    var firebaseId: String? = null
    var regionId = ""

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this




        if (SplashNewActivity.appData != null) {
            binding.tvHeading.text =
                SplashNewActivity.appData.accountVerificationScreen!!.heading
            binding.tvTitle.text =
                SplashNewActivity.appData.accountVerificationScreen!!.paragraph1

            Glide.with(this)
                .load(SplashNewActivity.appData.accountVerificationScreen!!.imgUrl)
                .into(binding.ivPleaseWait)


        }

        /**
         *  if coming from splash ( if user is created + KYC done but not verified)
         * */
        var from_which_screen = intent.getStringExtra(Constants.FROM_WHICH_SCREEN).toString()
        if (from_which_screen == Constants.SPLASH) {


            binding.clOne.visibility = View.GONE
            binding.clTwo.visibility = View.GONE
            binding.clPleaseWait.visibility = View.VISIBLE

        }
        /**
         * */


        val paint: TextPaint = binding.tvHeading.getPaint()
        val width = paint.measureText("Please wait...")


        val textShader: Shader = LinearGradient(
            0f, 0f, width, binding.tvHeading.textSize, intArrayOf(
                Color.parseColor("#FFFFFFFF"),
                Color.parseColor("#FECC2F")
            ), null, Shader.TileMode.REPEAT
        )

        binding.tvHeading.paint.setShader(textShader)
        binding.tvHeading.text = "Please wait..."

        CommonUtilities.showLoader(this)

        vm.getStates()



        if (!SplashNewActivity.appData.linkAccountScreen!!.paragraph1.isNullOrEmpty()) {
            binding.tvTitle.text = SplashNewActivity.appData.linkAccountScreen!!.paragraph1

        }


    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        vm = ViewModelProvider(this, viewModFactory).get(SignupProcessVM::class.java)

        setupObserver()
    }

    override fun stateClicked() {
        if (list != null) {
            //  regionId = binding.edState.text.toString()

            regionBottomSheet(list)
        }

    }

    override fun cityClicked() {
        //   TODO("Not yet implemented")
    }

    override fun createAccountClicked() {


        if (binding.edAddress.text.length < 1) {
            CommonUtilities.showToast(this, R.string.enter_address)
        } else if (binding.edState.text.length < 1) {
            CommonUtilities.showToast(this, R.string.select_state)
        } else if (binding.edCity.text.length < 1) {
            CommonUtilities.showToast(this, R.string.enter_city)
        } else if (binding.edZipCode.text.length < 1) {
            CommonUtilities.showToast(this, R.string.enter_zip)
        } else if (binding.edSsn.text.length < 1) {
            CommonUtilities.showToast(this, R.string.enter_ssn)
        }
        else if (!binding.checkbox.isChecked) {
            CommonUtilities.showToastString(this, "Please Agree to the Terms and Conditions")
        }
        else {


            var obj = JsonObject()
            obj.addProperty("firstName", GettingStarted1Activity.firstName)
            obj.addProperty("lastName", GettingStarted1Activity.lastName)
            obj.addProperty("email", GettingStarted1Activity.email)
            obj.addProperty("dateOfBirth", GettingStarted1Activity.dob)

            obj.addProperty("address1", binding.edAddress.text.toString())
            obj.addProperty("city", binding.edCity.text.toString())
            obj.addProperty("state", regionId)
            obj.addProperty("ssn", binding.edSsn.text.toString())
            obj.addProperty("postalCode", binding.edZipCode.text.toString())

            CommonUtilities.showLoader(this)

            vm.createVerifiedUserApi(
                CommonUtilities.getString(this, Constants.TOKEN).toString(),
                obj
            )


/*            binding.clOne.visibility = View.GONE
            binding.clTwo.visibility = View.GONE
            binding.clPleaseWait.visibility = View.VISIBLE*/


        }
    }
    override fun terms1Clicked() {

        val url = "https://www.sanabriatech.com/legal"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)


    }


    override fun terms2Clicked() {

        val url = "https://www.dwolla.com/legal/tos/"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)


    }


    override fun privacy1Clicked() {

        val url = "https://app.termly.io/document/privacy-policy/2214c02c-7199-425a-a6cc-3c73ce7714fe"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)

    }

    override fun privacy2Clicked() {

        val url ="https://www.dwolla.com/legal/privacy/"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)


    }



    override fun signinClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, LoginActivity::class.java)
                .putExtra(Constants.FROM_WHICH_SCREEN, Constants.LOGIN),
            isFinish = true,
            isForward = true
        )
    }

    override fun backClicked() {
        onBackPressed()
    }

    override fun linkAccountClicked() {

        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, LinkAccountActivity::class.java),
            isFinish = true,
            isForward = true
        )
    }


    fun setupObserver() {

        vm.responseRegion.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    Log.e("TAG", "SUCCESS")


                    if (it.data!!.statusCode == 200) {

                        if (it.data.data != null) {

                            regionList.clear()
                            list.clear()


                            regionList.addAll(it.data.data)

                            for (i in regionList) {
                                i.name?.let { it1 -> list.add(it1) }
                            }


                        }
                    } else {
                        //  CommonUtilities.showToast(requireActivity(), it.data.message.toString())
                    }


                }
                Status.LOADING -> {

                    Log.e("TAG", "LOADING")
                    //  CommonUtilities.showLoader(requireActivity())

                }
                Status.ERROR -> {

                    CommonUtilities.hideLoader()
                    Log.e("TAG", "ERROR ")

                    //  CommonUtilities.showToast(requireActivity(), it?.message.toString())


                }
            }
        })

        vm.responseCreateUser.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    Log.e("TAG", "SUCCESS")


                    if (it.data!!.statusCode == 200) {


                        binding.clOne.visibility = View.GONE
                        binding.clTwo.visibility = View.GONE
                        binding.clPleaseWait.visibility = View.VISIBLE

                        CommonUtilities.putBoolean(this, Constants.IS_KYC_DONE, true)

                        SavePrefResult(it.data.data)


                        if (it.data.data != null) {


                        }

                    } else {
                        CommonUtilities.showToastString(this, it.data.data.toString())
                    }


                }
                Status.LOADING -> {

                    Log.e("TAG", "LOADING")
                    //   CommonUtilities.showLoader(this)

                }
                Status.ERROR -> {

                    CommonUtilities.hideLoader()
                    Log.e("TAG", "ERROR ")

                    CommonUtilities.showToastString(this, it?.message.toString())


                }
            }
        })


    }


    /**
     * select state bottom sheet
     * */

    fun regionBottomSheet(list: ArrayList<String>) {
        mBottomUpSheet.regionBottomSheet(
            this,
            list,
            "Select State"
        )
        mBottomUpSheet.setCustomObjectListener(object :
            BottomSheet.OnOptionSelectedListener {
            override fun onOptionSelected(
                mAdapter: BottomUpDialogAdapter,
                position: Int,
                selectedOption: String
            ) {
                if (!mAdapter.selectedItem(position).equals("")) {

                    regionId = regionList.get(position).code.toString()
                    binding.edState.setText(regionList.get(position).name.toString())


                }
            }
        })

        mBottomUpSheet.show()
    }


    fun SavePrefResult(userData: Data?) {


        CommonUtilities.putString(this, Constants.FIRST_NAME, userData!!.firstName)
        CommonUtilities.putString(this, Constants.LAST_NAME, userData!!.lastName)
        CommonUtilities.putString(this, Constants.SENDER_ID, userData!!.id)
        CommonUtilities.putString(this, Constants.TOKEN, userData!!.token)
        CommonUtilities.putString(
            this,
            Constants.DWOLLA_CUSTOMER_ID,
            userData!!.dwollaCustomerId
        )
        CommonUtilities.putString(
            this,
            Constants.PHONE_WITH_CODE,
            userData!!.phoneNo
        )
        CommonUtilities.putString(
            this,
            Constants.PHONE_NO,
            userData!!.phoneNoWithoutCountryCode
        )
        CommonUtilities.putString(
            this,
            Constants.PHONE_CODE,
            userData!!.countryCode
        )
        CommonUtilities.putBoolean(this, Constants.HAVE_TOKEN, true)


    }


}
















