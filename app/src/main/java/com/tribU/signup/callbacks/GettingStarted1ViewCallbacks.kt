package com.tribU.signup.callbacks

interface GettingStarted1ViewCallbacks {
    fun signinClicked()
    fun backClicked()
    fun nextClicked()
    fun calendarClicked()

}