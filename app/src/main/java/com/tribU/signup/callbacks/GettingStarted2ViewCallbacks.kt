package com.tribU.signup.callbacks

interface GettingStarted2ViewCallbacks {
    fun stateClicked()
    fun cityClicked()
    fun createAccountClicked()


    fun signinClicked()
    fun backClicked()
    fun linkAccountClicked()

    fun terms1Clicked()
    fun terms2Clicked()
    fun privacy1Clicked()
    fun privacy2Clicked()


}