package com.tribU.signup.callbacks

interface SignupViewCallbacks {
    fun signupClicked()
    fun loginClicked()

}