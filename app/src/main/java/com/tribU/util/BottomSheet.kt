package com.tribU.utils


import android.app.Activity
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tribU.R


class BottomSheet {

    /**
     * Dummy Data
     */
    private var mDataList :ArrayList<String> = ArrayList();

    /**
     * Adapter of RecyclerView
     */
    var mAdapter: BottomUpDialogAdapter


    /**
     * LayoutManager for RecyclerView
     */
    lateinit var mLayoutManager: LinearLayoutManager


    /**
     * BottomSheet Object
     */
    private var sheet: BottomSheetDialog? = null
    /**
     * Recycler LayoutAnimation
     */
    lateinit var layout_animation: LayoutAnimationController
    /**
     * Listener
     */
    private var mListener: OnOptionSelectedListener? = null
    /** waiver reason
     * listener
     */

    private var mCancelAppointmentlistener: CancelAppointmemtListener? = null

    // private val mCancelled = CancelAppointmentActivity.getCancel()
    /** pricing
     * outstanding
     * listner
     */



    companion object {

        var mInstance: BottomSheet? = null

        fun getSheet(): BottomSheet {
            return if (mInstance != null) {
                mInstance as BottomSheet
            } else {
                mInstance = BottomSheet()
                mInstance!!
            }
        }
    }

    init {
        mAdapter = BottomUpDialogAdapter(mDataList)

    }

    /** set region bottom sheet
     *
     */

    var code = ""
    fun regionBottomSheet(
        mActivity: Activity,
        mCancellationList: ArrayList<String>,
        mTitle: String
    )
    {
        mAdapter.updateDataSet(mCancellationList)
        this.mAdapter.notifyDataSetChanged()

        mLayoutManager = LinearLayoutManager(mActivity.applicationContext)
        layout_animation = AnimationUtils.loadLayoutAnimation(mActivity, R.anim.layout_bottom_to_up)

        if (sheet != null) {
            sheet?.cancel()
        }

        sheet = BottomSheetDialog(mActivity)

        val sheet_view = mActivity.layoutInflater.inflate(R.layout.dialog_bottomup, null)
        sheet?.setContentView(sheet_view)
        var title_text=sheet_view.findViewById<TextView>(R.id.tv_title)
        var recyclerView=sheet_view.findViewById<RecyclerView>(R.id.rv_cancellation)
        title_text.text = mTitle
        recyclerView.layoutManager = mLayoutManager
        recyclerView.layoutAnimation = layout_animation
        recyclerView.adapter = mAdapter



        recyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(mActivity, object :
                RecyclerItemClickListener.OnItemClickListener {
                override fun onItemClick(view: View?, position: Int) {
                    mListener?.onOptionSelected(mAdapter, position, mAdapter.selectedItem(position))


                    hide()
                    sheet = null
                }
            })
        )

    }




    /** set listners
     *
     */
    fun setCustomObjectListener(listener: OnOptionSelectedListener) {
        this.mListener = listener
    }


    fun setCancelAppointmentObjectListener(listener: CancelAppointmemtListener) {
        this.mCancelAppointmentlistener = listener
    }



    /**
     * Interface for Passing Data
     */
    interface OnOptionSelectedListener {
        fun onOptionSelected(
            mAdapter: BottomUpDialogAdapter,
            position: Int,
            selectedOption: String
        )


    }

    interface CancelAppointmemtListener {
        fun cancel_proceed()

    }



    fun show() {
        sheet?.show()
    }

    fun hide() {
        sheet?.dismiss()
    }





}


