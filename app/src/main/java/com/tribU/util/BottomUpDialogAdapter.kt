package com.tribU.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tribU.R

class BottomUpDialogAdapter(private var myDataset: ArrayList<String>) :
    RecyclerView.Adapter<BottomUpDialogAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var option: TextView = view.findViewById(R.id.tv_option)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_dialog_bottomup, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.option.text = myDataset[position]
        holder.option.isSelected = true
    }

    fun selectedItem(position: Int): String {
        return myDataset[position]
    }

    override fun getItemCount() = myDataset.size



    fun updateDataSet(mNewList: ArrayList<String>) {
        this.myDataset = mNewList
    }


}