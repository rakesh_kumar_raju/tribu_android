package com.tribU.util

object Constants {
    const val TRIBU = "tribu"
    val TOKEN = "token"
    val IS_LOGIN = "is_login"
    val DEVICE_TOKEN = "device_token"
    val DWOLLA_CUSTOMER_ID = "DWOLLA_CUSTOMER_ID"
    val ID = "id"
    val NAME = "name"
    val OTP = "otp"

    /*  val BASE_URL = "http://ec2-3-101-127-5.us-west-1.compute.amazonaws.com:4000/"
      val URL_FOR_IMAGE = "http://ec2-3-101-127-5.us-west-1.compute.amazonaws.com:4000"*/
//  val BASE_URL = "http://3.101.127.5:8080/"
//    val URL_FOR_IMAGE = "http://3.101.127.5:8080"


    val BASE_URL_STAGING = "http://128.199.1.6:4000"
    val URL_FOR_IMAGE_STAGING = "http://128.199.1.6:4000"

    val BASE_URL = "http://ec2-3-101-127-5.us-west-1.compute.amazonaws.com:4000/auth/"
    val URL_FOR_IMAGE = "http://ec2-3-101-127-5.us-west-1.compute.amazonaws.com:4000"

    val PHONE_CODE = "phone_code"
    val PHONE_NO = "phone_no"
    val PHONE_WITH_CODE = "phone_with_code"
    val FIRST_NAME = "FIRST_NAME"
    val LAST_NAME = "LAST_NAME"
    val IS_KYC_DONE = "IS_KYC_DONE"
    val IS_DWOLLA_ONBOARDING_COMPLETE = "IS_DWOLLA_ONBOARDING_COMPLETE"
    val FROM_WHICH_SCREEN = "from_which_screen"
    val SIGNUP = "signUp"
    val LOGIN = "login"
    val SPLASH = "splash"
    val SHOW_MESSAGE = "show_message"
    val API_TOKEN = "api_token"
    val HAVE_TOKEN = "have_token"
    val IS_OLD_USER = "IS_OLD_USER"
    val ACCOUNT_CREATED = "account_created"
    val UNDER_VERIFICATION = "under_verification"
    val VERIFICATION_DONE = "verification_done"
    val FROM_HOME = "from_home"
    val PROFILE_PIC = "profile_pic"
    val IS_ACCOUNT_CREATED = "is_account_created"
    val REQUEST_ID = "request_id"
    val IS_ACCOUNT_VERIFIED = "is_account_verified"
    val FROM_SIGNUP = "from_signup"
    val SENDER_ID = "SENDER_ID"
    val FIREBASE_ID = "firebase_id"
    val SENT_SUCCESSFUL = "sent_successful"
    val RECEIVED_SUCCESSFUL = "received_successful"
    val TIME_OUT = "time_out"
    val ALREADY_SENT = "already_sent"
    var TRANSACTION_DONE = false
    var IS_PRODUCTION_ENVIORNMENT = false



    val PROFILE = "profile"


    val CLICK_TYPE = "click_type"
    val ADD = "add"
    val TRANSFER = "transfer"

    //transaction types
    var RECEIVED_MONEY = "recievedMoney"
    var OWN_WALLET_TO_BANK_ACCOUNT = "ownWalletToOwnBankAccount"
    var OWN_BANK_ACCOUNT_TO_WALLET = "ownBankAccountToOwnWallet"
    var OWN_BANK_ACCOUNT_TO_RECEIVER_WALLET = "ownBankAccountToReciverWallet"
    var OWN_WALLET_TO_RECEIVER_WALLET = "ownWalletToReciverWallet"



    var RECEIVED_MONEY_TEXT = "Tribu received"
    var OWN_WALLET_TO_BANK_ACCOUNT_TEXT = "Money Sent from wallet to bank"
    var OWN_BANK_ACCOUNT_TO_WALLET_TEXT = "Money sent from bank to wallet"
    var OWN_BANK_ACCOUNT_TO_RECEIVER_WALLET_TEXT = "Money sent from bank account"
    var OWN_WALLET_TO_RECEIVER_WALLET_TEXT = "Tribu Sent"


    //transaction STATUS
    var PENDING = "pending"
    var PROCESSED = "processed"




}