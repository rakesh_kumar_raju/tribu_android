package com.tribU.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.tribU.R
import com.tribU.login.view.LoginActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class CommonUtilities {
    companion object {
        var CATEGORY_VALUE = ""
        var INSURANCE_VALUE = ""
        var handler_value = ""

        @JvmStatic
        fun putInt(activity: Context, name: String?, value: Int) {
            val preferences: SharedPreferences =
                activity.getSharedPreferences(Constants.TRIBU, Context.MODE_PRIVATE)
            preferences.edit().putInt(name, value).apply()
        }

        fun getInt(activity: Context, name: String?): Int {
            val preferences: SharedPreferences =
                activity.getSharedPreferences(Constants.TRIBU, Context.MODE_PRIVATE)
            return preferences.getInt(name, 0)
        }


        fun putString(activity: Context, name: String?, value: String?) {
            val preferences: SharedPreferences =
                activity.getSharedPreferences(Constants.TRIBU, Context.MODE_PRIVATE)
            preferences.edit().putString(name, value).apply()
        }

        fun getString(activity: Context, name: String?): String? {
            val preferences: SharedPreferences =
                activity.getSharedPreferences(Constants.TRIBU, Context.MODE_PRIVATE)
            return preferences.getString(name, "")
        }


        fun putBoolean(activity: Context, name: String?, value: Boolean) {
            val preferences: SharedPreferences =
                activity.getSharedPreferences(Constants.TRIBU, Context.MODE_PRIVATE)
            preferences.edit().putBoolean(name, value).apply()
        }

        fun getBoolean(activity: Context, name: String?): Boolean {
            val preferences: SharedPreferences =
                activity.getSharedPreferences(Constants.TRIBU, Context.MODE_PRIVATE)
            return preferences.getBoolean(name, false)
        }

        fun clearPrefrences(activity: Context) {
            val preferences: SharedPreferences = activity.getSharedPreferences(
                Constants.TRIBU, Context.MODE_PRIVATE
            )
            preferences.edit().clear().apply()
        }

        //get the current version number and name
        fun getVersionInfo(activity: Activity): String? {
            var versionName = ""
            var versionCode = -1
            try {
                val packageInfo =
                    activity.packageManager.getPackageInfo(activity.packageName, 0)
                versionName = packageInfo.versionName
                versionCode = packageInfo.versionCode
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            Log.e("versionName", "" + versionName)
            Log.e("versionCode", "" + versionCode)
            return versionName
        }


        fun printHashKey(pContext: Context) {
            try {
                val info = pContext.packageManager
                    .getPackageInfo(pContext.packageName, PackageManager.GET_SIGNATURES)
                for (signature in info.signatures) {
                    val md = MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())
                    val hashKey = String(Base64.encode(md.digest(), 0))
                    Log.e("Login", "printHashKey() Hash Key: $hashKey")
                }
            } catch (e: NoSuchAlgorithmException) {
                Log.e("Login", "printHashKey()", e)
            } catch (e: Exception) {
                Log.e("Login", "printHashKey()", e)
            }
        }

        fun showToast(context: Context?, msg: Int?) {
            Toast.makeText(context, msg!!, Toast.LENGTH_SHORT).show()
        }

        fun showToastString(context: Context?, msg: String?) {
            Toast.makeText(context, msg!!, Toast.LENGTH_SHORT).show()
        }


        fun addZero(num: Int): String? {
            return if (num < 10) {
                "0$num"
            } else {
                "" + num
            }
        }


        fun changeDateFormat(
            dateStr: String?,
            oldFormat: String,
            newFormat: String
        ): String? {
            val inputFormat = SimpleDateFormat(oldFormat)
            val outputFormat = SimpleDateFormat(newFormat)
            var date: Date? = null
            var str: String? = null
            try {
                date = inputFormat.parse(dateStr)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()

            }
            return str
        }


        fun getDeviceToken(context: Context): String? {
            val android_id = Settings.Secure.getString(
                context.contentResolver,
                Settings.Secure.ANDROID_ID
            )
            Log.e("android_id", "" + android_id)
            return android_id
        }

        fun getFilePath(
            selectedImage: Uri?,
            context: Context
        ): String? {
            val filePathColumn =
                arrayOf(MediaStore.Images.Media.DATA)
            val cursor =
                context.contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
            cursor!!.moveToFirst()
            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            val filePath = cursor.getString(columnIndex)
            cursor.close()
            println("File path $filePath")
            return filePath
        }

        fun isConnectingToInternet(context: Context?): Boolean? {
            val connectivity =
                context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivity != null) {
                val info = connectivity.allNetworkInfo
                if (info != null) for (i in info.indices) {
                    if (info[i].state == NetworkInfo.State.CONNECTED) {
                        return true
                    }
                }
            }
            return false
        }


        fun changeStatusBarColor(activity: Activity, color: Int) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = activity.window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.statusBarColor = color
            }
        }


        fun convertUTCtoLocal(createdAt: String?): String {
            var formatLong: SimpleDateFormat? = null
            var formatShort: SimpleDateFormat? = null
            var timeLong = ""
            try {
                timeLong = createdAt!!
                formatLong = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
                formatShort = SimpleDateFormat("hh:mm aa", Locale.getDefault())
                Log.e("out", "time final: " + formatShort.format(formatLong.parse(timeLong)))


            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return formatShort!!.format(formatLong!!.parse(timeLong))
        }

        fun animleft(activity: Activity?): Animation? {
            return AnimationUtils.loadAnimation(
                activity,
                R.anim.enter_from_leftslow
            )
        }

        fun animRight(activity: Activity?): Animation? {
            return AnimationUtils.loadAnimation(activity, R.anim.enter_from_rightslow)
        }

        fun animShake(activity: Activity?): Animation? {
            return AnimationUtils.loadAnimation(activity, R.anim.shake_animation)
        }

        fun animTopToCenter(activity: Activity?): Animation? {
            return AnimationUtils.loadAnimation(activity, R.anim.grow_from_top)
        }

        fun fadeIn(activity: Activity?): Animation? {
            return AnimationUtils.loadAnimation(activity, R.anim.fade_in_anim)
        }


        fun getFirebaseToken(activity: Activity?) {
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(
                        "FCM_TAG",
                        "Fetching FCM registration token failed",
                        task.exception
                    )
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token = task.result
                putString(
                    activity!!,
                    Constants.DEVICE_TOKEN,
                    token
                )
                Log.d("FCM_TAG", token.toString())
            })
        }


        fun DialogToLogin(activity: Activity) {
            val builder =
                AlertDialog.Builder(activity!!)
            val inflater = activity.layoutInflater
            val dialogLayout: View =
                inflater.inflate(R.layout.dialog_login_again, null)
            builder.setCancelable(false);
            var btn_okay = dialogLayout.findViewById<Button>(R.id.btn_okay)
            btn_okay.setOnClickListener {
                putBoolean(activity, Constants.HAVE_TOKEN, false)
                activity.startActivity(Intent(activity, LoginActivity::class.java))

            }
            builder.setView(dialogLayout)
            builder.show()

        }


        fun dialogSendingYourTribu(activity: Activity) {


            var dialogGet = customDialog(
                context = activity!!,
                dialogView = R.layout.dialog_we_sending_you_tribu
            )

            val btn1 = dialogGet.findViewById<View>(R.id.btn) as Button

            btn1.setOnClickListener {
                dialogGet.cancel()
                dialogGet.dismiss()
            }


        }


        fun dialogTimeRanOut(activity: Activity, title: String, message: String) {


            var dialogGet = customDialog(
                context = activity!!,
                dialogView = R.layout.dialog_time_ran_out
            )

            val btn1 = dialogGet.findViewById<View>(R.id.btn) as Button
            val tvTitle = dialogGet.findViewById<View>(R.id.tv_title) as TextView
            val tvMessage = dialogGet.findViewById<View>(R.id.tv_message) as TextView



            tvTitle.text = title.toString()
            tvMessage.text = message.toString()

            btn1.setOnClickListener {
                dialogGet.cancel()
                dialogGet.dismiss()
            }


        }


        fun dialogAnonymousUserSentYou(activity: Activity) {


            var dialogGet = customDialog(
                context = activity!!,
                dialogView = R.layout.dialog_anonymous_user_sent_you
            )

            val btn1 = dialogGet.findViewById<View>(R.id.btn) as Button

            btn1.setOnClickListener {
                dialogGet.cancel()
                dialogGet.dismiss()
            }


        }


        fun DialogThanksForSignup(activity: Activity) {


            var dialogGet = customDialog(
                context = activity!!,
                dialogView = R.layout.dialog_thankyou_for_signingup
            )

            val iv_close = dialogGet.findViewById<View>(R.id.iv_close) as ImageView

            iv_close.setOnClickListener {
                dialogGet.cancel()
                dialogGet.dismiss()


            }


        }


        fun DialogTribuRecieved(activity: Activity, requestId: String) {


            var dialogGet = customDialog(
                context = activity!!,
                dialogView = R.layout.tribu_received_layout
            )

            val tv_accept = dialogGet.findViewById<View>(R.id.tv_accept) as Button
            val tv_reject = dialogGet.findViewById<View>(R.id.tv_reject) as Button

            tv_accept.setOnClickListener {
                dialogGet.cancel()
                dialogGet.dismiss()

            }

            tv_reject.setOnClickListener {
                dialogGet.cancel()
                dialogGet.dismiss()

            }


        }


        lateinit var dialog: Dialog
        fun showLoader(context: Context) {
            dialog = Dialogs.getLoadingDialog(context)
            dialog.show()
        }

        fun hideLoader() {

            try {
                if (dialog.isShowing) {
                    dialog.cancel()
                    dialog.dismiss()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }


        fun customDialog(context: Context, dialogView: Int, heightVal: Float = 0.80f): Dialog {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(dialogView)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setGravity(Gravity.CENTER_VERTICAL)


            val width = (context.resources.displayMetrics.widthPixels * 0.90).toInt()
            val height = (context.resources.displayMetrics.heightPixels * heightVal).toInt()

            dialog.getWindow()?.setLayout(width, height)
            dialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

            val iv_close = dialog.findViewById<ImageView>(R.id.iv_close) as ImageView?

            iv_close?.setOnClickListener {
                dialog.cancel()
            }

            dialog.show()
            return dialog
        }


        fun fireActivityIntent(
            sourceActivity: Activity,
            mIntent: Intent,
            isFinish: Boolean,
            isForward: Boolean
        ) {
            sourceActivity.startActivity(mIntent)
            if (isFinish) {
                sourceActivity.finish()
            }
            if (isForward) {
                sourceActivity.overridePendingTransition(
                    R.anim.slide_in_left,
                    R.anim.slide_out_left
                )
            } else {
                sourceActivity.overridePendingTransition(
                    R.anim.slide_in_right,
                    R.anim.slide_out_right
                )
            }
        }


    }

}

