package com.tribU.app

import android.app.Application
import com.google.android.gms.ads.MobileAds
import com.tribU.R
import com.tribU.di.DaggerProvider
import com.tribU.di.component.AppComponent

class MyApplication : Application() {
    private lateinit var mAppComponent: AppComponent

    init {
        instance_ = this
    }

    companion object {
        lateinit var instance_: MyApplication

        fun getInstance() = instance_
    }

    override fun onCreate() {
        super.onCreate()
        //MobileAds.initialize(this, getString(R.string.default_web_client_id))
        DaggerProvider.initComponent(this)
        DaggerProvider.getAppComponent()?.inject(this)

    }

}