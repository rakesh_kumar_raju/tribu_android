package com.tribU

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Handler
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.bishaan.onboarding.view.OnboardingActivity
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.SplashActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.model.AllData
import com.tribU.home.view.HomeActivity
import com.tribU.home.viewmodel.SplashVM
import com.tribU.login.callbacks.SplashViewCallbacks
import com.tribU.login.view.LoginActivity
import com.tribU.network.Status
import com.tribU.on_boarding.view.LetsGetStartedActivity
import com.tribU.otp.view.OTPActivity
import com.tribU.signup.view.GettingStarted1Activity
import com.tribU.signup.view.GettingStarted2Activity
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import javax.inject.Inject

class SplashNewActivity :
    BaseDataBindingActivity<SplashActivityDataBinding>(R.layout.activity_splash_new),
    SplashViewCallbacks {


    lateinit var device_token: String
    private val TAG = SplashNewActivity::class.java.simpleName
    var firebaseId: String? = null


    companion object {

        @SuppressLint("StaticFieldLeak")
        var mInstance: SplashNewActivity? = null

        var appData: AllData = AllData()

    }


    fun getInstance(): SplashNewActivity? {
        return mInstance
    }


    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var vm: SplashVM


    override fun onResume() {
        super.onResume()
        if (CommonUtilities.isConnectingToInternet(this) == true) {


            vm.getdataApi()

        } else {
            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
        }

    }

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this
        mInstance = this
        CommonUtilities.getFirebaseToken(this)


    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        //SignUpViewModel = ViewModelProvider(this, viewModelFactory).get(LanguageVM::class.java)
        vm = ViewModelProvider(this, viewModelFactory).get(SplashVM::class.java)




        setupObserver()


        //  splashHandler()
    }

    private fun setupObserver() {


        vm.responseAllData.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    // CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    //   CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        appData = result.data!!


                        Log.d("TAG", result.message.toString())
                        splashHandler()


                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d("TAG", it.data!!.message.toString())

                    }


                }
            }
        })


    }


    fun splashHandler() {
        Handler().postDelayed({
            var is_old_user = CommonUtilities.getBoolean(this, Constants.IS_OLD_USER)
            if (!is_old_user) {

                CommonUtilities.fireActivityIntent(
                    this,
                    Intent(this, OnboardingActivity::class.java)
                        .putExtra(Constants.FROM_WHICH_SCREEN, "splash"),
                    isFinish = true,
                    isForward = true
                )


            } else {

                if (!CommonUtilities.getBoolean(this, Constants.IS_LOGIN)) {

                    if (!CommonUtilities.getString(this, Constants.TOKEN).isNullOrEmpty()) {

                        if (CommonUtilities.getBoolean(this, Constants.IS_KYC_DONE)) {


                            if (CommonUtilities.getBoolean(this, Constants.IS_ACCOUNT_VERIFIED)
                            ) {

                                // if IS_ACCOUNT_VERIFIED is Done . IS_LOGIN will always be true

                            } else {
                                // account created + KYC done but not verified

                                CommonUtilities.fireActivityIntent(
                                    this,
                                    Intent(this, GettingStarted2Activity::class.java)
                                        .putExtra(Constants.FROM_WHICH_SCREEN, Constants.SPLASH),

                                    isFinish = true,
                                    isForward = true
                                )

                            }


                        } else {
                            // account created but not verified no KYC


                            CommonUtilities.fireActivityIntent(
                                this,
                                Intent(this, OTPActivity::class.java)
                                    .putExtra(Constants.FROM_WHICH_SCREEN, Constants.SPLASH),

                                isFinish = true,
                                isForward = true
                            )



                        }


                    } else {

                        // account not created


                        CommonUtilities.fireActivityIntent(
                            this,
                            Intent(this, LetsGetStartedActivity::class.java),
                            //   Intent(this, LoginActivity::class.java),
                            isFinish = true,
                            isForward = true
                        )

                    }


                } else {


                    CommonUtilities.fireActivityIntent(
                        this,
                        Intent(this, HomeActivity::class.java),
                        isFinish = true,
                        isForward = true
                    )


                }
            }
        }, 1000)


    }


}
















