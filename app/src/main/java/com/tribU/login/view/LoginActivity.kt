package com.tribU.login.view

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.tribU.R
import com.tribU.SplashNewActivity
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.LoginActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.login.callbacks.LoginViewCallbacks
import com.tribU.on_boarding.view.LetsGetStartedActivity
import com.tribU.otp.view.OTPActivity
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants

class LoginActivity : BaseDataBindingActivity<LoginActivityDataBinding>(R.layout.activity_login),
    LoginViewCallbacks {


    lateinit var device_token: String
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    lateinit var auth: FirebaseAuth
    private var storedVerificationId: String? = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private var verificationId: String? = null
    private val TAG = LoginActivity::class.java.simpleName
    var firebaseId: String? = null
    var from_which_screen = ""


    override fun onDataBindingCreated() {
        auth = FirebaseAuth.getInstance()
        binding.callback = this
        binding.lifecycleOwner = this
        CommonUtilities.getFirebaseToken(this)



        from_which_screen = intent.getStringExtra(Constants.FROM_WHICH_SCREEN).toString()

        if (from_which_screen == Constants.LOGIN) {
            binding.tvHaveAccount.visibility = View.GONE
            binding.tvNoAccount.visibility = View.VISIBLE

            binding.tvWelcome.text = "Let's Log you in . . ."
            binding.tvCreate.text = "Welcome back, you've been missed !"

        } else {
            binding.tvHaveAccount.visibility = View.VISIBLE
            binding.tvNoAccount.visibility = View.GONE
        }





  /*      if (SplashNewActivity.appData != null) {
            binding.tvWelcome.text = SplashNewActivity.appData.loginOrRegisterScreen!!.heading
            binding.tvCreate.text = SplashNewActivity.appData.loginOrRegisterScreen!!.paragraph1

        }*/


        /* Glide.with(this)
             .load(SplashNewActivity.appData.letStartScreen!!.imgUrl)
             .into(binding.ivBg)*/


    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        //SignUpViewModel = ViewModelProvider(this, viewModelFactory).get(LanguageVM::class.java)
    }

    override fun enterPhoneCodeClicked() {

    }

    override fun enterPhoneNoClicked() {

    }

    override fun loginClicked() {
        if (binding.edPhoneNo.text.length < 1) {
            CommonUtilities.showToast(this, R.string.please_enter_your_mobile_no)
        } else if (binding.edPhoneNo.text.length < 10) {
            CommonUtilities.showToast(this, R.string.please_enter_valid_mobile_no)
        } else if (!binding.checkbox.isChecked) {
            CommonUtilities.showToastString(this, "Please Agree to the Terms and Conditions")
        } else {
            Log.d("phone_code", binding.tvPhoneCode.textView_selectedCountry.text.toString())
            if (CommonUtilities.isConnectingToInternet(this)!!) {
                /*  binding.loader.startAnimation();
                  binding.loader.setIsVisible(true);*/

                CommonUtilities.showLoader(this)


                if (from_which_screen == Constants.LOGIN) {
                    checkUserExistForLogin(binding.tvPhoneCode.selectedCountryCodeWithPlus.toString() + binding.edPhoneNo.text.toString())
                } else {
                    checkUserExistForSignup(binding.tvPhoneCode.selectedCountryCodeWithPlus.toString() + binding.edPhoneNo.text.toString())
                }


                binding.btsend.isEnabled = false
            } else {
                CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
            }

        }
    }

    override fun backClicked() {
        onBackPressed()
    }


    override fun onBackPressed() {
        // super.onBackPressed()

        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, LetsGetStartedActivity::class.java),
            isFinish = true,
            isForward = false
        )

    }

    override fun signupClicked() {


        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, LoginActivity::class.java)
                .putExtra(Constants.FROM_WHICH_SCREEN, Constants.SIGNUP),
            isFinish = true,
            isForward = true
        )

    }

    override fun signinClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, LoginActivity::class.java)
                .putExtra(Constants.FROM_WHICH_SCREEN, Constants.LOGIN),

            isFinish = true,
            isForward = true
        )
    }

    override fun terms1Clicked() {

        val url = "https://www.sanabriatech.com/legal"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)


    }


    override fun terms2Clicked() {

        val url = "https://www.dwolla.com/legal/tos/"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)


    }


    override fun privacy1Clicked() {

        val url = "https://app.termly.io/document/privacy-policy/2214c02c-7199-425a-a6cc-3c73ce7714fe"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)

    }

    override fun privacy2Clicked() {

        val url ="https://www.dwolla.com/legal/privacy/"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)


    }



    fun checkUserExistForLogin(mobile_with_code: String) {

        Log.d("TAG", "phone no : " + mobile_with_code)


        val rootRef = FirebaseFirestore.getInstance()
        rootRef.collection("users")
            .whereEqualTo(
                "phone",
                binding.tvPhoneCode.textView_selectedCountry.text.toString() + binding.edPhoneNo.text.toString()
            )
            .get()
            .addOnSuccessListener { documents ->


                Log.d("TAG", "" + documents.size())




                for (documentSnapshot in documents) {
                    val phone = documentSnapshot.getString("phone")

                    Log.e("TAG", phone + " = " + mobile_with_code)

                }


                for (document in documents) {
                    Log.d(TAG, document.id)
                    firebaseId = document.id
                }


                if (firebaseId != null) {


                    CommonUtilities.hideLoader()


                    /*  binding.loader.stopAnimation();
                      binding.loader.setIsVisible(false);*/
                    binding.btsend.isEnabled = true
                    CommonUtilities.fireActivityIntent(
                        this,
                        Intent(this, OTPActivity::class.java)
                            .putExtra(Constants.PHONE_NO, binding.edPhoneNo.text.toString())
                            .putExtra(Constants.FROM_WHICH_SCREEN, Constants.LOGIN)
                            .putExtra(Constants.FIREBASE_ID, firebaseId)
                            .putExtra(
                                Constants.PHONE_CODE,
                                binding.tvPhoneCode.textView_selectedCountry.text.toString()
                            ),
                        isFinish = false,
                        isForward = true
                    )

                } else {
                    /*  binding.loader.stopAnimation();
                      binding.loader.setIsVisible(false);*/

                    CommonUtilities.hideLoader()
                    CommonUtilities.showToast(this, R.string.user_dont_exist)
                    binding.btsend.isEnabled = true

                }


            }
            .addOnFailureListener { exception ->
                /*    binding.loader.stopAnimation();
                    binding.loader.setIsVisible(false);*/

                CommonUtilities.hideLoader()
                CommonUtilities.showToast(this, R.string.user_dont_exist)

                Log.w(TAG, "Error getting documents: ", exception)
                binding.btsend.isEnabled = true
            }


/*
        val usersRef: CollectionReference? = FirebaseFirestore.getInstance()?.collection("users")
        val query: Query = usersRef?.whereEqualTo("phone",
            mobile_with_code
        )!!
        query.get().addOnCompleteListener { task ->



            if (task.isSuccessful) {

                Log.d("TAG", ""+task.result?.size())


                for (documentSnapshot in task.result!!) {
                    val phone = documentSnapshot.getString("phone")
                    if (phone == mobile_with_code) {
                        Log.d(TAG, "User Exists")
                          Toast.makeText(this, "User Exists", Toast.LENGTH_SHORT).show()

*//*
                        binding.loader.stopAnimation();
                        binding.loader.setIsVisible(false);
                        binding.btsend.isEnabled=true
                        CommonUtilities.fireActivityIntent(
                            this,
                            Intent(this, OTPActivity::class.java)
                                .putExtra(Constants.PHONE_NO, binding.edPhoneNo.text.toString())
                                .putExtra(Constants.FROM_WHICH_SCREEN, Constants.LOGIN)
                                .putExtra(Constants.FIREBASE_ID, firebaseId)
                                .putExtra(
                                    Constants.PHONE_CODE,
                                    binding.tvPhoneCode.textView_selectedCountry.text.toString()
                                ),
                            isFinish = false,
                            isForward = true
                        )*//*

                    }
                }


            }
            if (task.result?.size() == 0) {

                //   Toast.makeText(this@VerifyActivity, "User not Exists", Toast.LENGTH_SHORT).show()

                Log.d("TAG", "User not Exists")


                binding.loader.stopAnimation();
                binding.loader.setIsVisible(false);
             //   CommonUtilities.showToast(this, R.string.user_dont_exist)
                binding.btsend.isEnabled=true

                Toast.makeText(this, "Please Signup First fail0", Toast.LENGTH_SHORT).show()



                //You can store new user information here
            }



        }.addOnFailureListener {


            binding.loader.stopAnimation();
            binding.loader.setIsVisible(false);
            Log.d("TAG", "Error getting documents: ", it)
            binding.btsend.isEnabled=true


            Toast.makeText(this, "Please Signup First fail", Toast.LENGTH_SHORT).show()

            Log.d("TAG", "User not Exists")*/

        //   }


    }


    fun checkUserExistForSignup(mobile_with_code: String) {
        val rootRef = FirebaseFirestore.getInstance()
        rootRef.collection("users")
            .whereEqualTo(
                "phone",
                binding.tvPhoneCode.textView_selectedCountry.text.toString() + binding.edPhoneNo.text.toString()
            )
            .get()
            .addOnSuccessListener { documents ->


                CommonUtilities.hideLoader()

                Log.d("TAG", "" + documents.size())




                for (documentSnapshot in documents) {
                    val phone = documentSnapshot.getString("phone")

                    Log.e("TAG", phone + " = " + mobile_with_code)

                }


                for (document in documents) {
                    Log.d(TAG, document.id)
                    firebaseId = document.id
                }


                if (firebaseId != null) {


                    binding.btsend.isEnabled = true

                    CommonUtilities.showToast(this, R.string.user_already_exist)


                } else {

                    //   CommonUtilities.showToast(this, R.string.user_dont_exist)
                    binding.btsend.isEnabled = true


                    CommonUtilities.fireActivityIntent(
                        this,
                        Intent(this, OTPActivity::class.java)
                            .putExtra(Constants.PHONE_NO, binding.edPhoneNo.text.toString())
                            .putExtra(Constants.FROM_WHICH_SCREEN, Constants.SIGNUP)
                            .putExtra(
                                Constants.PHONE_CODE,
                                binding.tvPhoneCode.textView_selectedCountry.text.toString()
                            ),
                        isFinish = false,
                        isForward = true
                    )

                }


            }
            .addOnFailureListener { exception ->
                /*binding.loader.stopAnimation();
                binding.load er.setIsVisible(false);*/

                CommonUtilities.hideLoader()
                Log.w(TAG, "Error getting documents: ", exception)
                binding.btsend.isEnabled = true
            }

    }


}
















