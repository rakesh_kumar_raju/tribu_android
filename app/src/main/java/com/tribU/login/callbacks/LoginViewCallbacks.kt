package com.tribU.login.callbacks

interface LoginViewCallbacks {
    fun enterPhoneCodeClicked()
    fun enterPhoneNoClicked()
    fun loginClicked()
    fun backClicked()
    fun signupClicked()
    fun signinClicked()
    fun terms1Clicked()
    fun terms2Clicked()
    fun privacy1Clicked()
    fun privacy2Clicked()
}