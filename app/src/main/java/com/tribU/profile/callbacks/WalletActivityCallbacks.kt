package com.tribU.profile.callbacks

interface WalletActivityCallbacks {
    fun backClicked()
    fun addMoneyClicked()
    fun transactionHistoryClicked()
    fun bankTransferClicked()

}