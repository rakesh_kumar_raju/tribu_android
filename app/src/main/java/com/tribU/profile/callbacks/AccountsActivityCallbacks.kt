package com.tribU.profile.callbacks

interface AccountsActivityCallbacks {
    fun backClicked()
    fun sendClicked()
    fun addClicked()
    fun deleteClicked()

}