package com.tribU.profile.callbacks

import android.net.Uri
import java.io.File

interface ProfileActivityCallbacks {
    fun backClicked()
    fun walletClicked()
    fun accountsClicked()
    fun supportClicked()
    fun tutorialClicked()
    fun shareClicked()
    fun signoutClicked()
    fun changeClicked()
    fun setImage(image_uri: Uri, file: File)

}