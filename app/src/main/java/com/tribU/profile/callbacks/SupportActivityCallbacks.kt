package com.tribU.profile.callbacks

interface SupportActivityCallbacks {
    fun backClicked()
    fun emailClicked()

}