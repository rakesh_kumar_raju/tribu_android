package com.tribU.profile.callbacks

import android.net.Uri
import java.io.File

interface ProfileFragViewCallbacks {
    fun contactUsClick()
    fun backClick()
    fun imageClick()
    fun logoutClick()
    fun setImgae(image_uri: Uri,file: File)

}