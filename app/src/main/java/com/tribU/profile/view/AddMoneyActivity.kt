package com.tribU.profile.view

import android.annotation.SuppressLint
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.lottie.LottieAnimationView
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.google.gson.JsonObject
import com.tribU.R
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.AddMoneyActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.network.Status
import com.tribU.profile.adapter.SelectBankAccountAdapter
import com.tribU.profile.callbacks.AddMoneyActivityCallbacks
import com.tribU.profile.model.FundingSourcesDataItem
import com.tribU.profile.viewmodel.ProfileVM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import com.tribU.util.MyBottomSheetDialog
import java.text.SimpleDateFormat
import javax.inject.Inject


class AddMoneyActivity :
    BaseDataBindingActivity<AddMoneyActivityDataBinding>(R.layout.activity_add_money),
    AddMoneyActivityCallbacks {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var vm: ProfileVM

    var fundingSourcesList: ArrayList<FundingSourcesDataItem> = ArrayList()


    private val TAG = AddMoneyActivity::class.java.simpleName

    var selectedBankId = ""
    var walletId = ""

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this


        if (intent.getStringExtra("balance") != null) {
            var balance = intent.getStringExtra("balance").toString()
            binding.tvBalance.text = balance
        }




        if (intent.getStringExtra(Constants.CLICK_TYPE).toString() == Constants.TRANSFER) {
            binding.tvTitle.text = "Bank Transfer"
            binding.btSend.text = "Continue"
            binding.tvHowMuch.text = "How much would you like to transfer?"
            binding.tvSelectBank.text = "Select a Bank account to Transfer"
        }




        initRecycler()


    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        vm = ViewModelProvider(this, viewModelFactory).get(ProfileVM::class.java)

        setupObserver()

        if (CommonUtilities.isConnectingToInternet(this) == true) {
            CommonUtilities.showLoader(this)


            fundingSourcesList.clear()

            vm.getFundingSources(
                CommonUtilities.getString(this, Constants.TOKEN).toString()
            )


        } else {
            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
        }


    }


    fun checkifAnyAccountSelected() {
        for (i in fundingSourcesList) {
            if (i.isSelected == true) {
                selectedBankId = i.id.toString()
            }
        }

        if (binding.edPrice.text.isEmpty()) {
            CommonUtilities.showToastString(this, "Please enter amount")

        } else if (walletId.isEmpty()) {
            // CommonUtilities.showToastString(this,"wallet id null")

        } else if (selectedBankId.isEmpty()) {

            CommonUtilities.showToastString(this, "Please select account")

        } else {

            if (intent.getStringExtra(Constants.CLICK_TYPE).toString() == Constants.TRANSFER) {


                if (CommonUtilities.isConnectingToInternet(this) == true) {
                    CommonUtilities.showLoader(this)


                    // transfer money from wallet to bank
                    var obj = JsonObject()
                    obj.addProperty("to", selectedBankId)
                    obj.addProperty("from", walletId)
                    obj.addProperty("amount", binding.edPrice.text.toString().toInt())




                    vm.sddOrTransferMoneyApi(
                        CommonUtilities.getString(this, Constants.TOKEN).toString(),
                        obj
                    )


                } else {
                    CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
                }


            } else {

                // transfer money from bank to wallet
                var obj = JsonObject()
                obj.addProperty("from", selectedBankId)
                obj.addProperty("to", walletId)
                obj.addProperty("amount", binding.edPrice.text.toString().toInt())


                vm.sddOrTransferMoneyApi(
                    CommonUtilities.getString(this, Constants.TOKEN).toString(),
                    obj
                )


                //  showBottomSheet(2, "Money successfully\ntransfered to bank account", "")

            }

        }
    }


    fun initRecycler(fundingSourcesList: ArrayList<FundingSourcesDataItem>) {

        var accountsAdapter =
            SelectBankAccountAdapter(this!!, fundingSourcesList)
        binding?.rvHistory?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = accountsAdapter
        }


    }

    private fun setupObserver() {

        vm.responseFundingSources.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        if (result?.data != null) {


                            //   result?.data?.let { it1 -> fundingSourcesList.addAll(it1) }


                            for (i in result?.data) {
                                if (i.type.equals("bank")) {
                                    fundingSourcesList.add(i)
                                } else {
                                    walletId = i.id.toString()
                                }
                            }




                            if (fundingSourcesList.isNotEmpty()) {

                                binding.rvHistory.visibility = View.VISIBLE
                                initRecycler(fundingSourcesList)

                            } else {
                                binding.rvHistory.visibility = View.GONE
                            }


                        } else {
                            binding.rvHistory.visibility = View.GONE

                        }


                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })



        vm.responseAddOrTransferMoney.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    CommonUtilities.showLoader(this)

                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        if (result?.data != null) {

                            var data = result?.data




                            if (intent.getStringExtra(Constants.CLICK_TYPE).toString() == Constants.TRANSFER) {


                                showBottomSheet(2,
                                    data.finalResponse!!.popUpTextAccount!!.title.toString(),
                                    data.finalResponse!!.popUpTextAccount!!.description.toString(),
                                    data.finalResponse!!.id.toString(),
                                    data.finalResponse!!.createdAt.toString(),
                                    data.finalResponse!!.amount.toString()
                                    )



                            }else
                            {


                                showBottomSheet(1,
                                    data.finalResponse!!.popUpTextWallet!!.title.toString(),
                                    data.finalResponse!!.popUpTextWallet!!.description.toString(),
                                    data.finalResponse!!.id.toString(),
                                    data.finalResponse!!.createdAt.toString(),
                                    data.finalResponse!!.amount.toString()
                                )

                            }



                            }


                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })

    }


    fun initRecycler() {

        var selectBankAccountAdapter =
            SelectBankAccountAdapter(this!!, fundingSourcesList)
        binding?.rvHistory?.apply {
            layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = selectBankAccountAdapter
        }


    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun backClicked() {
        onBackPressed()
    }

    override fun continueClicked() {

        checkifAnyAccountSelected()

    }


    private fun showBottomSheet(type: Int, title: String, message: String,transactionId: String,date: String,amount: String) {


        var bottomSheetDialog = MyBottomSheetDialog(this, false)


        @SuppressLint("InflateParams") val view: View =
            layoutInflater.inflate(R.layout.success_bottom_sheet, null)




        if (type == 2) {
            (view.findViewById(R.id.lottie_wallet_recharge) as LottieAnimationView).visibility =
                View.VISIBLE
        } else {
            (view.findViewById(R.id.lottie_wallet_recharge) as LottieAnimationView).visibility =
                View.VISIBLE
        }



        (view.findViewById(R.id.tv_title) as TextView).text = title
         (view.findViewById(R.id.tv_message) as TextView).text = message
        (view.findViewById(R.id.tv_balance) as TextView).text = amount+" USD"
        (view.findViewById(R.id.tv_transaction_id) as TextView).text = transactionId


        try {

            var date1 = date.toString()
            try {


                var date1 = date.toString()

                var format1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                val newDate1 = format1.parse(date1)
                format1 = SimpleDateFormat("dd MMM, hh:mm aaa")
                val date2 = format1.format(newDate1)
                (view.findViewById(R.id.tv_date) as TextView).text = date2

            } catch (e: Exception) {
                e.printStackTrace()
                (view.findViewById(R.id.tv_date) as TextView).text = date1
            }


        } catch (e: Exception) {
            e.printStackTrace()
            (view.findViewById(R.id.tv_date) as TextView).visibility = View.GONE

        }


        (view.findViewById(R.id.bt_continue) as View).setOnClickListener {

            bottomSheetDialog!!.dismiss()


            Handler().postDelayed({
                finish()
            }, 200)

        }



        bottomSheetDialog!!.setContentView(view)


/*

        val behaviour: BottomSheetBehavior<View> = BottomSheetBehavior.from<View>(view)
        behaviour.setBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {

                    finish()

                }
            }

            override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {}
        })
*/


        /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             Objects.requireNonNull(bottomSheetDialog!!.window)!!
                 .addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
         }*/

        bottomSheetDialog!!.show()
    }


}
















