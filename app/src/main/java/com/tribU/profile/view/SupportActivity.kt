package com.tribU.profile.view

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.bishaan.onboarding.view.OnboardingActivity
import com.tribU.R
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.ProfileActivityDataBinding
import com.tribU.databinding.SupportActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.view.HomeActivity
import com.tribU.network.Status
import com.tribU.on_boarding.view.LetsGetStartedActivity
import com.tribU.otp.viewmodel.OtpActivityVM
import com.tribU.profile.callbacks.ProfileActivityCallbacks
import com.tribU.profile.callbacks.SupportActivityCallbacks
import com.tribU.profile.viewmodel.ProfileVM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import javax.inject.Inject
import android.widget.Toast




class SupportActivity :
    BaseDataBindingActivity<SupportActivityDataBinding>(R.layout.activity_support),
    SupportActivityCallbacks {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var vm: ProfileVM


    private val TAG = SupportActivity::class.java.simpleName

    override fun onDataBindingCreated() {

        binding.callback = this
        binding.lifecycleOwner = this

    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        vm = ViewModelProvider(this, viewModelFactory).get(ProfileVM::class.java)

        setupObserver()
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun backClicked() {
        onBackPressed()
    }


    override fun emailClicked() {
 /*       val emailIntent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("sanabriatech@gmail.com")
        }
        startActivity(Intent.createChooser(emailIntent, "Send Support Email"))*/
        val intent =
            Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "sanabriatech@gmail.com", null))
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
        intent.putExtra(Intent.EXTRA_TEXT, "Message")

        try {
            startActivity(Intent.createChooser(intent, "Send email"))
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(
                this,
                "There are no email clients installed.",
                Toast.LENGTH_SHORT
            ).show()
        }

    }


    private fun setupObserver() {

        /*
            vm.responseLogout.observe(this, Observer {
                when (it?.status) {
                    Status.LOADING -> {
                        CommonUtilities.showLoader(this)
                    }
                    Status.ERROR -> {
                        CommonUtilities.hideLoader()
                    }
                    Status.SUCCESS -> {

                        CommonUtilities.hideLoader()

                        val result = it.data
                        if (result?.statusCode == 200) {

                            CommonUtilities.clearPrefrences(this)
                            CommonUtilities.putBoolean(this, Constants.IS_OLD_USER, true)

                            CommonUtilities.fireActivityIntent(
                                this,
                                Intent(this, LetsGetStartedActivity::class.java),
                                isFinish = true,
                                isForward = true
                            )

                        } else {
                            CommonUtilities.showToastString(this, it.data?.message.toString())
                            Log.d(TAG, it.data!!.message.toString())

                        }
                    }
                }
            })
    */

    }



}
















