package com.tribU.profile.view

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.bishaan.onboarding.view.OnboardingActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.tribU.R
import com.tribU.SplashNewActivity
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.ProfileActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.view.HomeActivity
import com.tribU.main_cantainer.view.BottomNavigationActivity
import com.tribU.network.Status
import com.tribU.on_boarding.view.LetsGetStartedActivity
import com.tribU.otp.viewmodel.OtpActivityVM
import com.tribU.profile.callbacks.ProfileActivityCallbacks
import com.tribU.profile.viewmodel.ProfileVM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import id.zelory.compressor.Compressor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

class ProfileActivity :
    BaseDataBindingActivity<ProfileActivityDataBinding>(R.layout.activity_profile),
    ProfileActivityCallbacks {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var vm: ProfileVM


    private val TAG = ProfileActivity::class.java.simpleName

    override fun onResume() {
        super.onResume()


        if (!CommonUtilities.getString(this, Constants.PROFILE_PIC).isNullOrEmpty())
        {
            Glide.with(this)
                .load(CommonUtilities.getString(this, Constants.PROFILE_PIC))
                .placeholder(R.drawable.ic_user_placeholder2)
                .into(binding.ivProfile)
        }


        Log.e("appData", "appData: "+SplashNewActivity.appData )



    }



    private fun selectImage() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(2042)        //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                1080,
                1080
            )//Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this





        binding.tvFirstname.text = CommonUtilities.getString(this, Constants.FIRST_NAME)!!.split(' ').joinToString(" ") { it.capitalize() }
        binding.tvLastname.text = CommonUtilities.getString(this, Constants.LAST_NAME)!!.split(' ').joinToString(" ") { it.capitalize() }


        binding.tvPhone.text = CommonUtilities.getString(this, Constants.PHONE_WITH_CODE)
    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        vm = ViewModelProvider(this, viewModelFactory).get(ProfileVM::class.java)

        setupObserver()
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun backClicked() {
        onBackPressed()
    }

    override fun walletClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, WalletActivity::class.java),
            isFinish = false,
            isForward = true
        )
    }

    override fun accountsClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, AccountsActivity::class.java)
                .putExtra(Constants.FROM_WHICH_SCREEN, Constants.PROFILE),
            isFinish = false,
            isForward = true
        )
    }

    override fun supportClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, SupportActivity::class.java),
            isFinish = false,
            isForward = true
        )
    }

    override fun tutorialClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, OnboardingActivity::class.java)
                .putExtra(Constants.FROM_WHICH_SCREEN, "tutorial"),
            isFinish = false,
            isForward = true
        )
    }

    override fun shareClicked() {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_TEXT, "Hey Check out this Great app:")
        intent.type = "text/plain"
        startActivity(Intent.createChooser(intent, "Share To:"))
    }

    override fun signoutClicked() {
        if (CommonUtilities.isConnectingToInternet(this) == true) {
            vm.logoutApi(CommonUtilities.getString(this, Constants.TOKEN).toString())

        } else {
            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
        }

    }

    override fun changeClicked() {
        selectImage()
    }


    private fun setupObserver() {

        vm.responseLogout.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                    CommonUtilities.showLoader(this)
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {

                        CommonUtilities.clearPrefrences(this)
                        CommonUtilities.putBoolean(this, Constants.IS_OLD_USER, true)

                        CommonUtilities.fireActivityIntent(
                            this,
                            Intent(this, LetsGetStartedActivity::class.java),
                            isFinish = true,
                            isForward = true
                        )

                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })



        vm.uploadImagetResult.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                 CommonUtilities.showLoader(this)
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()

                    CommonUtilities.showToastString(this, it.message.toString())
                }
                Status.SUCCESS -> {
                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {
                        Log.d("result_true", "true")


                        if(Constants.IS_PRODUCTION_ENVIORNMENT)
                        {
                            CommonUtilities.putString(
                                this,
                                Constants.PROFILE_PIC,
                                Constants.URL_FOR_IMAGE + "" + result.data!!.path.toString()
                            )
                        }else {

                            CommonUtilities.putString(
                                this,
                                Constants.PROFILE_PIC,
                                Constants.URL_FOR_IMAGE_STAGING + "" + result.data!!.path.toString()
                            )

                        }
                        Glide.with(this)
                            .load(CommonUtilities.getString(this, Constants.PROFILE_PIC))
                            .into(binding.ivProfile)


                        updateProfilePicFunc(result.data!!.path.toString())



                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(this)

                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d("result_true", "false")
                    }
                }
            }

        })

        vm.updateImagetResult.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
             //    CommonUtilities.showLoader(this)
                }
                Status.ERROR -> {
                   // CommonUtilities.hideLoader()

                    CommonUtilities.showToastString(this, it.message.toString())
                }
                Status.SUCCESS -> {
                 //   CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {
                        Log.d("result_true", "true")


                    } else if (result?.statusCode == 401) {
                        CommonUtilities.DialogToLogin(this)

                    } else {
                    //    CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d("result_true", "false")
                    }
                }
            }

        })

    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


            if (resultCode == Activity.RESULT_OK) {
                val uri: Uri = data?.data!!
                var file = ImagePicker.getFile(data)


                val compressedImageFile = Compressor.getDefault(this).compressToFile(file);
                setImage(uri, file!!)
            }


    }


    override fun setImage(uri: Uri, file: File) {
        val requestBody =
            file.let { RequestBody.create("image/*".toMediaTypeOrNull(), it) }
        var multipart = requestBody?.let {
            MultipartBody.Part.createFormData(
                "files",
                file.getName(),
                it
            )
        }
        vm.uploadProfilePicApi(multipart)


    }


    fun updateProfilePicFunc(path: String) {
        vm.updateProfilePicApi(
            CommonUtilities.getString(
                this,
                Constants.TOKEN
            )!!, path
        )
    }


}
















