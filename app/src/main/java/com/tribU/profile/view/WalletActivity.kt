package com.tribU.profile.view

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.bumptech.glide.Glide
import com.tribU.R
import com.tribU.SplashNewActivity
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.WalletActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.view.TransactionHistoryActivity
import com.tribU.home.viewmodel.TransactionsVM
import com.tribU.network.Status
import com.tribU.profile.adapter.TransactionHistoryAdapter
import com.tribU.profile.callbacks.WalletActivityCallbacks
import com.tribU.profile.model.TransactionsItem
import com.tribU.profile.viewmodel.WalletVM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import javax.inject.Inject

class WalletActivity :
    BaseDataBindingActivity<WalletActivityDataBinding>(R.layout.activity_wallet),
    WalletActivityCallbacks {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var vm: WalletVM


    var myAdapter: TransactionHistoryAdapter? = null

    var transactionsList: ArrayList<TransactionsItem> = ArrayList()

    var limit = 10
    var skip = 0
    var total = 0
    var balance = ""


    private val TAG = WalletActivity::class.java.simpleName

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this



        if (!SplashNewActivity.appData.walletScreen!!.heading.isNullOrEmpty()) {
            binding.tvCreate.text = SplashNewActivity.appData.walletScreen!!.heading
            binding.tvAvailableBalTitle.text = SplashNewActivity.appData.walletScreen!!.heading

        }



    /*    Glide.with(this)
            .load(SplashNewActivity.appData.walletScreen!!.imgUrl)
            .into(binding.ivBg)*/

    }


    override fun onResume() {
        super.onResume()

        if (CommonUtilities.isConnectingToInternet(this) == true) {
            CommonUtilities.showLoader(this)


            transactionsList.clear()

            vm.transactionListApi(
                CommonUtilities.getString(this, Constants.TOKEN).toString(),
                skip,
                limit
            )

            setupObserver()


        } else {
            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
        }
    }

    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)


        vm = ViewModelProvider(this, viewModelFactory).get(WalletVM::class.java)





    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun backClicked() {
        onBackPressed()
    }

    override fun addMoneyClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, AddMoneyActivity::class.java)
                .putExtra(Constants.CLICK_TYPE, Constants.ADD)
                .putExtra("balance", balance),
            isFinish = false,
            isForward = true
        )
    }

    override fun transactionHistoryClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, TransactionHistoryActivity::class.java),
            isFinish = false,
            isForward = true
        )
    }

    override fun bankTransferClicked() {
        CommonUtilities.fireActivityIntent(
            this,
            Intent(this, AddMoneyActivity::class.java)
                .putExtra(Constants.CLICK_TYPE, Constants.TRANSFER)
                .putExtra("balance", balance),


            isFinish = false,
            isForward = true
        )
    }


    private fun setupObserver() {

        vm.responseWalletBalance.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {

                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        if (result?.data?.balanceDetails != null) {

                            var data = result?.data?.balanceDetails

                            balance = data?.balance?.value + " " + data?.balance?.currency
                            binding.tvBalance.text = balance

                        }


                        if (result?.data?.transactions != null) {

                            total = result?.data?.totalNumberofTransactions!!

                            transactionsList.clear()
                            result?.data?.transactions?.let { it1 -> transactionsList.addAll(it1) }


                            if (transactionsList.isNotEmpty()) {

                                binding.nestedScroll.visibility = View.VISIBLE
                                binding.noData.visibility = View.GONE

                                initRecycler(transactionsList)
                               // initPagination()

                            } else {
                                binding.nestedScroll.visibility = View.GONE
                                binding.noData.visibility = View.VISIBLE

                            }


                        } else {
                            binding.nestedScroll.visibility = View.GONE
                            binding.noData.visibility = View.VISIBLE

                        }


                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })

    }


    fun initRecycler(list: ArrayList<TransactionsItem>) {


        myAdapter =
            TransactionHistoryAdapter(this, list)
        binding?.rvHistory?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = myAdapter
        }


    }


    private fun initPagination() {

        binding!!.nestedScroll.isNestedScrollingEnabled = false
        binding!!.nestedScroll.setSmoothScrollingEnabled(true)
        binding!!.nestedScroll.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (v.getChildAt(v.childCount - 1) != null) {
                if (scrollY >= v.getChildAt(v.childCount - 1)
                        .measuredHeight - v.measuredHeight &&
                    scrollY > oldScrollY
                ) {


                    if (transactionsList.size < total) {


                        if (CommonUtilities.isConnectingToInternet(this) == true) {
                            CommonUtilities.showLoader(this)


                            vm.transactionListApi(
                                CommonUtilities.getString(this, Constants.TOKEN).toString(),
                                skip,
                                limit
                            )

                        } else {
                            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
                        }


                    }

                }
            }

        })

    }


}
















