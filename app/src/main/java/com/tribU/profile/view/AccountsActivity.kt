package com.tribU.profile.view

import android.annotation.SuppressLint
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.lottie.LottieAnimationView
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.google.gson.JsonObject
import com.tribU.R
import com.tribU.common.base.activity.BaseDataBindingActivity
import com.tribU.databinding.AccountsActivityDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.network.Status
import com.tribU.profile.adapter.AccountsAdapter
import com.tribU.profile.adapter.SelectBankAccountAdapter
import com.tribU.profile.callbacks.AccountsActivityCallbacks
import com.tribU.profile.model.FundingSourcesDataItem
import com.tribU.profile.viewmodel.AccountsVM
import com.tribU.util.CommonUtilities
import com.tribU.util.Constants
import com.tribU.util.MyBottomSheetDialog
import java.text.SimpleDateFormat
import javax.inject.Inject

class AccountsActivity :
    BaseDataBindingActivity<AccountsActivityDataBinding>(R.layout.activity_accounts),
    AccountsActivityCallbacks {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var vm: AccountsVM

    var from_which_screen = ""
    var fundingSourcesList: ArrayList<FundingSourcesDataItem> = ArrayList()


    var accountId = ""
    var amount = 0


    private val TAG = AccountsActivity::class.java.simpleName

    override fun onDataBindingCreated() {
        binding.callback = this
        binding.lifecycleOwner = this



        from_which_screen = intent.getStringExtra(Constants.FROM_WHICH_SCREEN).toString()
        amount = intent.getIntExtra("amount", 0).toInt()


    }


    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
        vm = ViewModelProvider(this, viewModelFactory).get(AccountsVM::class.java)



        if (CommonUtilities.isConnectingToInternet(this) == true) {
            CommonUtilities.showLoader(this)


            fundingSourcesList.clear()

            vm.getFundingSources(
                CommonUtilities.getString(this, Constants.TOKEN).toString()
            )

            setupObserver()


        } else {
            CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
        }


    }


    fun initFundingSourcesRecycler(fundingSourcesList: ArrayList<FundingSourcesDataItem>) {

        var accountsAdapter =
            AccountsAdapter(this!!, fundingSourcesList)
        binding?.rvAccount?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = accountsAdapter
        }


    }


    fun initSelectAccountRecycler(fundingSourcesList: ArrayList<FundingSourcesDataItem>) {

        var accountsAdapter =
            SelectBankAccountAdapter(this!!, fundingSourcesList)
        binding?.rvAccount?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = accountsAdapter
        }


    }


    private fun setupObserver() {

        vm.responseFundingSources.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        if (result?.data != null) {


                            //   result?.data?.let { it1 -> fundingSourcesList.addAll(it1) }


                            for (i in result?.data) {
                                if (i.type.equals("bank")) {
                                    fundingSourcesList.add(i)
                                }
                            }




                            if (fundingSourcesList.isNotEmpty()) {

                                binding.cardView.visibility = View.VISIBLE

                                if (from_which_screen == Constants.FROM_HOME) {

                                    initSelectAccountRecycler(fundingSourcesList)
                                    binding.btSend.visibility = View.VISIBLE

                                } else {

                                    initFundingSourcesRecycler(fundingSourcesList)
                                    binding.btAdd.visibility = View.VISIBLE
                                }

                            } else {
                                binding.cardView.visibility = View.GONE
                            }


                        } else {
                            binding.cardView.visibility = View.GONE

                        }


                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })



        vm.responsSendTribuRequest.observe(this, Observer {
            when (it?.status) {
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    CommonUtilities.hideLoader()
                }
                Status.SUCCESS -> {

                    CommonUtilities.hideLoader()

                    val result = it.data
                    if (result?.statusCode == 200) {


                        if (result?.data != null) {


                            var data = result?.data






                            showBottomSheet(
                                1,
                                data.popUpText!!.title.toString(),
                                data.popUpText!!.description.toString(),
                                data.id.toString(),
                                data.popUpText!!.createdAt.toString(),
                                data.amount.toString()
                            )


                        }


                    } else {
                        CommonUtilities.showToastString(this, it.data?.message.toString())
                        Log.d(TAG, it.data!!.message.toString())

                    }
                }
            }
        })


    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun backClicked() {
        onBackPressed()
    }

    override fun sendClicked() {
        sendTribuFromSelectedAccount()
    }

    override fun addClicked() {
        //  TODO("Not yet implemented")
    }

    override fun deleteClicked() {
        //  TODO("Not yet implemented")
    }


    fun sendTribuFromSelectedAccount() {


        for (i in fundingSourcesList) {
            if (i.isSelected == true) {
                accountId = i.id.toString()
                break
            }
        }

        if (accountId.isNotEmpty()) {


            if (CommonUtilities.isConnectingToInternet(this) == true) {

                CommonUtilities.showLoader(this)


                var obj = JsonObject()
                obj.addProperty("ifFromWallet", false)
                obj.addProperty("from", accountId)
                obj.addProperty("amount", amount)
                //  obj.addProperty("amount", 5)


                vm.sendTribuRequestApi(
                    CommonUtilities.getString(this, Constants.TOKEN).toString(),
                    obj
                )


            } else {
                CommonUtilities.showToast(this, R.string.oops_no_internet_connection)
            }


        } else {
            CommonUtilities.showToastString(this, "Please select account to send TribU")
        }


    }


    private fun showBottomSheet(
        type: Int,
        title: String,
        message: String,
        transactionId: String,
        date: String,
        amount: String
    ) {


        var bottomSheetDialog = MyBottomSheetDialog(this, false)


        @SuppressLint("InflateParams") val view: View =
            layoutInflater.inflate(R.layout.success_bottom_sheet, null)




        if (type == 2) {
            (view.findViewById(R.id.lottie_error) as LottieAnimationView).visibility =
                View.VISIBLE
        } else {
            (view.findViewById(R.id.lottie_successful_payment) as LottieAnimationView).visibility =
                View.VISIBLE
        }



        (view.findViewById(R.id.tv_title) as TextView).text = title
        (view.findViewById(R.id.tv_message) as TextView).text = message
        (view.findViewById(R.id.tv_balance) as TextView).text = amount + " USD"
        (view.findViewById(R.id.tv_transaction_id) as TextView).text = transactionId


        try {

            var date1 = date.toString()
            try {


                var date1 = date.toString()

                var format1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                val newDate1 = format1.parse(date1)
                format1 = SimpleDateFormat("dd MMM, hh:mm aaa")
                val date2 = format1.format(newDate1)
                (view.findViewById(R.id.tv_date) as TextView).text = date2

            } catch (e: Exception) {
                e.printStackTrace()
                (view.findViewById(R.id.tv_date) as TextView).text = date1
            }


        } catch (e: Exception) {
            e.printStackTrace()
            (view.findViewById(R.id.tv_date) as TextView).visibility = View.GONE

        }


        (view.findViewById(R.id.bt_continue) as View).setOnClickListener {

            bottomSheetDialog!!.dismiss()
            //  binding.edPrice.setText("")

            Handler().postDelayed({
                finish()
            }, 200)


        }



        bottomSheetDialog!!.setContentView(view)



        bottomSheetDialog!!.show()
    }


}
















