package com.tribU.profile.model

import com.google.gson.annotations.SerializedName

data class ResponseUploadImageDM(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("error")
	val error: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class Data(

	@field:SerializedName("fieldname")
	val fieldname: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("filename")
	val filename: String? = null,

	@field:SerializedName("size")
	val size: Int? = null,

	@field:SerializedName("originalname")
	val originalname: String? = null,

	@field:SerializedName("destination")
	val destination: String? = null,

	@field:SerializedName("mimetype")
	val mimetype: String? = null,

	@field:SerializedName("encoding")
	val encoding: String? = null
)
