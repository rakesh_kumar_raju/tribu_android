package com.tribU.profile.model

import com.google.gson.annotations.SerializedName

data class ResponseAddOrTransferMoney(

	@field:SerializedName("data")
	val data: AddOrTransferMoney? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class PopUpTextAccount(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class FinalResponse(

	@field:SerializedName("transactionType")
	val transactionType: String? = null,

	@field:SerializedName("tribuSentAt")
	val tribuSentAt: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("amount")
	val amount: Double? = null,

	@field:SerializedName("senderId")
	val senderId: String? = null,

	@field:SerializedName("transactionStatus")
	val transactionStatus: String? = null,

	@field:SerializedName("popUpTextAccount")
	val popUpTextAccount: PopUpTextAccount? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("transferId")
	val transferId: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null,

	@field:SerializedName("popUpTextWallet")
	val popUpTextWallet: PopUpTextWallet? = null
)

data class PopUpTextWallet(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class AddOrTransferMoney(

	@field:SerializedName("finalResponse")
	val finalResponse: FinalResponse? = null

)
