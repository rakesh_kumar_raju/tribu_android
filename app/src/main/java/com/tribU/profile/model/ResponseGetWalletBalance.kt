package com.tribU.profile.model

import com.google.gson.annotations.SerializedName

data class ResponseGetWalletBalance(

	@field:SerializedName("data")
	val data: WalletBalData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class Total(

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("value")
	val value: String? = null
)

data class WalletBalData(

	@field:SerializedName("balanceDetails")
	val balanceDetails: BalanceDetails? = null,

	@field:SerializedName("transactions")
	val transactions: ArrayList<TransactionsItem>? = null,

	@field:SerializedName("totalNumberofTransactions")
	val totalNumberofTransactions: Int? = null
)

data class TransactionsItem(

	@field:SerializedName("transactionType")
	val transactionType: String? = null,

	@field:SerializedName("tribuSentAt")
	val tribuSentAt: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("amount")
	val amount: Double? = null,

	@field:SerializedName("senderId")
	val senderId: String? = null,

	@field:SerializedName("transactionStatus")
	val transactionStatus: String? = null,

	@field:SerializedName("__v")
	val V: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("transferId")
	val transferId: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null,

	@field:SerializedName("recieverId")
	val recieverId: String? = null
)

data class Balance(

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("value")
	val value: String? = null
)

data class BalanceDetails(

	@field:SerializedName("lastUpdated")
	val lastUpdated: String? = null,

	@field:SerializedName("total")
	val total: Total? = null,

	@field:SerializedName("balance")
	val balance: Balance? = null
)
