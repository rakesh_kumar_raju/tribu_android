package com.tribU.profile.model

import com.google.gson.annotations.SerializedName

data class ResponseFundingSources(

	@field:SerializedName("data")
	val data: List<FundingSourcesDataItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class IavAccountHolders(

	@field:SerializedName("selected")
	val selected: String? = null
)

data class FundingSourcesDataItem(

	@field:SerializedName("removed")
	val removed: Boolean? = null,

	@field:SerializedName("channels")
	val channels: ArrayList<String>? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("iavAccountHolders")
	val iavAccountHolders: IavAccountHolders? = null,

	@field:SerializedName("bankAccountType")
	val bankAccountType: String? = null,

	@field:SerializedName("isPrimary")
	val isPrimary: Boolean? = null,

	@field:SerializedName("fingerprint")
	val fingerprint: String? = null,

	@field:SerializedName("bankName")
	val bankName: String? = null,

	var isSelected: Boolean? = false
)
