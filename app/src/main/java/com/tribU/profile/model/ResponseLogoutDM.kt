package com.tribU.profile.model

import com.google.gson.annotations.SerializedName

data class ResponseLogoutDM(

/*	@field:SerializedName("data")
	val data: Data? = null,*/

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("error")
	val error: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

/*data class Data(
	val any: Any? = null
)*/
