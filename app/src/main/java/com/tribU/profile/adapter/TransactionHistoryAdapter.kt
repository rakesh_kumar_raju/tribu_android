package com.tribU.profile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tribU.R
import com.tribU.databinding.ItemTransactionHistoryBinding
import com.tribU.profile.model.TransactionsItem
import com.tribU.util.Constants
import java.text.SimpleDateFormat


class TransactionHistoryAdapter(
    var context: Context,
    var mList: ArrayList<TransactionsItem>
) :
    RecyclerView.Adapter<TransactionHistoryAdapter.MyViewHolder>(), View.OnClickListener {


    private lateinit var mBinding: ItemTransactionHistoryBinding
    var list: ArrayList<TransactionsItem> = ArrayList()

    init {
        this.list = mList
    }

/*
    fun updateData(pojoList: ArrayList<SlotDM>) {
        this.pojoList = pojoList
        notifyDataSetChanged()

    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        mBinding =
            ItemTransactionHistoryBinding.inflate(LayoutInflater.from(context), parent, false)


        return MyViewHolder(mBinding)
    }


    inner class MyViewHolder(var binding: ItemTransactionHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {


    }

    override fun getItemCount(): Int {

        return list.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var data = list[position]

        if (position == list.size - 1) {
            holder.binding.line.visibility = View.GONE
        }





        if (data.transactionType.equals(Constants.RECEIVED_MONEY))
        {
            holder.binding.tvTitle.text = Constants.RECEIVED_MONEY_TEXT
            //received
            holder.binding.tvAmount.text = "+"+data.amount.toString()+" USD"


            holder.binding.ivImg.setImageDrawable(context.resources.getDrawable(R.drawable.ic_sent))
        }
        else if (data.transactionType.equals(Constants.OWN_BANK_ACCOUNT_TO_RECEIVER_WALLET))
        {
            holder.binding.tvTitle.text = Constants.OWN_BANK_ACCOUNT_TO_RECEIVER_WALLET_TEXT

            //sent
            holder.binding.tvAmount.text = "-"+data.amount.toString()+" USD"


            holder.binding.ivImg.setImageDrawable(context.resources.getDrawable(R.drawable.ic_bank_to_wallet))

        }
        else if (data.transactionType.equals(Constants.OWN_BANK_ACCOUNT_TO_WALLET))
        {
            holder.binding.tvTitle.text = Constants.OWN_BANK_ACCOUNT_TO_WALLET_TEXT
            holder.binding.tvAmount.text = "+"+data.amount.toString()+" USD"


            holder.binding.ivImg.setImageDrawable(context.resources.getDrawable(R.drawable.ic_bank_to_wallet))

        }
        else if (data.transactionType.equals(Constants.OWN_WALLET_TO_BANK_ACCOUNT))
        {
            holder.binding.tvAmount.text = "+"+data.amount.toString()+" USD"


            holder.binding.tvTitle.text = Constants.OWN_WALLET_TO_BANK_ACCOUNT_TEXT
            holder.binding.ivImg.setImageDrawable(context.resources.getDrawable(R.drawable.ic_wallett_to_bank))

        }
        else if (data.transactionType.equals(Constants.OWN_WALLET_TO_RECEIVER_WALLET))
        {
            holder.binding.tvTitle.text = Constants.OWN_WALLET_TO_RECEIVER_WALLET_TEXT
            holder.binding.tvAmount.text = "-"+data.amount.toString()+" USD"


            //sent
            holder.binding.ivImg.setImageDrawable(context.resources.getDrawable(R.drawable.ic_received))

        }





        if (data.transactionStatus.equals(Constants.PROCESSED))
        {
            holder.binding.tvSuccessfull.visibility=View.VISIBLE
        }
        else if (data.transactionStatus.equals(Constants.PENDING))
        {
            holder.binding.tvPending.visibility=View.VISIBLE
        }




        try {

            var date1 = data.tribuSentAt.toString()
            try {


                var date1 = data.tribuSentAt.toString()

                var format1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                val newDate1 = format1.parse(date1)
                format1 = SimpleDateFormat("dd MMM, hh:mm aaa")
                val date2 = format1.format(newDate1)
                holder.binding.tvDate.text = (date2)

            } catch (e: Exception) {
                e.printStackTrace()
                holder.binding.tvDate.text = (date1)
            }


        } catch (e: Exception) {
            e.printStackTrace()
            holder.binding.tvDate.visibility = View.GONE
        }




    }

    override fun onClick(p0: View?) {
        // TODO("Not yet implemented")
    }


}