package com.tribU.profile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tribU.databinding.ItemAccountsBinding
import com.tribU.databinding.ItemTransactionHistoryBinding
import com.tribU.profile.model.FundingSourcesDataItem
import com.tribU.profile.model.TransactionsItem


class AccountsAdapter(
    var context: Context,
    var mList: ArrayList<FundingSourcesDataItem>
) :
    RecyclerView.Adapter<AccountsAdapter.MyViewHolder>(), View.OnClickListener {


    private lateinit var mBinding: ItemAccountsBinding
    var list: ArrayList<FundingSourcesDataItem> = ArrayList()

    init {
        this.list = mList
    }


/*
    fun updateData(pojoList: ArrayList<SlotDM>) {
        this.pojoList = pojoList
        notifyDataSetChanged()

    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        mBinding = ItemAccountsBinding.inflate(LayoutInflater.from(context), parent, false)






        return MyViewHolder(mBinding)
    }


    inner class MyViewHolder(var binding: ItemAccountsBinding) :
        RecyclerView.ViewHolder(binding.root) {


    }

    override fun getItemCount(): Int {

        return list.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {



        var data = list[position]


        if (position == list.size - 1) {
            holder.binding.line.visibility = View.GONE
        }

        holder.binding.tvTitle.text = data.name.toString()


    }

    override fun onClick(p0: View?) {
        // TODO("Not yet implemented")
    }


}