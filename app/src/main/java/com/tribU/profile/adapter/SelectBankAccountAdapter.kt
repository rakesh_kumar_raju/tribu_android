package com.tribU.profile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tribU.databinding.ItemSelectBankAccountBinding
import com.tribU.profile.model.FundingSourcesDataItem


class SelectBankAccountAdapter(
    var context: Context,
    var mList: ArrayList<FundingSourcesDataItem>
) :
    RecyclerView.Adapter<SelectBankAccountAdapter.MyViewHolder>(), View.OnClickListener {


    private lateinit var mBinding: ItemSelectBankAccountBinding

    var list: ArrayList<FundingSourcesDataItem> = ArrayList()


    init {
        this.list = mList
    }


/*
    fun updateData(pojoList: ArrayList<SlotDM>) {
        this.pojoList = pojoList
        notifyDataSetChanged()

    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        mBinding = ItemSelectBankAccountBinding.inflate(LayoutInflater.from(context), parent, false)






        return MyViewHolder(mBinding)
    }


    inner class MyViewHolder(var binding: ItemSelectBankAccountBinding) :
        RecyclerView.ViewHolder(binding.root) {


    }

    override fun getItemCount(): Int {

        return list.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        var data = list[position]


        if (position == list.size - 1) {
            holder.binding.line.visibility = View.GONE
        }

        holder.binding.tvName.text = data.name.toString()


        if (data.isSelected == true) {
            holder.binding.radioButton.isChecked = true
        } else {
            holder.binding.radioButton.isChecked = false
        }


        holder.itemView.setOnClickListener {

            if (data.isSelected == true) {
                data.isSelected = false
            } else {


                for (i in list) {
                    i.isSelected = false

                }
                data.isSelected = true
            }

            notifyDataSetChanged()

        }


    }

    override fun onClick(p0: View?) {
        // TODO("Not yet implemented")
    }


}