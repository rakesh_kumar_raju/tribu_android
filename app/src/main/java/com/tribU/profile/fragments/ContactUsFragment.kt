package com.tribU.profile.fragments

import android.content.Context
import android.os.Bundle
import com.bishaan.di.modules.ViewModules.ViewModelFactory
import com.tribU.R
import com.tribU.common.base.fragment.BaseDataBindingFragment
import com.tribU.databinding.ContactUsFragDataBinding
import com.tribU.di.DaggerProvider
import com.tribU.home.fragments.ConfirmationFragment
import com.tribU.profile.callbacks.ContactUsFragViewCallbacks
import com.tribU.profile.viewmodel.ContactUsFragVM
import javax.inject.Inject

class ContactUsFragment: BaseDataBindingFragment<ContactUsFragDataBinding>(R.layout.contact_us_layout),
    ContactUsFragViewCallbacks
{ companion object {

    fun newInstance() =
        ConfirmationFragment()
}

    @set:Inject
    lateinit var factory: ViewModelFactory
    public var view_model: ContactUsFragVM?=null
    override lateinit var binding: ContactUsFragDataBinding
    var mContext: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onDataBindingCreated() {
        binding.callback=this



    }




    override fun injectDaggerComponent() {
        DaggerProvider.getAppComponent()?.inject(this)
    }





//        override fun onOptionsItemSelected(@NonNull item: MenuItem): Boolean {
//            when (item.itemId) {
//                android.R.id.home -> {
//                    (mContext as HomeBaseActivity).onBackPressed()
//                    return true
//                }
//            }
//            return super.onOptionsItemSelected(item)
//        }
//


}



