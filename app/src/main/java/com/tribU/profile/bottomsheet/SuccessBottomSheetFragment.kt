package com.tribU.profile.bottomsheet

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tribU.R

import java.util.*

class SuccessBottomSheetFragment : BottomSheetDialogFragment() {

    companion object {

        fun newInstance(): SuccessBottomSheetFragment =
            SuccessBottomSheetFragment().apply {

            }

        const val TAG = "SuccessBottomSheetFragment"

    }




    var isSnoozeActivated = false


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.success_bottom_sheet, container, false)

    }

    override fun onDestroy() {
        super.onDestroy()




        /*  val intent = Intent(activity, HomeActivity::class.java)
          startActivity(intent)*/

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (view?.parent as View).setBackgroundColor(Color.TRANSPARENT)


/*
        btnfinish!!.setOnClickListener {
            stopSong()



        }
        */


    }


  //  override fun getTheme() = R.style.NoBackgroundDialogTheme


    override fun onResume() {
        super.onResume()

    }

    override fun onPause() {
        super.onPause()
    }




}


