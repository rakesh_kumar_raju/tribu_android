package com.tribU.profile.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.home.model.ResponseSendTribu
import com.tribU.network.ProfileData
import com.tribU.network.Result
import com.tribU.network.uploadImageData
import com.tribU.profile.model.ResponseFundingSources
import com.tribU.profile.model.ResponseLogoutDM
import com.tribU.profile.model.ResponseUploadImageDM
import com.tribU.signup.model.ResponseSignUpSignInDM
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject

class AccountsVM@Inject constructor(private  val data: ProfileData,
                                    private val errorProvider: ErrorProvider
): ViewModel() {




    private var _responseFundingSources = MutableLiveData<Result<ResponseFundingSources>>()
    val responseFundingSources: LiveData<Result<ResponseFundingSources>>
        get() = _responseFundingSources




    private var _responsSendTribuRequest= MutableLiveData<Result<ResponseSendTribu>>()
    val responsSendTribuRequest: LiveData<Result<ResponseSendTribu>>
        get() = _responsSendTribuRequest




    fun sendTribuRequestApi(token:String,obj:JsonObject) {
        viewModelScope.launch {
            try {
                _responsSendTribuRequest.postValue(Result.loading())
                val response = data.SendTribuRequest(token,obj)
                _responsSendTribuRequest.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _responsSendTribuRequest.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }




    fun getFundingSources(token:String) {
        viewModelScope.launch {
            try {
                _responseFundingSources.postValue(Result.loading())
                val response = data.GetFundingSourcesProcess(token)
                _responseFundingSources.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _responseFundingSources.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }




}
