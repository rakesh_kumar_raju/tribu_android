package com.tribU.profile.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.network.ProfileData
import com.tribU.network.Result
import com.tribU.network.uploadImageData
import com.tribU.profile.model.ResponseAddOrTransferMoney
import com.tribU.profile.model.ResponseGetWalletBalance
import com.tribU.profile.model.ResponseLogoutDM
import com.tribU.profile.model.ResponseUploadImageDM
import com.tribU.signup.model.ResponseSignUpSignInDM
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import org.json.JSONObject
import javax.inject.Inject

class WalletVM@Inject constructor(private  val data: ProfileData,
                                  private val errorProvider: ErrorProvider
): ViewModel() {




    private var _responseWalletBalance = MutableLiveData<Result<ResponseGetWalletBalance>>()
    val responseWalletBalance: LiveData<Result<ResponseGetWalletBalance>>
        get() = _responseWalletBalance




    fun transactionListApi(token:String,skip:Int,limit:Int){
        viewModelScope.launch {
            try {
                _responseWalletBalance.postValue(Result.loading())
                val response = data.GetTransactions(token,skip,limit)
                _responseWalletBalance.postValue(Result.success(response))
            } catch (exception: Exception) {
                exception.message?.let { Log.d("get_error", it) }
                _responseWalletBalance.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }



}
