package com.tribU.profile.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.home.model.ResponseSendTribu
import com.tribU.network.ProfileData
import com.tribU.network.Result
import com.tribU.network.uploadImageData
import com.tribU.profile.model.ResponseAddOrTransferMoney
import com.tribU.profile.model.ResponseFundingSources
import com.tribU.profile.model.ResponseLogoutDM
import com.tribU.profile.model.ResponseUploadImageDM
import com.tribU.signup.model.ResponseSignUpSignInDM
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject

class ProfileVM@Inject constructor(private  val data: ProfileData,
                                   private val errorProvider: ErrorProvider
): ViewModel() {




    private var _responseLogout = MutableLiveData<Result<ResponseLogoutDM>>()
    val responseLogout: LiveData<Result<ResponseLogoutDM>>
        get() = _responseLogout



    private var _responseFundingSources = MutableLiveData<Result<ResponseFundingSources>>()
    val responseFundingSources: LiveData<Result<ResponseFundingSources>>
        get() = _responseFundingSources



    private var _responseAddOrTransferMoney = MutableLiveData<Result<ResponseAddOrTransferMoney>>()
    val responseAddOrTransferMoney: LiveData<Result<ResponseAddOrTransferMoney>>
        get() = _responseAddOrTransferMoney






    private var _uploadImagetResult = MutableLiveData<Result<ResponseUploadImageDM>>()
    val uploadImagetResult: LiveData<Result<ResponseUploadImageDM>>
        get() = _uploadImagetResult


    private var _updateImagetResult = MutableLiveData<Result<ResponseSignUpSignInDM>>()
    val updateImagetResult: LiveData<Result<ResponseSignUpSignInDM>>
        get() = _updateImagetResult





    fun uploadProfilePicApi(files:MultipartBody.Part) {
        viewModelScope.launch {
            try {
                _uploadImagetResult.postValue(Result.loading())
                val response = data.UploadProfilePicProcess(files)
                _uploadImagetResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _uploadImagetResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }




    fun updateProfilePicApi(token:String,imageUrl:String) {
        viewModelScope.launch {
            try {
                _updateImagetResult.postValue(Result.loading())
                val response = data.UpdateProfilePicProcess("application/x-www-form-urlencoded",token,imageUrl)
                _updateImagetResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _updateImagetResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }





    fun sddOrTransferMoneyApi(token:String,obj:JsonObject){
        viewModelScope.launch {
            try {
                _responseAddOrTransferMoney.postValue(Result.loading())
                val response = data.AddOrTransferMoneyProcess(token,obj)
                _responseAddOrTransferMoney.postValue(Result.success(response))
            } catch (exception: Exception) {
                exception.message?.let { Log.d("get_error", it) }
                _responseAddOrTransferMoney.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }




    fun getFundingSources(token:String) {
        viewModelScope.launch {
            try {
                _responseFundingSources.postValue(Result.loading())
                val response = data.GetFundingSourcesProcess(token)
                _responseFundingSources.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _responseFundingSources.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }



    fun logoutApi(token:String) {
        viewModelScope.launch {
            try {
                _responseLogout.postValue(Result.loading())
                val response = data.LogoutProcess(token)
                _responseLogout.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _responseLogout.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }





}
