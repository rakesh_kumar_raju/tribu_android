package com.tribU.profile.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.tribU.errorProvider.ErrorProvider
import com.tribU.network.Result
import com.tribU.network.uploadImageData
import com.tribU.profile.model.ResponseLogoutDM
import com.tribU.profile.model.ResponseUploadImageDM
import com.tribU.signup.model.ResponseSignUpSignInDM
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject

class ProfileFragmentVM@Inject constructor(private  val upload_profile_data: uploadImageData,
                                           private val errorProvider: ErrorProvider
): ViewModel() {

    private var _uploadImagetResult = MutableLiveData<Result<ResponseUploadImageDM>>()
    val uploadImagetResult: LiveData<Result<ResponseUploadImageDM>>
        get() = _uploadImagetResult


    private var _updateImagetResult = MutableLiveData<Result<ResponseSignUpSignInDM>>()
    val updateImagetResult: LiveData<Result<ResponseSignUpSignInDM>>
        get() = _updateImagetResult



    private var _responseLogout = MutableLiveData<Result<ResponseLogoutDM>>()
    val responseLogout: LiveData<Result<ResponseLogoutDM>>
        get() = _responseLogout





    fun uploadProfilePicApi(files:MultipartBody.Part) {
        viewModelScope.launch {
            try {
                _uploadImagetResult.postValue(Result.loading())
                val response = upload_profile_data.UploadProfilePicProcess(files)
                _uploadImagetResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _uploadImagetResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }




    fun updateProfilePicApi(token:String,imageUrl:String) {
        viewModelScope.launch {
            try {
                _updateImagetResult.postValue(Result.loading())
                val response = upload_profile_data.UpdateProfilePicProcess("application/x-www-form-urlencoded",token,imageUrl)
                _updateImagetResult.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _updateImagetResult.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }



    fun logoutApi(token:String,obj:JsonObject) {
        viewModelScope.launch {
            try {
                _responseLogout.postValue(Result.loading())
                val response = upload_profile_data.LogoutProcess(token,obj)
                _responseLogout.postValue(Result.success(response))
                Log.d("check_working", "true")
            } catch (exception: Exception) {
                Log.d("check_working", exception.message.toString())
                _responseLogout.postValue(Result.error(errorProvider.getErrorMessage(exception)))
            }
        }
    }





}
